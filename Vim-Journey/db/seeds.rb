require 'date'
require 'neo4j'
require 'app/models/approval'
require 'app/models/stub_user'
require 'app/models/comment'
require 'app/models/status'
require 'app/models/message'
require 'app/models/event'
require 'app/models/company'
require 'app/models/group'
require 'app/models/privacy_settings'
require 'app/models/user'

Neo4j::Config[:storage_path] = 'db/neo4j-development'

Neo4j::Transaction.run do
  bryce = User.create({
    :first_name => 'Bryce',
    :last_name => 'Darling',
    :email => 'b@d.c',
    :password => 'test',
    :birthday => Date.new(1986, 9, 3),
    :waypoint => 'The Mentor',
    :bio => 'bryces bio'
    })

  brian = User.create({
    :first_name => 'Brian',
    :last_name => 'Tsuchiya',
    :email => 'b@t.c',
    :password => 'test',
    :birthday => Date.new(1965, 11, 26),
    :waypoint => 'The Dump',
    :bio => 'brians bio'
    })
end
