window.show = (element) ->
  element.css('visibility', 'visible').hide().fadeIn().removeClass('hidden')

window.hide = (element) ->
  console.log(element)
  element.css('visibility', 'hidden').show().fadeOut().addClass('hidden')

bindHasWaypointAndHasCategory = ->
  isAboutAWaypoint = $('#entry_is_about_a_waypoint')
  hasWaypoint = $('#entryHasWaypoint')
  hasCategory = $('#entryHasCategory')
  waypointControls = $('#entryWaypointControls')
  categoryControls = $('#entryCategoryControls')

  hasWaypoint.click ->
    isAboutAWaypoint.val('1')
    hide(categoryControls)
    show(waypointControls)

  hasCategory.click ->
    isAboutAWaypoint.val('0')
    hide(waypointControls)
    show(categoryControls)

entries = []
slider = null
min = 0
max = 3

getMax = (entries) ->
  m = entries.length
  if m > 4
    m = m - 4
  else
    m = 0
  m

bindSlider = ->
  entries = $('.journalEntry')
  slider = $('#slider')
  slider.slider({
    max: getMax(entries),
    slide: (e, el) ->
      min = el.value
      max = min + 3
      renderEntries()
  })

showLife = true
showBusiness = true
showWaypoint = true

renderEntries = ->
  filteredEntries = entries.filter((i, entry) ->
      $entry = $(entry)
      if not showLife and $entry.hasClass('lifeEntry')
        $entry.hide()
        return false
      if not showBusiness and $entry.hasClass('businessEntry')
        $entry.hide()
        return false
      if not showWaypoint and $entry.hasClass('waypointEntry')
        $entry.hide()
        return false
      return true
    )
  slider.slider('option', 'max', getMax(filteredEntries));
  filteredEntries.each((i, entry) ->
      if i >= min && i <= max
        $(entry).show()
      else
        $(entry).hide()
    )

bindLifeBusinessWaypoint = ->
  $('#life').click ->
    showLife = $(this).is(':checked')
    renderEntries()
    true

  $('#business').click ->
    showBusiness = $(this).is(':checked')
    renderEntries()
    true

  $('#waypoint').click ->
    showWaypoint = $(this).is(':checked')
    renderEntries()
    true

bindNVD3Graph = ->
  nv.addGraph -> 
    chart = nv.models.lineChart()

    chart = chart.forceY([-10,10]).tooltipContent (key, x, y, e, graph) ->
      entry = entriesData[x]
      category =  if entry.is_about_a_waypoint then entry.waypoint else entry.category
      '<h3>' + category + '</h3>' + '<p>' +  entry.date + '</p>';

    #chart.xAxis.axisLabel('Time (ms)').tickFormat(d3.format(',r'))

    #chart.yAxis.axisLabel('Voltage (v)').tickFormat(d3.format('.02f'))

    svg = d3.select('#chart svg')

    values = entriesData.map (entry, i) -> { x: i, y: entry.bliss }

    svg.datum([{
      values: values
      key: 'Entries',
      color: '#2ca02c'
    }]).transition().duration(500).call(chart)

    nv.utils.windowResize -> svg.call(chart)

    chart

bindChartDataPointClicks = ->
  $('#chart svg').on 'click', (e) ->
    elem = $(e.target)

    if elem.parent('.nv-point-paths').length
      currentItem = e.target.getAttribute('class').match(/\d+/)[0]
      currentUrl = _data[0].urls [currentItem]
      console.log(currentItem)
      console.log(currentUrl)

$ ->
  bindSlider()
  bindHasWaypointAndHasCategory()
  bindLifeBusinessWaypoint()
  bindNVD3Graph()
  bindChartDataPointClicks()
