class PrivacySettingsController < ApplicationController
  # GET /privacy-settings
  def index
    require_current_user
  end

  # POST /privacy-settings
  def update
    user = User.find(params[:user_id])

    redirect_to login_url if user.nil?

    params.each do |param|
      if param[0].start_with? "privacy_settings_"
        user[param[0]] = param[1] == "1"
        user.save
      end
    end

    # ugly hack
    session[:user_id] = user.id
    render :index
  end
end
