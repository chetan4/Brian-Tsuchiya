class UsersController < ApplicationController
  # GET /users
  # GET /users.json
  def index
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = current_user || User.new

    respond_to do |format|
      if current_user
        if not current_user.has_confirmed_email; format.html { render action: 'email_confirmation_required' }
        elsif current_user.step_number == 1; format.html { render action: 'step_1' }
        elsif current_user.step_number == 2; format.html { render action: 'step_2' }
        elsif current_user.step_number == 3; format.html { render action: 'step_3' }
        elsif current_user.step_number == 4; format.html { render action: 'step_4' }
        elsif current_user.step_number == 5; format.html { render action: 'step_5' }
        elsif current_user.step_number == 6; format.html { render action: 'step_6' }
        elsif current_user.step_number == 7; format.html { render action: 'step_7' }
        elsif current_user.step_number == 8; format.html { render action: 'step_8' }
        elsif current_user.step_number == 9; format.html { render action: 'step_9' }
        elsif current_user.step_number == 10; format.html { render action: 'step_10' }
        elsif current_user.step_number == 11; format.html { render action: 'step_11' }
        else format.html { render action: 'wall' }
        end
      else
        format.html # new.html.erb
      end
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        UserMailer.registration_confirmation(@user).deliver

        format.html { redirect_to '/email-confirmation-sent' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: 'new', notice: 'User could not be created.' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    redirect_to login_url if @user.nil?

    picture = params[:user][:picture]
    if not picture.nil?
      ext = File.extname(picture.original_filename)
      File.open(Rails.root.join('public', 'users', "#{@user.id}#{ext}"), 'w') do |file|
        file.write(picture.read)
      end
      params[:user].delete(:picture)
    end

    @user.add_status(params[:status_content])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to params[:redirect_to] || root_url }
        format.json { head :no_content }
      else
        format.html { render action: 'edit', notice: 'User could not be updated.' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was deleted.' }
      format.json { head :no_content }
    end
  end

  # GET /email-confirmation-sent
  def get_email_confirmation_sent
    render action: 'email_confirmation_sent'
  end

  # GET /confirm-email
  def get_confirm_email
    @user = User.find(params[:user_id])

    if @user.confirm_email(params[:email])
      session[:user_id] = @user.id
      redirect_to root_url
    else
      redirect_to root_url, notice: 'User email could not be confirmed.'
    end
  end
end
