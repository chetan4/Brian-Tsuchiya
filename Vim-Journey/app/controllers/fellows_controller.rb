class FellowsController < ApplicationController
  # POST /fellows/invite
  # POST /fellows/invite.json
  def invite
    require_current_user

    user_id = params[:user_id]
    email = params[:email]

    if user_id.nil? and email.nil?
      raise Exception, "user_id or email required"
    end

    user = User.find(user_id) if not user_id.nil?
    user = User.find_by_email(email) if not email.nil?

    if user.nil? and not user_id.nil?
      raise Exception, "user not found for user_id #{user_id}"
    elsif user.nil? and not email.nil?
      who = email
      invited = UserMailer.invite_fellow(current_user, email).deliver
    else
      who = user.name
      invited = current_user.invite_fellow(user)
    end

    respond_to do |format|
      if invited
        format.html { redirect_to '/', notice: "#{who} was invited." }
        format.json { render json: current_user, status: :invited, location: current_user }
      else
        format.html { redirect_to '/', notice: "#{who} could not be invited." }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /fellows/approve
  # POST /fellows/approve.json
  def approve
    require_current_user

    user_id = params[:user_id]

    user = User.find(user_id)

    raise Exception, "user not found for user_id #{user_id}" if user.nil?

    respond_to do |format|
      if current_user.approve_fellow(user)
        format.html { redirect_to '/', notice: "#{user.name} was approved." }
        format.json { render json: current_user, status: :approved, location: current_user }
      else
        format.html { redirect_to '/', notice: "#{user.name} could not be approved." }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /fellows/ignore
  # POST /fellows/ignore.json
  def ignore
    require_current_user

    user_id = params[:user_id]

    user = User.find(user_id)

    raise Exception, "user not found for user_id #{user_id}" if user.nil?

    respond_to do |format|
      if current_user.ignore_fellow(user)
        format.html { redirect_to '/', notice: "#{user.name} was ignored." }
        format.json { render json: current_user, status: :ignored, location: current_user }
      else
        format.html { redirect_to '/', notice: "#{user.name} could not be ignored." }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /fellows/remove
  # POST /fellows/remove.json
  def remove
    require_current_user

    user_id = params[:user_id]

    user = User.find(user_id)

    raise Exception, "user not found for user_id #{user_id}" if user.nil?

    respond_to do |format|
      if current_user.remove_fellow(user)
        format.html { redirect_to '/', notice: "#{user.name} was removed." }
        format.json { render json: current_user, status: :removed, location: current_user }
      else
        format.html { redirect_to '/', notice: "#{user.name} could not be removed." }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end
end
