class AccountController < ApplicationController
  # GET /account
  def index
    require_current_user
  end
end
