class CommentsController < ApplicationController
  # POST /comments
  # POST /comments.json
  def create
    require_current_user

    content = params[:content]

    raise Exception, "content required" if content.nil?

    status_id = params[:status_id]

    raise Exception, "status_id required" if status_id.nil?

    status = Status.find(status_id)

    raise Exception, "status not found for status_id #{status_id}" if status.nil?

    respond_to do |format|
      if status.add_comment(current_user, content)
        format.html { redirect_to '/' }
        format.json { render json: current_user, status: :created, location: current_user }
      else
        format.html { redirect_to '/', notice: "Comment for Status #{status_id} could not be added." }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end
end
