class StatusesController < ApplicationController
  # POST /statuses
  # POST /statuses.json
  def create
    require_current_user

    respond_to do |format|
      if current_user.add_status(params[:content])
        format.html { redirect_to '/' }
        format.json { render json: current_user, status: :created, location: current_user }
      else
        format.html { redirect_to '/', notice: 'Status could not be created.' }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end
end
