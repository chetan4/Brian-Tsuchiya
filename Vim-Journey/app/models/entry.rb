class Entry < Neo4j::Rails::Model
  property :is_about_a_waypoint, :type => :boolean
  property :waypoint, :type => String
  property :category, :type => String
  property :date, :type => Date
  property :bliss, :type => Fixnum
  property :privacy, :type => Fixnum
  property :description, :type => String
end
