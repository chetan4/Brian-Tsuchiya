class Message < Neo4j::Rails::Model
  property :content, :type => String
  property :created_at, :type => DateTime
  property :updated_at, :type => DateTime

  has_one(:author).to(User)
end
