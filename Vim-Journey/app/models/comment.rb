class Comment < Neo4j::Rails::Model
  property :content, :type => String
  property :created_at, :type => DateTime
  property :updated_at, :type => DateTime

  has_one(:author).to(User)
  has_n(:likes).to(User)

  def add_like(user)
    not is_liked_by(user) and likes << user and save
  end

  def is_liked_by(user)
    not likes.find(user).nil?
  end

  def remove_like(user)
    delete_rel(likes_rels, user)
  end

  def delete_rel(rels, model)
    Neo4j::Transaction.run do
      r = rels.find(model) and not r.nil? and r.del
    end
    not rels.find(model)
  end
end
