class Event < Neo4j::Rails::Model
  property :name, :type => String
  property :description, :type => String
  property :created_at, :type => DateTime
  property :updated_at, :type => DateTime

  has_n(:likes).to(User)
  has_n(:comments).to(Comment)

  def add_like(user)
    likes.find(user).nil? and likes << user and save
  end

  def add_comment(user, content)
    c = Comment.new(:content => content) and c.author = user and comments << c and save
  end
end
