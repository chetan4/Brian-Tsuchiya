class User < Neo4j::Rails::Model
  property :first_name, :type => String
  property :last_name, :type => String
  property :username, :type => String
  property :email, :type => String
  property :password, :type => String
  #property :password_confirmation, :type => String
  property :birthday, :type => Date
  property :waypoint, :type => String
  property :bio, :type => String
  property :created_at, :type => DateTime
  property :updated_at, :type => DateTime

  property :has_confirmed_email, :type => :boolean, :default => false
  property :step_number, :type => Fixnum, :default => 1
  property :has_read_entrepreneurs_journey, :type => Fixnum, :default => Answer::NO_ANSWER
  property :is_familiar_with_waypoints, :type => Fixnum, :default => Answer::NO_ANSWER
  property :wants_to_read_entrepreneurs_journey, :type => Fixnum, :default => Answer::NO_ANSWER
  property :wants_to_read_waypoints, :type => Fixnum, :default => Answer::NO_ANSWER
  property :has_taken_ezog, :type => Fixnum, :default => Answer::NO_ANSWER
  property :wants_to_take_ezog, :type => Fixnum, :default => Answer::NO_ANSWER
  property :visionary_score, :type => Fixnum, :default => Answer::NO_ANSWER
  property :architect_score, :type => Fixnum, :default => Answer::NO_ANSWER
  property :builder_score, :type => Fixnum, :default => Answer::NO_ANSWER
  property :cultivator_score, :type => Fixnum, :default => Answer::NO_ANSWER

  property :privacy_settings_ezog, :type => :boolean, :default => true
  property :privacy_settings_birthday, :type => :boolean, :default => true
  property :privacy_settings_background, :type => :boolean, :default => true
  property :privacy_settings_hometown, :type => :boolean, :default => true
  property :privacy_settings_website, :type => :boolean, :default => true
  property :privacy_settings_email, :type => :boolean, :default => true
  property :privacy_settings_address, :type => :boolean, :default => true
  property :privacy_settings_phone_number, :type => :boolean, :default => true
  property :privacy_settings_company_address, :type => :boolean, :default => true
  property :privacy_settings_company_phone_number, :type => :boolean, :default => true

  property :hometown, :type => String
  property :gender, :type => String
  property :college, :type => String
  property :phone_number, :type => String
  property :address, :type => String
  property :website, :type => String
  property :skype_username, :type => String
  property :url, :type => String
  property :company_name, :type => String
  property :company_address, :type => String
  property :company_website, :type => String
  property :company_industry, :type => String
  property :company_phone_number, :type => String
  property :stage_of_entrepreneurship, :type => String
  property :favorite_quotation, :type => String
  property :entrepreneurial_super_power, :type => String

  def self.find_by_email(email)
    User.all.find { |u| u.email == email }
  end

  def authenticate(password)
    # TODO: secure password with bcrypt
    self.password == password
  end

  def confirm_email(email)
    self.has_confirmed_email = self.email == email
    self.has_confirmed_email and save
  end
  
  #attr_accessible :email, :password, :password_confirmation
  #has_secure_password
  #validates_presence_of :password, :on => :create

  #validates_presence_of :first_name, :last_name, :email, :birthday
  #validates_uniqueness_of :email
  
  #has_secure_password

  has_n(:fellows).to(User)
  has_n(:statuses).to(Status)
  has_n(:messages).to(Message)
  has_n(:events).to(Event)
  has_n(:companies).to(Company)
  has_n(:groups).to(Group)
  has_n(:entries).to(Entry)

  def name
    "#{first_name} #{last_name}"
  end

  def make_mutual_fellows(user)
    add_fellow(user) and user.add_fellow(self)
  end

  def add_fellow(user)
    not has_fellow(user) and fellows << user and save
  end

  def has_fellow(user)
    not fellows.find(user).nil?
  end

  def approve_fellow(user)
    update_fellow_status(user, Approval::APPROVED)
  end

  def ignore_fellow(user)
    update_fellow_status(user, Approval::IGNORED)
  end

  def remove_fellow(user)
    delete_rel(fellows_rels, user) and delete_rel(user.fellows_rels, self)
  end

  def update_fellow_status(user, status)
    update_rel_status(fellows_rels, user, status)
  end

  def invite_fellow(user)
    not self.has_fellow(user) and not user.has_fellow(self) and
    self.fellows_rels.connect(user, :status => Approval::APPROVED).save and
    user.fellows_rels.connect(self, :status => Approval::UNAPPROVED).save
  end

  def unapproved_fellows
    fellows_rels_by_status(Approval::UNAPPROVED)
  end

  def approved_fellows
    fellows_rels_by_status(Approval::APPROVED)
  end

  def suggested_fellows
    User.all.find_all { |u| u.id != id and not has_unapproved_fellow?(u) and not has_approved_fellow?(u) }
  end

  def fellows_rels_by_status(status)
    fellows_rels.find_all { |f| f.status == status }.map { |f| f.end_node }
  end

  def are_mutual_fellows?(user)
    has_approved_fellow?(user) and user.has_approved_fellow?(self)
  end

  def has_unapproved_fellow?(user)
    has_rel_status(fellows_rels, user, Approval::UNAPPROVED)
  end

  def has_approved_fellow?(user)
    has_rel_status(fellows_rels, user, Approval::APPROVED)
  end

  def mutual_fellows
    fellows.find_all { |f| self.are_mutual_fellows?(f) }
  end

  def mutual_fellows_statuses
    mutual_fellows.map { |f| f.statuses }.flatten
  end

  def wall_statuses
    mutual_fellows_statuses.concat(statuses).sort_by { |s| s.updated_at }.reverse!
  end

  def add_status(content)
    statuses << Status.new(:content => content, :author => self) and save if not content.nil? and not content.empty?
  end

  def add_entry(entry)
    entries << entry and save
  end

  def send_message(user, content)
    user.receive_message Message.new(:content => content, :author => self)
  end

  def receive_message(message)
    user.messages << message and user.save
  end

  def add_event(name, description)
    event = Event.new(:name => name, :description => description).save
    invite_to_event(event) and approve_event(event)
  end

  def invite_to_event(event)
    events << event and save
  end

  def approve_event(event)
    update_event_status(event, Approval::APPROVED)
  end

  def ignore_event(event)
    update_event_status(event, Approval::IGNORED)
  end

  def update_event_status(event, status)
    update_rel_status(events_rels, event, status)
  end

  def add_company(name, description)
    company = Company.new(:name => name, :description => description).save
    invite_to_company(company) and approve_company(company)
  end

  def invite_to_company(company)
    companies << company and save
  end

  def approve_company(company)
    update_company_status(company, Approval::APPROVED)
  end

  def ignore_company(company)
    update_company_status(company, Approval::IGNORED)
  end

  def update_company_status(company, status)
    update_rel_status(company_rels, company, status)
  end

  def add_group(name, description)
    group = Group.new(:name => name, :description => description).save
    invite_to_group(group) and approve_group(group)
  end

  def invite_to_group(group)
    groups << group and save
  end

  def approve_group(group)
    update_group_status(group, Approval::APPROVED)
  end

  def ignore_group(group)
    update_group_status(group, Approval::IGNORED)
  end

  def update_group_status(group, status)
    update_rel_status(group_rels, group, status)
  end

  def update_rel_status(rels, model, status)
    r = rels.find(model) and not r.nil? and r[:status] = status and r.save
  end

  def has_rel_status(rels, model, status)
    r = rels.find(model) and not r.nil? and r[:status] == status
  end

  def delete_rel(rels, model)
    Neo4j::Transaction.run do
      r = rels.find(model) and not r.nil? and r.del
    end
    not rels.find(model)
  end

  def picture(size)
    files = ["/#{id}.jpg", "#{id}.gif", "#{id}.png"]
    files.each do |file|
      return file if File.exists?(file)
    end
    "http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=#{size}"
  end
end
