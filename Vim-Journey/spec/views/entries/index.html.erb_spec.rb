require 'spec_helper'

describe "entries/index" do
  before(:each) do
    assign(:entries, [
      stub_model(Entry,
        :waypoint => "Waypoint",
        :category => "Category",
        :bliss => "",
        :privacy => "",
        :post_to_wall => false,
        :description => "MyText"
      ),
      stub_model(Entry,
        :waypoint => "Waypoint",
        :category => "Category",
        :bliss => "",
        :privacy => "",
        :post_to_wall => false,
        :description => "MyText"
      )
    ])
  end

  it "renders a list of entries" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Waypoint".to_s, :count => 2
    assert_select "tr>td", :text => "Category".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
