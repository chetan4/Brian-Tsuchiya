require 'spec_helper'

describe "entries/show" do
  before(:each) do
    @entry = assign(:entry, stub_model(Entry,
      :waypoint => "Waypoint",
      :category => "Category",
      :bliss => "",
      :privacy => "",
      :post_to_wall => false,
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Waypoint/)
    rendered.should match(/Category/)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(/false/)
    rendered.should match(/MyText/)
  end
end
