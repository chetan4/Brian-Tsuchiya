require 'spec_helper'

describe "entries/edit" do
  before(:each) do
    @entry = assign(:entry, stub_model(Entry,
      :waypoint => "MyString",
      :category => "MyString",
      :bliss => "",
      :privacy => "",
      :post_to_wall => false,
      :description => "MyText"
    ))
  end

  it "renders the edit entry form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", entry_path(@entry), "post" do
      assert_select "input#entry_waypoint[name=?]", "entry[waypoint]"
      assert_select "input#entry_category[name=?]", "entry[category]"
      assert_select "input#entry_bliss[name=?]", "entry[bliss]"
      assert_select "input#entry_privacy[name=?]", "entry[privacy]"
      assert_select "input#entry_post_to_wall[name=?]", "entry[post_to_wall]"
      assert_select "textarea#entry_description[name=?]", "entry[description]"
    end
  end
end
