Array.prototype.random = function() {
  return this[Math.floor(Math.random() * this.length)];
};

$(function() {
  var prompt = [
    'How are you doing?',
    'Tell us your story...'
  ].random();

  $('#updateJourney textarea').attr('placeholder', prompt);
});