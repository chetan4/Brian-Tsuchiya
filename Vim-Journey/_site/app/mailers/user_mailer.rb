class UserMailer < ActionMailer::Base
  default from: "darlingbryce@gmail.com"

  def registration_confirmation(user)
    @user = user
    @url = "http://98.250.38.164:3000/confirm-email?user_id=#{user.id}&email=#{user.email}"
    mail(:to => user.email, :subject => "Welcome to The Entrepreneur's Journey")
  end

  def invite_fellow(from, to)
    @from = from
    @url = "http://98.250.38.164:3000/"
    mail(:to => to, :subject => "An Invitation to The Entrepreneur's Journey")
  end
end
