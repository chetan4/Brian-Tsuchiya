class Group < Neo4j::Rails::Model
  property :name, :type => String
  property :description, :type => String
  property :created_at, :type => DateTime
  property :updated_at, :type => DateTime
end
