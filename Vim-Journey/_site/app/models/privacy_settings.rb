class PrivacySettings < Neo4j::Rails::Model
  property :ezog, :type => :boolean
  property :birthday, :type => :boolean
  property :background, :type => :boolean
  property :hometown, :type => :boolean
  property :website, :type => :boolean
  property :email, :type => :boolean
  property :address, :type => :boolean
  property :phone_number, :type => :boolean
  property :company_address, :type => :boolean
  property :company_phone_number, :type => :boolean

  def initialize
    ezog = birthday = background = hometown = website = email = address = phone_number = company_address = company_phone_number = true
  end
end
