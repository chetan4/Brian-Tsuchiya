class Company < Neo4j::Rails::Model
  property :name, :type => String
  property :description, :type => String
  property :created_at, :type => DateTime
  property :updated_at, :type => DateTime

  has_n(:likes).to(User)

  def add_like(user)
    likes.find(user).nil? and likes << user and save
  end
end
