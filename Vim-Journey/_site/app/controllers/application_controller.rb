class ApplicationController < ActionController::Base
  protect_from_forgery
  force_ssl

  protected

  def require_current_user
    raise Exception, "current user required" if current_user.nil?
  end

  private

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  helper_method :current_user
end
