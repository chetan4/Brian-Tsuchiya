class LikesController < ApplicationController
  # POST /likes
  # POST /likes.json
  def create
    require_current_user

    status_id = params[:status_id]
    comment_id = params[:comment_id]

    raise Exception, "status_id or comment_id required" if status_id.nil? and comment_id.nil?

    if not status_id.nil?
      add_status_like(status_id)
    elsif not comment_id.nil?
      add_comment_like(comment_id)
    end
  end

  # DELETE /likes
  # DELETE /likes.json
  def destroy
    require_current_user

    status_id = params[:status_id]
    comment_id = params[:comment_id]

    raise Exception, "status_id or comment_id required" if status_id.nil? and comment_id.nil?

    if not status_id.nil?
      remove_status_like(status_id)
    elsif not comment_id.nil?
      remove_comment_like(comment_id)
    end
  end

  private

  def add_status_like(status_id)
    status = Status.find(status_id)

    raise Exception, "status not found for status_id #{status_id}" if status.nil?

    respond_to do |format|
      if status.add_like(current_user)
        format.html { redirect_to '/' }
        format.json { render json: current_user, status: :liked, location: current_user }
      else
        format.html { redirect_to '/', notice: "Status #{status_id} could not be liked." }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end

  def remove_status_like(status_id)
    status = Status.find(status_id)

    raise Exception, "status not found for status_id #{status_id}" if status.nil?

    respond_to do |format|
      if status.remove_like(current_user)
        format.html { redirect_to '/' }
        format.json { render json: current_user, status: :unliked, location: current_user }
      else
        format.html { redirect_to '/', notice: "Status #{status_id} could not be unliked." }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end

  def add_comment_like(comment_id)
    comment = Comment.find(comment_id)

    raise Exception, "comment not found for comment_id #{comment_id}" if comment.nil?

    respond_to do |format|
      if comment.add_like(current_user)
        format.html { redirect_to '/' }
        format.json { render json: current_user, status: :liked, location: current_user }
      else
        format.html { redirect_to '/', notice: "Comment #{comment_id} could not be liked." }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end

  def remove_comment_like(comment_id)
    comment = Comment.find(comment_id)

    raise Exception, "comment not found for comment_id #{comment_id}" if comment.nil?

    respond_to do |format|
      if comment.remove_like(current_user)
        format.html { redirect_to '/' }
        format.json { render json: current_user, status: :unliked, location: current_user }
      else
        format.html { redirect_to '/', notice: "Comment #{comment_id} could not be unliked." }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end
end
