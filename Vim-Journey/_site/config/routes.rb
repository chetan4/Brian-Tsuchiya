VimJourney::Application.routes.draw do
  resources :users

  controller :users do
    get 'email-confirmation-sent' => 'users#get_email_confirmation_sent'
    get 'confirm-email' => 'users#get_confirm_email'
  end

  controller :sessions do
    get 'login' => 'sessions#new'
    post 'login' => 'sessions#create'
    post 'logout' => 'sessions#destroy'
  end

  controller :statuses do
    post 'statuses' => 'statuses#create'
  end

  controller :likes do
    post 'likes' => 'likes#create'
    delete 'likes' => 'likes#destroy'
  end

  controller :fellows do
    post 'fellows/invite' => 'fellows#invite', :as => 'invite_fellow'
    post 'fellows/approve' => 'fellows#approve'
    post 'fellows/ignore' => 'fellows#ignore'
    post 'fellows/remove' => 'fellows#remove'
  end

  controller :comments do
    post 'comments' => 'comments#create'
  end

  root :to => 'users#new'
end
