# Load the FLP object
require File.expand_path('../fundinglaunchpad', __FILE__)

# Load the rails application
require File.expand_path('../application', __FILE__)

# Load heroku vars from local file
heroku_env = File.expand_path('../heroku_env.rb', __FILE__)
load(heroku_env) if File.exists?(heroku_env) and Rails.env.development?

# Load the PCP Constraint for routing
require File.expand_path('../../lib/pcp_constraint', __FILE__)

# Initialize the rails application
Icf::Application.initialize!
