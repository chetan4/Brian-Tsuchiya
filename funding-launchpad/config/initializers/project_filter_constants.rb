PROJECT_FILTERS = [
  {:scope => "all", :label => "All"},
  {:scope => "active", :label => "Active"},
  {:scope => "unapproved", :label => "Unapproved"}
]
