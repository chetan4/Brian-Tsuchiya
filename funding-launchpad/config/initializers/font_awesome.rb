if Rails.env.test?
  Rails.application.config.assets.paths.reject! { |path| path =~ /font-awesome/ }
end
