Version.module_eval do
  include HTMLDiff

  self.abstract_class = true

  define_method :html_diff do |key|
    if not self.changeset.nil? and self.changeset.has_key?(key)
       self.diff(self.changeset[key].first, self.changeset[key].last)
    end
  end
end
