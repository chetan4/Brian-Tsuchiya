# Access is based on a user access level.  The level is stored in the 'access' attribute
# which is an integer union of the following constants:

VIM_USER = 1
ADMIN_USER = 2
EDITOR_USER = 4