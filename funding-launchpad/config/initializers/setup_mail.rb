ActionMailer::Base.smtp_settings = {  
  :address   => "smtp.mandrillapp.com",
  :port      => 587,
  :user_name => FLP["MANDRILL_USERNAME"],
  :password  => FLP["MANDRILL_SMTP_PASS"]

}