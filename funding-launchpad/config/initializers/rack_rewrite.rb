Icf::Application.config.middleware.insert_before(Rack::Lock, Rack::Rewrite) do
  r301 '/blog', 'http://blog.fundinglaunchpad.com'
  r301 '/blog/', 'http://blog.fundinglaunchpad.com'
  r301 %r{/wp-content/uploads(.*)}, 'http://blog.fundinglaunchpad.com/wp-content/uploads$1' #fix whitepaper downlaod link
  r301 %r{/2012(.*)}, 'http://blog.fundinglaunchpad.com/2012$1'
  r301 %r{/tag(.*)}, 'http://blog.fundinglaunchpad.com/tag$1'
  r301 %r{/category(.*)}, 'http://blog.fundinglaunchpad.com/category$1'
  r301 %r{/author(.*)}, 'http://blog.fundinglaunchpad.com/author$1'
end
