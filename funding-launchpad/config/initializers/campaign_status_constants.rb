# values used against Offeirng.status to determine current status of an offering

IN_DEVELOPMENT = 1
DEVELOPMENT_COMPLETED = 2
APPROVED_FOR_LAUNCH = 4
ACTIVE = 8
FUNDING = 16
FUNDED = 32
DISABLED = 64
CLOSED = 128
HAS_BEEN_ACTIVE = 256