require File.expand_path('../boot', __FILE__)

require "csv"
# Pick the frameworks you want:
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "active_resource/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  Bundler.require(*Rails.groups(:assets => %w(development test)))
  # If you want your assets lazily compiled in production, use this line
  # Bundler.require(:default, :assets, Rails.env)
end

module Icf
  class Application < Rails::Application

    ChronicDuration.raise_exceptions = true
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    config.autoload_paths += %W(#{config.root}/app/observers #{config.root}/app/models/modules)

    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    config.active_record.observers = [:investment_observer, :question_observer, :answer_observer, :contact_observer, :update_observer]

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password, :password_confirmation]

    # Use SQL instead of Active Record's schema dumper when creating the database.
    # This is necessary if your schema can't be completely dumped by the schema dumper,
    # like if you have constraints or database-specific column types
    # config.active_record.schema_format = :sql

    # Enforce whitelist mode for mass assignment.
    # This will create an empty whitelist of attributes available for mass-assignment for all models
    # in your app. As such, your models will need to explicitly whitelist or blacklist accessible
    # parameters by using an attr_accessible or attr_protected declaration.
    # config.active_record.whitelist_attributes = true

    # Enable the asset pipeline
    config.assets.enabled = true

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'

    #config.less.compress = true
    config.assets.initialize_on_precompile = false

    #precompile since it isn't included in application.js manifest
    config.assets.precompile += %w( html5shiv.js )

    config.generators do |g|
      g.stylesheets false
      g.helper      false
      g.javascripts false
      g.test_framework :rspec
    end

  end
end

# Constants used by Stripe.  Entered here instead of in an initializer because initializers are not run on precompile so
# these constants would not be set there.

STRIPE_ACCESS_TOKEN_URL       = "https://connect.stripe.com/oauth/token"
STRIPE_PER_TRANS_PERCENTAGE   = 0.029 # 2.9%
STRIPE_PER_TRANS_FEE          = 30    # $0.30
FL_USAGE_FEE                  = 0.08  # 8%
if ["production"].include?(Rails.env)
  STRIPE_CLIENT_ID            = 'ca_1Gdj1GX13pi6jDKLOH5Q5bzeTyiMNtjy'
  Stripe.api_key              = 'sk_live_8WDRyUd8wKrFgAtLKcwbo6Rq'
  STRIPE_SECRET_KEY           = 'sk_live_8WDRyUd8wKrFgAtLKcwbo6Rq'
  STRIPE_PUBLIC_KEY           = 'pk_live_rnIAnuqGJuTgZks8pN78Gi67'
else
  STRIPE_CLIENT_ID            = 'ca_1GdjKRK978WQZLD1ilouvfP5phyWmLsc'
  Stripe.api_key              = 'sk_test_ldUUHRpvOCWgplIeWzwnoAE2'
  STRIPE_SECRET_KEY           = 'sk_test_ldUUHRpvOCWgplIeWzwnoAE2'
  STRIPE_PUBLIC_KEY           = 'pk_test_yATQiZWD4cxmhdkXCTxS4B8x'
end

# For franchising, we are currently only dealing with contributions.  To preserve the ability
# to later deal with actual investments (without rewriting code), we use a flag to hide investment
# related material without actually removing it from the code base.
# A hack, yes, but quicker than removing code and less confusing than maintaining multiple code bases.

CONTRIBUTIONS_ONLY = true

#load whitelabel objects
PCP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../pcp.yml', __FILE__))))

PCP_CONFIG.class_eval do
  define_method :"is_pcp?" do
    ENV['PCP_ENV'] ? ENV['PCP_ENV'].to_bool : false
  end
end

[:logo, :logo_alt, :twitter_name, :blog_url, :email, :facebook_url, :ga_tracker, :admin_email, :name, :description, :support_email, :logo_email].each do |arg|
  PCP_CONFIG.class_eval do
    define_method arg do
      self.get_val(arg)
    end
  end
end

PCP_CONFIG.class_eval do
  define_method :"get_val" do |arg|
    if self.is_pcp? and self.has_key? ENV['PCP']
      key = ENV['PCP'].to_sym
      self[key][arg]
    else
      nil
    end
  end
end
