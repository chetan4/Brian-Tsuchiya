Icf::Application.routes.draw do

  match 'press/admin' => 'press#admin'
  resources :press

  mount_roboto

  devise_for :users, :controllers => {:invitations => 'users/invitations', :registrations => 'users/registrations', :confirmations => 'users/confirmations', :omniauth_callbacks => 'users/omniauth_callbacks' }, :skip => [:sessions]
  as :user do
    get 'login' => 'users/registrations#new', :as => :new_user_session
    post 'login' => 'users/sessions#create', :as => :user_session
    match 'logout' => 'devise/sessions#destroy', :as => :destroy_user_session,
      :via => Devise.mappings[:user].sign_out_via
    get "signup", :to => "users/registrations#new", :as => :new_user_registration
    post "signup", :to => "users/registrations#create", :as => :user_registration
    put "signup", :to => "users/registrations#update", :as => :user_registration
  end
  resources :authentications
  resources :offerings do
    member do
      get :revisions
    end
    resources :photos
    resource  :company
    match 'bank/admin' => 'bank#admin'
    resource  :bank
    resources :contributions
    resources :documents
    resources :links
    match 'events/admin' => 'events#admin'
    resources :events
    match 'auctions/admin' => 'auctions#admin'
    resources :auctions
    match 'rewards/admin' => 'rewards#admin'
    resources :rewards do
      resources :user_rewards
    end
    resources :user_rewards

    match 'investments/share' => 'investments#share'
    resources :investments, :except => [:edit, :show]

    resources :private_investments, :only => [:new, :create]
    resources :questions do
      resources :answers
      get 'page/:page', :action => :index, :on => :collection
    end

    resources :team_members do
      collection do
        put 'update'
      end
    end

    resources :updates do
      member do
        get :revisions
      end
      collection do
        get :admin
      end
    end

    resources :videos do
      collection do
        get 'preview'
      end
    end
    member do
      get :complete
      get :uncomplete
      get :approve
      get :activate
      get :admin
      get :contributors
      get :investors
      get :investment_history
      get :change_status
      get :follow
      get :unfollow
      get :followers
    end
  end

#  resources :contributions
  resources :auction_preferences
  resource :bank
  resources :bid_increments
  resources :default_following_options
  resources :filings
  resources :following_options
  resources :investment_totals
  resources :mycontributions
  resources :myinvestments
  resources :pages
  resources :people do
    member do
      get 'profile'
      get :profile_edit
      post :profile_update
    end
  end
  resources :reviews
  resources :rewards
  resources :user_rewards do
    member do
      get :fulfill
    end
  end
  resources :users do
    member do
      get 'invite'
      get :followings
      get :unfollow
      put :confirm
    end
  end
  resources :contacts, :only => [:new, :create]

  resources :groups do
    member do
      get :members
      put :toggle_offering
    end
  end

  devise_scope :user do
    get "offerings/:offering_id/invitation/new", :to => "users/invitations#new_offering_invite", :as => "new_offering_invitation"
    post "offerings/:offering_id/invitation", :to => "users/invitations#create_offering_invite", :as => "offering_invitation"
    delete "offerings/:offering_id/invitation/:user_id", :to => "users/invitations#destroy_offering_invite", :as => "destroy_offering_invitation"
    post 'offerings/:offering_id/invitation/:id', :to => "users/invitations#resend_offering_invite", :as => "resend_offering_invitation"
    get 'user/accredited', :to => "users#accredited", :as => "accredited_user"
    get '/users/auth/:provider' => 'users/omniauth_callbacks#passthru'
  end

  match "/unsubscribe" => "users#unsubscribe"
  match "/newsletter" => "users#newsletter"
  match 'about' => 'pages#new_message', :as => 'about', :via => :get
  match 'about' => 'pages#create_message', :as => 'about', :via => :post
  match "/help" => "pages#help"
  match "/my_offerings" => "pages#my_offerings"
  match "/privacy" => "pages#privacy"
  match "/terms" => "pages#terms"
  match "/admin" => "pages#admin"
  match "/settings" => "pages#settings"
  match "/optin" => "pages#optin"
  match "/stripe_validation" => "pages#stripe_validation"
  match "/financials" => "pages#financials"

  #PCP routing
  pcp_constraint = PCPConstraint.new
  root :to => redirect("/offerings/%{name}" % {name: pcp_constraint.name}), :constraints => pcp_constraint
  # funding launchpad routing
  root :to => 'offerings#index'
end
