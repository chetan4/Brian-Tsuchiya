# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#require all of the files in db/seeds
#Dir[File.dirname(__FILE__) + '/seeds/*.rb'].each {|file| require file }

#For invidual files
#require File.expand_path('../seeds/answers.rb', __FILE__)

p "Creating People"
chris_person_admin = Person.create!({:first_name => "Chris", :last_name => "Doyle", :dont_validate_residence => true}, :without_protection => true)
c_issuer_person = Person.create!({:bio => "Chris has been doing this for 30 years.  'nuf said.", :first_name => "Chris", :last_name => "Issuer", :title => "Co Founder", :dont_validate_residence => true}, :without_protection => true)
c_issuer_person.profile_picture.store!(File.open(File.join(Rails.root, 'db/file_uploads/clyde.jpg')))
c_issuer_person.save!

c_investor_person = Person.create!({:first_name => "Chris", :last_name => "Investor", :dont_validate_residence => true}, :without_protection => true)
c_investor_person.profile_picture.store!(File.open(File.join(Rails.root, 'db/file_uploads/clyde.jpg')))
c_investor_person.save!

steve_person_admin = Person.create!({:bio => "Steve Reaser is a serial entrepreneur and seed-stage investor who has recently moved west to join the Boulder startup community. Most recently Steve co-founded an educational technology company in Raleigh, NC, which has grown profitably to over one million users by cultivating an engaged community of instructors. Steve holds a BS from Cornell University.", :first_name => "Steve", :last_name => "Reaser", :title => "CEO", :dont_validate_residence => true}, :without_protection => true)
s_issuer_person = Person.create!({ :bio => "Steve Reaser is a serial entrepreneur and seed-stage investor who has recently moved west to join the Boulder startup community. Most recently Steve co-founded an educational technology company in Raleigh, NC, which has grown profitably to over one million users by cultivating an engaged community of instructors. Steve holds a BS from Cornell University.", :first_name => "Steve", :last_name => "Issuer", :title => "Co Founder", :dont_validate_residence => true}, :without_protection => true)
s_investor_person = Person.create!({:first_name => "Steve", :last_name => "Investor", :dont_validate_residence => true}, :without_protection => true )
s_investor_person.profile_picture.store!(File.open(File.join(Rails.root, 'db/file_uploads/steve_reaser.png')))
s_investor_person.save!

d_issuer_person = Person.create!({ :bio => "Dave Milliken has extensive experience in marketing, business strategy, and sales. Prior to co-founding Vim Funding, Dave was Vice President of Marketing for Smashburger, #99 in the 2011 Inc. 500. Previously he held positions with Coors Brewing Company, Louis Vuitton Moet Hennessey, The Scotts Miracle-Gro Company, Zagat Survey, and IBM. Dave holds an MBA from NYU and a BBA from Emory University.", :first_name => "Dave", :last_name => "Issuer", :title => "Co Founder", :dont_validate_residence => true}, :without_protection => true)
d_investor_person = Person.create!({:first_name => "Dave", :last_name => "Investor", :dont_validate_residence => true}, :without_protection => true)

shane_issuer_person = Person.create!({ :bio => "Shane Fleenor has five years of large law firm experience in New York City, where he represented clients as diverse as multi-billion dollar private equity and hedge fund sponsors, to publicly traded companies, to high net worth family offices.  This legal experience enables him to navigate the complex web of securities laws that Vim Funding and its clients will face together.   Shane holds a J.D. cum laude from Harvard Law School and a B.A. with Distinction from the University of Virginia.", :first_name => "Shane", :last_name => "Issuer", :title => "Co Founder", :dont_validate_residence => true}, :without_protection => true)
shane_investor_person = Person.create!({:first_name => "Shane", :last_name => "Investor", :dont_validate_residence => true}, :without_protection => true)

# kyle_person_admin = Person.create!({:first_name => "Kyle", :last_name => "Lamy", :dont_validate_residence => true}, :without_protection => true)
# k_issuer_person = Person.create!({ :bio => "Kyle Lamy has experience developing software for private and government clients in a variety of technical languages. His experience and effective communication skills on both the technical and business level have allowed him to provide services to a variety of well known organizations, including Red Hat, Capital One, Advanced Auto Parts, the U.S. Army and the FBI. Kyle holds a B.S. cum laude from Virginia Tech.", :first_name => "Kyle", :last_name => "Issuer", :title => "Co Founder", :dont_validate_residence => true}, :without_protection => true)
# k_issuer_person.profile_picture.store!(File.open(File.join(Rails.root, 'db/file_uploads/kyle_pic.jpg')))
# k_issuer_person.save!
# 
# k_investor_person = Person.create!({:first_name => "Kyle", :last_name => "Investor", :dont_validate_residence => true}, :without_protection => true )
# k_investor_person.profile_picture.store!(File.open(File.join(Rails.root, 'db/file_uploads/frog.jpg')))
# k_investor_person.save!

p "Creating Users"
c_admin_user = chris_person_admin.create_user({:email => "chris@fundinglaunchpad.com", :password => "F5n*rb$ZX$", :password_confirmation => "F5n*rb$ZX$"}, :without_protection => true)
c_admin_user.add_role(:vim_admin)

c_issuer = c_issuer_person.create_user({:email => "chris-issuer@fundinglaunchpad.com", :password => "F5n*rb$ZX$", :password_confirmation => "F5n*rb$ZX$"}, :without_protection => true)
c_investor = c_investor_person.create_user({:email => "chris-investor@fundinglaunchpad.com", :password => "F5n*rb$ZX$", :password_confirmation => "F5n*rb$ZX$"}, :without_protection => true)

s_admin_user = steve_person_admin.create_user({:email => "steve@fundinglaunchpad.com", :password => "F5n*rb$ZX$", :password_confirmation => "F5n*rb$ZX$"}, :without_protection => true)
s_admin_user.add_role(:vim_admin)
s_issuer = s_issuer_person.create_user({:email => "steve-issuer@fundinglaunchpad.com", :password => "F5n*rb$ZX$", :password_confirmation => "F5n*rb$ZX$"}, :without_protection => true)
s_investor = s_investor_person.create_user({:email => "steve-investor@fundinglaunchpad.com", :password => "F5n*rb$ZX$", :password_confirmation => "F5n*rb$ZX$"}, :without_protection => true)

d_issuer = d_issuer_person.create_user({:email => "dave-issuer@fundinglaunchpad.com", :password => "F5n*rb$ZX$", :password_confirmation => "F5n*rb$ZX$"}, :without_protection => true)
d_investor = d_investor_person.create_user({ :email => "dave-investor@fundinglaunchpad.com", :password => "F5n*rb$ZX$", :password_confirmation => "F5n*rb$ZX$"}, :without_protection => true)

shane_issuer = shane_issuer_person.create_user({:email => "shane-issuer@fundinglaunchpad.com", :password => "F5n*rb$ZX$", :password_confirmation => "F5n*rb$ZX$"}, :without_protection => true)
shane_investor = shane_investor_person.create_user({ :email => "shane-investor@fundinglaunchpad.com", :password => "F5n*rb$ZX$", :password_confirmation => "F5n*rb$ZX$"}, :without_protection => true)

# k_admin_user = kyle_person_admin.create_user({:email => "kyle@fundinglaunchpad.com", :password => "F5n*rb$ZX$", :password_confirmation => "F5n*rb$ZX$"}, :without_protection => true)
# k_admin_user.add_role(:vim_admin)
# 
# k_issuer = k_issuer_person.create_user({:email => "kyle-issuer@fundinglaunchpad.com", :password => "F5n*rb$ZX$", :password_confirmation => "F5n*rb$ZX$"}, :without_protection => true)
# k_investor = k_investor_person.create_user({ :email => "kyle-investor@fundinglaunchpad.com", :password => "F5n*rb$ZX$", :password_confirmation => "F5n*rb$ZX$"}, :without_protection => true)

users = [c_admin_user, c_issuer, c_investor, s_admin_user, s_issuer, s_investor, d_issuer, d_investor, shane_issuer, shane_investor]

users.each do |user|
  p user.email
  user.confirm!
end

p "Creating offerings"
c = Offering.create!({:contact_id => c_issuer.id, :name => "Funding Launchpad Launch", :short_description => "Need capital to grow your startup or small business? Investment crowdfunding legislation is stalled in Congress, but Funding Launchpad can help you raise up to $1 million from consumer investors in most states, today.\r\n\r\nOnce you have obtained the proper registration, you will be able to legally reach out to your fans and customers -- including non accredited investors -- and engage them as investors in your business.\r\n\r\nIf you have a compelling story and the ability to reach an audience of fans, friends, and customers, then you can crowdfund your business today.", :status => 14, :activated_at => 30.days.ago, :share_price => 1, :min_shares => 100000, :max_shares => 150000, :shares_issued => 100, :min_investment => 2}, :without_protection => true)
c.logo.store!(File.open(File.join(Rails.root, 'app/assets/images/square_color_logo.png')))
c.save!

c2 = Offering.create!({:contact_id => c_issuer.id, :name => "Crowd CoWorking Space", :short_description => "We want to create a coworking space that is focused on people involved with any aspect of 'the crowd', whether that is crowdfunding, crowdsourcing, crowdmobbing, etc.. We hope you will join us in it!", :status => 264, :activated_at => 20.days.ago, :share_price => 2, :min_shares => 20000, :max_shares => 80000, :shares_issued => 0, :min_investment => 2}, :without_protection => true)
c2.logo.store!(File.open(File.join(Rails.root, 'db/file_uploads/boston_city_flow.jpg')))
c2.expiration_date = "#{(Date.today + 5.days).strftime('%m/%d/%Y')}"
c2.save!

c3 = Offering.create!({:contact_id => c_issuer.id, :name => "Travelers Unite", :short_description => "We want to change the way that people travel by making the travel experience social. Instead of traveling exclusively with friends, we match you up with travel companions based on who you are.", :status => 264, :activated_at => 30.days.ago, :share_price => 1, :min_shares => 3000, :max_shares => 9000, :shares_issued => 0, :min_investment => 2}, :without_protection => true)
c3.expiration_date = "#{(Date.today + 1.days).strftime('%m/%d/%Y')}"
c3.logo.store!(File.open(File.join(Rails.root, 'db/file_uploads/ayers_rock.jpg')))
c3.save!

c4 = Offering.create!({:contact_id => c_issuer.id, :name => "RandomSPACE", :short_description => "Exceptional residential construction and renovation", :status => 264, :activated_at => 30.days.ago, :share_price => 1, :min_shares => 3000, :max_shares => 9000, :shares_issued => 0, :min_investment => 2, :investments_enabled => false, :contributions_enabled => true, :rewards_enabled => true, :contribution_goal => 100000}, :without_protection => true)
c4.expiration_date = "#{(Date.today + 5.days).strftime('%m/%d/%Y')}"
c4.logo.store!(File.open(File.join(Rails.root, 'db/file_uploads/randomspace.gif')))
c4.save!

p "Giving users offering admin roles"

c_issuer.add_role(:offering_admin, c)
c_issuer.add_role(:offering_admin, c2)
c_issuer.add_role(:offering_admin, c3)
c_issuer.add_role(:offering_admin, c4)

s_issuer.add_role(:offering_admin, c)
s_issuer.add_role(:offering_admin, c2)
s_issuer.add_role(:offering_admin, c3)

d_issuer.add_role(:offering_admin, c)
d_issuer.add_role(:offering_admin, c2)
d_issuer.add_role(:offering_admin, c3)

shane_issuer.add_role(:offering_admin, c)
shane_issuer.add_role(:offering_admin, c2)
shane_issuer.add_role(:offering_admin, c3)

p "Creating companies"

c.create_company({:name => "Funding Launchpad", :description => "We are going to revolutionize fundraising"})
c2.create_company({:name => "Metal Widgets", :description => "We make things that help you make things."})
c4.create_company({:name => "RandomSPACE", :description => "Exceptional builders"})
Email.create(:content_type => "Company", :content => "chris@fundinglaunchpad.com", :emailable_id => c.company.id, :emailable_type => "Company")
Email.create(:content_type => "Company", :content => "chris@fundinglaunchpad.com", :emailable_id => c4.company.id, :emailable_type => "Company")

p "Creating events"
c.events.create({:description => "Come join us in this webinar where we will discuss the upcoming crowdfunding legislation and how that impacts you as a small business owner. ", :start_date => 'Wed, 23 May 2012 10:00:00 UTC +00:00', :end_date => 'Wed, 23 May 2012 11:00:00 UTC +00:00', :title => "Webinar"}, without_protection: true)
c.events.create({:description => "We will be giving a talk about funding options for entrepreneurs and diving into the new crowdfunding law.  ", :start_date => 'Fri, 25 May 2012 17:30:00 UTC +00:00', :title => "Boulder New Tech"}, without_protection: true)
c.events.create({:description => "Join us at the TedX event where we will discuss our platform and how to raise money from the crowd.", :start_date => 'Thu, 14 Jun 2012 10:30:00 UTC +00:00', :title => "Tedx"}, without_protection: true)

p "Creating filings"
c.filings.create(:state => "CO")
c.filings.create(:state => "CA")
c2.filings.create(:state => "CO")
c2.filings.create(:state => "NC")

p "Creating company addresses"
c.company.create_address(:content_type => "Company", :city => "Boulder", :state => "CO", :zip_code => "80301")
c2.company.create_address(:content_type => "Company")
c4.company.create_address(:content_type => "Company", :city => "Boulder", :state => "CO", :zip_code => "80304")

p "Creating person addresses"
c_issuer_person.addresses.create(:content_type => "Residence", :state => "CO", :country_code => "US")
c_investor_person.addresses.create(:content_type => "Residence", :state => "CA", :country_code => "US")
s_investor_person.addresses.create(:content_type => "Residence", :state => "CO", :country_code => "US")
d_investor_person.addresses.create(:content_type => "Residence", :state => "CO", :country_code => "US")
shane_investor_person.addresses.create(:content_type => "Residence", :state => "CO", :country_code => "US")

p "Creating questions"
q1 = c_investor.questions.create(:offering_id => c.id, :content => "When is this going to be ready?", :created_at => 4.days.ago)
q2 = s_investor.questions.create(:offering_id => c.id, :content => "Are you excited to get this platform live?", :created_at => 2.days.ago)
q3 = shane_investor.questions.create(:offering_id => c.id, :content => "How does the new crowdfunding law change the investing landscape for small business?", :created_at => 3.days.ago)
q4 = s_investor.questions.create(:offering_id => c.id, :content => "Is there anything you can see as a roadblock to your success?", :created_at => 3.days.ago)
q5 = d_investor.questions.create(:offering_id => c.id, :content => "I looked at your U7. Can you please explain paragraph 4 on page 30 in more detail?", :created_at => 2.days.ago)
q6 = c_investor.questions.create(:offering_id => c.id, :content => "What do you like most about crowd funding?", :created_at => 1.days.ago)

p "Creating answers"
c_issuer.answers.create(:question_id => q1.id, :content => "It's coming soon!", :created_at => 1.minute.ago)
c_issuer.answers.create(:question_id => q2.id, :content => "Yes, We are really excited to get this launched!", :created_at => 1.days.ago)
s_issuer.answers.create(:question_id => q2.id, :content => "As Kyle said, we are really really excited.", :created_at => 12.hours.ago)
s_investor.answers.create(:question_id => q5.id, :content => "It seems pretty self explanatory to me...", :created_at => 2.days.ago)
c_issuer.answers.create(:question_id => q5.id, :content => "Sure, all it really means is that we are taking the proper steps to mitigate our risk.", :created_at => 1.days.ago)

p "Creating documents"
d = Document.new(:offering_id => c.id, :description => "Form U-7 for SCOR")
d.document.store!(File.open(File.join(Rails.root, 'db/file_uploads/u7_doc.pdf')))
d.save!

d = Document.new(:offering_id => c.id, :description => "Regulatory Approval Letter")
d.document.store!(File.open(File.join(Rails.root, 'db/file_uploads/cover-letter.pdf')))
d.save!

d = Document.new(:offering_id => c.id, :description => "Subscription Agreement")
d.document.store!(File.open(File.join(Rails.root, 'db/file_uploads/cover-letter.pdf')))
d.save!

d = Document.new(:offering_id => c.id, :description => "Escrow Agreement")
d.document.store!(File.open(File.join(Rails.root, 'db/file_uploads/cover-letter.pdf')))
d.save!

p "Creating investments"
inv1 = c_investor.investments.create(:offering_id => c.id, :offered_amount => 10000, :status => Investment.initiated, :comment => "They have a solid business plan, positive cash flow and I am excited about their product!", :anonymous => true)
inv2 = c_investor.investments.create(:offering_id => c.id, :offered_amount => 500, :status => Investment.initiated)
inv3 = c_investor.investments.create(:offering_id => c.id, :offered_amount => 2600, :status => Investment.initiated, :comment => "I think this is a great concept and I am impressed with the management team and their backgrounds!", :anonymous => false)

p "Creating links"
c3.links.create(:link => "http://www.randomspace.com", :description => "What once was")
c.links.create(:link => "http://www.fundinglaunchpad.com/blog", :description => "Our Blog")

p "Creating company phones"
Phone.create!(:content_type => "Company", :content => "555-123-1234", :phonable_id => c.company.id, :phonable_type => "Company")
Phone.create!(:content_type => "Company", :content => "804-555-1234", :phonable_id => c2.company.id, :phonable_type => "Company")

p "Creating photos"
photo = Photo.new( :offering_id => c.id, :caption => "New Zealand")
photo.save(:validate => false)
photo.image.store!(File.open(File.join(Rails.root, 'db/file_uploads/IMG_0868.jpg')))
photo.save!

photo1 = Photo.new(:offering_id => c.id, :caption => "Sunset")
photo1.save(:validate => false)
photo1.image.store!(File.open(File.join(Rails.root, 'db/file_uploads/sunset.jpg')))
photo1.save!

photo2 = Photo.new(:offering_id => c3.id, :caption => "Seals")
photo2.save(:validate => false)
photo2.image.store!(File.open(File.join(Rails.root, 'db/file_uploads/seals.jpg')))
photo2.save!

photo3 = Photo.new(:offering_id => c2.id, :caption => "Auckland, New Zealand")
photo3.save(:validate => false)
photo3.image.store!(File.open(File.join(Rails.root, 'db/file_uploads/auckland.jpg')))
photo3.save!

p "Creating videos"
c.videos.create(:link => "http://www.youtube.com/watch?v=ZUG9qYTJMsI", :description => "Razor Business")

p "Creating updates"
c.updates.create(:person_id => c_issuer.id, :subject => "Thank You", :content => "<h2>We are making progress</h2>Thanks to everyone for the support!<br>", :created_at => 4.days.ago)
c.updates.create(:person_id => c_issuer.id, :subject => "Here we go!", :content => "<img src=\"http://www.kylelamyphoto.com/Virginia/Cold-Mountain/i-R47QtSN/1/X2/ColdMountain96-X2.jpg\" alt=\"\"><br><h1>Heading 1 is here</h1>This is normal<br><br><h2>Heading 2 is here</h2>This is normal<br><i>This is italic</i><br><ul><li>hey</li><li>test<br></li></ul>", :created_at => 2.days.ago)
c.updates.create(:person_id => c_issuer.id, :subject => "Are you ready?", :content => "<img src=\"http://www.kylelamyphoto.com/South-East-Asia/Thailand/Chiang-Mai/-/158107976_mSi3S-L-2.jpg\" alt=\"\"><br><br><h2>We are moving quickly</h2>In case you haven't been keeping up with our progress, let me just fill you in a bit. We are quickly approaching our MVP for funding launchpad and we are excited to begin getting clients on the platform. The JOBS act was passed recently and signed into law, so there will be new rules coming from the SEC concerning the crowdfunding legislation. We will keep you posted on what that means for us and for you, so stay tuned!<br><br>Sincerely,<br>Funding Launchpad Team<br>", :created_at => 30.seconds.ago)

