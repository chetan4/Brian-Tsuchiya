# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130331020715) do

  create_table "addresses", :force => true do |t|
    t.string   "content_type"
    t.integer  "addressable_id"
    t.string   "addressable_type"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "zip_code"
    t.string   "country_code"
  end

  create_table "answers", :force => true do |t|
    t.integer  "question_id"
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "auction_preferences", :force => true do |t|
    t.integer  "user_id"
    t.integer  "auction_id"
    t.boolean  "auto_bid"
    t.decimal  "max_bid"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.boolean  "max_bid_notification"
    t.decimal  "current_bid"
  end

  create_table "auctions", :force => true do |t|
    t.integer  "offering_id"
    t.string   "title"
    t.text     "description"
    t.integer  "primary_photo_id"
    t.decimal  "price"
    t.decimal  "base_cost"
    t.string   "base_cost_description"
    t.decimal  "ancillary_cost"
    t.string   "ancillary_cost_description"
    t.string   "note"
    t.boolean  "enabled"
    t.string   "delivery_method"
    t.datetime "auction_end_time"
    t.decimal  "bid_increment"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "authentications", :force => true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "bid_increments", :force => true do |t|
    t.decimal  "min_price"
    t.decimal  "max_price"
    t.decimal  "bid_increment"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "bids", :force => true do |t|
    t.integer  "auction_id"
    t.integer  "user_id"
    t.decimal  "amount"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "offering_id"
    t.string   "tax_id"
  end

  add_index "companies", ["offering_id"], :name => "index_companies_on_offering_id"

  create_table "contacts", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "business_state"
    t.boolean  "investor_ready"
    t.text     "description"
    t.string   "industry"
    t.string   "referral"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "twitter_username"
    t.string   "facebook_url"
    t.integer  "raise_amount"
  end

  create_table "contributions", :force => true do |t|
    t.integer  "offering_id"
    t.integer  "user_id"
    t.decimal  "amount",          :precision => 10, :scale => 0
    t.string   "payment_method"
    t.string   "status"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.string   "account_number"
    t.string   "account_name"
    t.string   "expiration_date"
    t.string   "security_code"
  end

  create_table "default_following_options", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "new_offering_update",      :default => true
    t.boolean  "new_offering_qa",          :default => false
    t.boolean  "investment_offered",       :default => false
    t.boolean  "investment_accepted",      :default => false
    t.boolean  "investment_declined",      :default => false
    t.boolean  "investment_funds_sent",    :default => false
    t.boolean  "investment_escrowed",      :default => false
    t.boolean  "investment_invested",      :default => false
    t.boolean  "investment_returned",      :default => false
    t.boolean  "offering_funded",          :default => true
    t.boolean  "offering_expired",         :default => true
    t.boolean  "offering_content_changed", :default => false
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
  end

  create_table "details", :force => true do |t|
    t.string   "content_type"
    t.string   "content"
    t.integer  "detailable_id"
    t.string   "detailable_type"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "document_viewers", :id => false, :force => true do |t|
    t.integer  "document_id"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "document_viewers", ["document_id", "user_id"], :name => "index_document_viewers_on_document_id_and_user_id"

  create_table "documents", :force => true do |t|
    t.integer  "offering_id"
    t.text     "description"
    t.boolean  "required"
    t.string   "document"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "emails", :force => true do |t|
    t.string   "content_type"
    t.string   "content"
    t.integer  "emailable_id"
    t.string   "emailable_type"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "events", :force => true do |t|
    t.text     "description"
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "title"
    t.integer  "offering_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "events", ["offering_id"], :name => "index_events_on_campaign_id"

  create_table "filings", :force => true do |t|
    t.string   "state"
    t.integer  "offering_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "filings", ["offering_id"], :name => "index_filings_on_campaign_id"

  create_table "followers", :force => true do |t|
    t.integer  "offering_id"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "following_options", :force => true do |t|
    t.integer  "offering_id"
    t.integer  "user_id"
    t.boolean  "new_offering_update",      :default => false
    t.boolean  "new_offering_qa",          :default => false
    t.boolean  "investment_offered",       :default => false
    t.boolean  "investment_accepted",      :default => false
    t.boolean  "investment_declined",      :default => false
    t.boolean  "investment_funds_sent",    :default => false
    t.boolean  "investment_escrowed",      :default => false
    t.boolean  "investment_invested",      :default => false
    t.boolean  "investment_returned",      :default => false
    t.boolean  "offering_funded",          :default => false
    t.boolean  "offering_expired",         :default => false
    t.boolean  "offering_content_changed", :default => false
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
  end

  create_table "followings", :force => true do |t|
    t.integer  "user_id"
    t.integer  "offering_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "friendly_id_slugs", :force => true do |t|
    t.string   "slug",                         :null => false
    t.integer  "sluggable_id",                 :null => false
    t.string   "sluggable_type", :limit => 40
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type"], :name => "index_friendly_id_slugs_on_slug_and_sluggable_type", :unique => true
  add_index "friendly_id_slugs", ["sluggable_id"], :name => "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], :name => "index_friendly_id_slugs_on_sluggable_type"

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.string   "subdomain"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "slug"
  end

  add_index "groups", ["slug"], :name => "index_groups_on_slug", :unique => true
  add_index "groups", ["subdomain"], :name => "index_groups_on_subdomain", :unique => true

  create_table "investment_statuses", :force => true do |t|
    t.integer  "investment_id"
    t.string   "status"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "investment_totals", :force => true do |t|
    t.integer  "amount",     :default => 0
    t.boolean  "show",       :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "investments", :force => true do |t|
    t.integer  "offering_id",                                    :null => false
    t.integer  "user_id",                                        :null => false
    t.decimal  "offered_amount",  :precision => 10, :scale => 2
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.integer  "payment_id"
    t.decimal  "accepted_amount", :precision => 10, :scale => 2
    t.string   "status"
    t.text     "comment"
    t.boolean  "anonymous"
  end

  create_table "invitations", :force => true do |t|
    t.string   "email"
    t.string   "name"
    t.date     "sent_on"
    t.date     "accepted_on"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "company_id"
    t.string   "registration_key"
    t.string   "password"
  end

  create_table "links", :force => true do |t|
    t.integer  "offering_id"
    t.string   "link"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "offering_versions", :force => true do |t|
    t.string   "item_type",      :null => false
    t.integer  "item_id",        :null => false
    t.string   "event",          :null => false
    t.string   "whodunnit"
    t.text     "object"
    t.text     "object_changes"
    t.datetime "created_at"
  end

  add_index "offering_versions", ["item_type", "item_id"], :name => "index_offering_versions_on_item_type_and_item_id"

  create_table "offerings", :force => true do |t|
    t.integer  "contact_id"
    t.string   "name"
    t.text     "short_description"
    t.text     "long_description"
    t.integer  "status",                                                          :default => 0
    t.decimal  "confirmed_investment",             :precision => 10, :scale => 2
    t.date     "desired_start_date"
    t.datetime "activated_at"
    t.datetime "created_at",                                                                         :null => false
    t.datetime "updated_at",                                                                         :null => false
    t.string   "logo"
    t.text     "instructions"
    t.date     "expiration_date"
    t.decimal  "share_price",                      :precision => 10, :scale => 2, :default => 0.0
    t.integer  "min_shares",                                                      :default => 0
    t.integer  "max_shares",                                                      :default => 0
    t.integer  "shares_issued",                                                   :default => 0
    t.boolean  "ppm",                                                             :default => false
    t.boolean  "private",                                                         :default => false
    t.string   "slug"
    t.decimal  "min_investment",                   :precision => 10, :scale => 2, :default => 1.0
    t.string   "edit_summary"
    t.integer  "group_id"
    t.boolean  "debt",                                                            :default => false
    t.integer  "term"
    t.decimal  "interest_rate",                    :precision => 10, :scale => 0
    t.integer  "repayment"
    t.integer  "minimum_goal"
    t.boolean  "rewards_enabled",                                                 :default => false
    t.string   "rewards_display_orientation",                                     :default => "V"
    t.string   "rewards_horizontal_alignment",                                    :default => "C"
    t.integer  "rewards_max_displayed",                                           :default => 99
    t.integer  "rewards_default_auction_duration",                                :default => 30
    t.boolean  "seeking_contributions",                                           :default => false
    t.string   "rewards_gift_heading"
    t.string   "rewards_gift_subheading"
    t.string   "rewards_coupon_heading"
    t.string   "rewards_coupon_subheading"
    t.string   "rewards_auction_heading"
    t.string   "rewards_auction_subheading"
    t.boolean  "contributions_enabled",                                           :default => false
    t.decimal  "contribution_goal",                :precision => 10, :scale => 0, :default => 0
    t.boolean  "investments_enabled",                                             :default => true
    t.boolean  "show_update_count",                                               :default => true
    t.boolean  "show_investor_count",                                             :default => true
    t.boolean  "show_social_media",                                               :default => true
    t.boolean  "show_progress_bar",                                               :default => true
    t.string   "access_token"
    t.string   "publishable_key"
  end

  add_index "offerings", ["group_id"], :name => "index_offerings_on_group_id"
  add_index "offerings", ["slug"], :name => "index_campaigns_on_slug"

  create_table "offerings_followings", :id => false, :force => true do |t|
    t.integer "offering_id"
    t.integer "following_id"
  end

  create_table "offerings_people", :id => false, :force => true do |t|
    t.integer "offering_id"
    t.integer "person_id"
  end

  create_table "pages", :force => true do |t|
    t.string   "name"
    t.text     "content"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "title"
  end

  create_table "people", :force => true do |t|
    t.integer  "project_id"
    t.text     "bio"
    t.string   "profile_picture"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "title"
    t.integer  "offering_id"
    t.boolean  "accredited"
  end

  create_table "phones", :force => true do |t|
    t.string   "content_type"
    t.string   "content"
    t.integer  "phonable_id"
    t.string   "phonable_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "photos", :force => true do |t|
    t.integer  "offering_id"
    t.string   "caption"
    t.string   "image"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.boolean  "default"
  end

  create_table "press", :force => true do |t|
    t.string   "title"
    t.string   "link"
    t.text     "description"
    t.datetime "published_date"
    t.string   "publisher_logo"
    t.boolean  "publish",        :default => false
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.boolean  "feature"
  end

  create_table "questions", :force => true do |t|
    t.integer  "offering_id"
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "reward_fulfillment_tasks", :force => true do |t|
    t.integer  "fulfillment_id"
    t.string   "status"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "reward_fulfillments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "reward_id"
    t.integer  "address_id"
    t.string   "status"
    t.string   "user_note"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "reward_payments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "reward_id"
    t.string   "payment_method"
    t.decimal  "amount",         :precision => 10, :scale => 0
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
  end

  create_table "rewards", :force => true do |t|
    t.integer  "offering_id"
    t.decimal  "price",                      :precision => 10, :scale => 0
    t.integer  "number_available"
    t.integer  "number_granted",                                            :default => 0
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",                                                               :null => false
    t.datetime "updated_at",                                                               :null => false
    t.decimal  "base_cost",                  :precision => 10, :scale => 0, :default => 0
    t.string   "base_cost_description"
    t.decimal  "ancillary_cost",             :precision => 10, :scale => 0, :default => 0
    t.string   "ancillary_cost_description"
    t.string   "note"
    t.boolean  "enabled"
    t.string   "reward_type"
    t.string   "delivery_method"
    t.string   "short_description"
    t.string   "photo"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "team_members", :force => true do |t|
    t.string   "name"
    t.string   "title"
    t.text     "bio"
    t.string   "picture"
    t.integer  "offering_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "position"
  end

  create_table "update_versions", :force => true do |t|
    t.string   "item_type",      :null => false
    t.integer  "item_id",        :null => false
    t.string   "event",          :null => false
    t.string   "whodunnit"
    t.text     "object"
    t.text     "object_changes"
    t.datetime "created_at"
  end

  add_index "update_versions", ["item_type", "item_id"], :name => "index_update_versions_on_item_type_and_item_id"

  create_table "updates", :force => true do |t|
    t.integer  "offering_id"
    t.integer  "person_id"
    t.string   "subject"
    t.text     "content"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "edit_summary"
  end

  create_table "user_rewards", :force => true do |t|
    t.integer  "user_id"
    t.integer  "reward_id"
    t.decimal  "price",              :precision => 10, :scale => 0
    t.datetime "created_at",                                                       :null => false
    t.datetime "updated_at",                                                       :null => false
    t.integer  "address_id"
    t.string   "payment_method"
    t.string   "fulfillment_status"
    t.string   "user_delivery_note"
    t.integer  "offering_id"
    t.string   "email"
    t.integer  "quantity",                                          :default => 1
    t.string   "reward_type"
    t.datetime "fulfilled_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                :default => "",    :null => false
    t.string   "encrypted_password",                   :default => ""
    t.datetime "created_at",                                              :null => false
    t.datetime "updated_at",                                              :null => false
    t.integer  "access",                               :default => 0
    t.integer  "person_id"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "invitation_token",       :limit => 60
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.boolean  "newsletter",                           :default => false
    t.boolean  "pcp_email",                            :default => false
    t.integer  "invitation_offering"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["invitation_token"], :name => "index_users_on_invitation_token"
  add_index "users", ["invited_by_id"], :name => "index_users_on_invited_by_id"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

  create_table "videos", :force => true do |t|
    t.text     "link"
    t.text     "link_html"
    t.string   "description"
    t.integer  "offering_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.boolean  "default"
  end

  add_index "videos", ["offering_id"], :name => "index_videos_on_campaign_id"

end
