class RenameCampaignTables < ActiveRecord::Migration
  def change
    rename_table :campaigns_followings, :offerings_followings
    rename_table :campaigns_people, :offerings_people
  end
end
