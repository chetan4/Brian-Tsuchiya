class RenameCampaignToOffering < ActiveRecord::Migration
  def change
    rename_table :campaigns, :offerings
    rename_column :campaigns_followings, :campaign_id, :offering_id
    rename_column :campaigns_people, :campaign_id, :offering_id
    rename_column :documents, :campaign_id, :offering_id
    rename_column :events, :campaign_id, :offering_id
    rename_column :filings, :campaign_id, :offering_id
    rename_column :followers, :campaign_id, :offering_id
    rename_column :following_options, :campaign_id, :offering_id
    rename_column :followings, :campaign_id, :offering_id
    rename_column :investments, :campaign_id, :offering_id
    rename_column :links, :campaign_id, :offering_id
    rename_column :people, :campaign_id, :offering_id
    rename_column :photos, :campaign_id, :offering_id
    rename_column :questions, :campaign_id, :offering_id
    rename_column :rewards, :campaign_id, :offering_id
    rename_column :team_members, :campaign_id, :offering_id
    rename_column :updates, :campaign_id, :offering_id
    rename_column :videos, :campaign_id, :offering_id
  end
end
