class CreateAuctions < ActiveRecord::Migration
  def change
    create_table :auctions do |t|
      t.integer :offering_id
      t.string :title
      t.text :description
      t.integer :primary_photo_id
      t.decimal :price
      t.decimal :base_cost
      t.string :base_cost_description
      t.decimal :ancillary_cost
      t.string :ancillary_cost_description
      t.string :note
      t.boolean :enabled
      t.string :delivery_method
      t.datetime :auction_end_time
      t.decimal :bid_increment

      t.timestamps
    end
  end
end
