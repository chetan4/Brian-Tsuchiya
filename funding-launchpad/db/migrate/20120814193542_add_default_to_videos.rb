class AddDefaultToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :default, :boolean
  end
end
