class SetDefaultForProjectsPledged < ActiveRecord::Migration
  def up
    change_column :projects, :pledged, :decimal, { :default => 0.00 }
  end

  def down
  end
end
