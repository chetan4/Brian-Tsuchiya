class AddColumnsToInvitations < ActiveRecord::Migration
  def change
    add_column :invitations, :registration_key, :string
    add_column :invitations, :password, :string
  end
end
