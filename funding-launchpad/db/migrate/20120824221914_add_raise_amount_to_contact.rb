class AddRaiseAmountToContact < ActiveRecord::Migration
  def change
    add_column :contacts, :raise_amount, :integer
  end
end
