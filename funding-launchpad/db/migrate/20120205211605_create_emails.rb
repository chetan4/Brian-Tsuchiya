class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string :content_type
      t.string :content
      t.references :emailable, :polymorphic => true
      t.timestamps
    end
  end
end
