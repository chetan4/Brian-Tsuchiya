class AddMaxBidNotificationToAuctionPreferences < ActiveRecord::Migration
  def change
    add_column :auction_preferences, :max_bid_notification, :boolean
  end
end
