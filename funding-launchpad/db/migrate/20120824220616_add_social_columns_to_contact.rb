class AddSocialColumnsToContact < ActiveRecord::Migration
  def change
    add_column :contacts, :twitter_username, :string
    add_column :contacts, :facebook_url, :string
  end
end
