class AddDefaultValueToAwardedInAwards < ActiveRecord::Migration
  def up
    change_column :rewards, :awarded, :integer, :default => 0
  end
  
  def down
    change_column :rewards, :awarded, :integer
  end
end
