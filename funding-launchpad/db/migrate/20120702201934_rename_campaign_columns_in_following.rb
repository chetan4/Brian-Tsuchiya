class RenameCampaignColumnsInFollowing < ActiveRecord::Migration
  def change
    rename_column :default_following_options, :new_campaign_update, :new_offering_update
    rename_column :default_following_options, :new_campaign_qa, :new_offering_qa
    rename_column :default_following_options, :campaign_funded, :offering_funded
    rename_column :default_following_options, :campaign_expired, :offering_expired
    rename_column :default_following_options, :campaign_content_changed, :offering_content_changed
  end
end
