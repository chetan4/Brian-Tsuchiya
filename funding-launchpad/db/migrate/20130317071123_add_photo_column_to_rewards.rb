class AddPhotoColumnToRewards < ActiveRecord::Migration
  def change
    add_column :rewards, :photo, :string
  end
end
