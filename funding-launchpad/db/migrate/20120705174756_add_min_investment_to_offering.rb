class AddMinInvestmentToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :min_investment, :decimal, :precision => 10, :scale => 2, :default => 1.00
  end
end
