class RenameNameToDocumentInDocuments < ActiveRecord::Migration
  def up
    rename_column :documents, :name, :document
  end

  def down
    rename_column :documents, :document, :name
  end
end
