class AddLogoToProjectLayouts < ActiveRecord::Migration
  def change
    add_column :project_layouts, :logo_image_url, :string

  end
end
