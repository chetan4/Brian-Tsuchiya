class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.integer :company_id
      t.integer :contact_id
      t.string :name
      t.text :short_description
      t.text :long_description
      t.integer :status
      t.decimal :minimum_goal, :precision => 10, :scale => 2
      t.decimal :maximum_goal, :precision => 10, :scale => 2
      t.decimal :confirmed_investment, :precision => 10, :scale => 2
      t.date :desired_start_date
      t.date :activation_date
      t.integer :duration

      t.timestamps
    end
  end
end
