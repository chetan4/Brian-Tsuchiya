class AddEditSummaryToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :edit_summary, :string
  end
end
