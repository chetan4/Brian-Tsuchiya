class CreateDefaultFollowingOptions < ActiveRecord::Migration
  def change
    create_table :default_following_options do |t|
      t.integer :user_id
      t.boolean :new_campaign_update, :default => false
      t.boolean :new_campaign_qa, :default => false
      t.boolean :investment_offered, :default => false
      t.boolean :investment_accepted, :default => false
      t.boolean :investment_declined, :default => false
      t.boolean :investment_funds_sent, :default => false
      t.boolean :investment_escrowed, :default => false
      t.boolean :investment_invested, :default => false
      t.boolean :investment_returned, :default => false
      t.boolean :campaign_funded, :default => false
      t.boolean :campaign_expired, :default => false
      t.boolean :campaign_content_changed, :default => false
      t.timestamps
    end
  end
end
