class AddShowInvestorCountColumnToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :show_investor_count, :boolean, :default => true
  end
end
