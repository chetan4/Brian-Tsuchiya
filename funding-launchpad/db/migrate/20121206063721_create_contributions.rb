class CreateContributions < ActiveRecord::Migration
  def change
    create_table :contributions do |t|
      t.integer :offering_id
      t.integer :user_id
      t.decimal :amount
      t.string :payment_type
      t.string :status

      t.timestamps
    end
  end
end
