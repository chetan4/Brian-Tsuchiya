class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|
      t.string :content_type
      t.string :content
      t.references :detailable, :polymorphic => true
      t.timestamps
    end
  end
end
