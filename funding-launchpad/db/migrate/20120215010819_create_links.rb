class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.integer :campaign_id
      t.string :link
      t.string :description

      t.timestamps
    end
  end
end
