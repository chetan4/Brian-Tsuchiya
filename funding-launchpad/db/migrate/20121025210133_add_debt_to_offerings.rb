class AddDebtToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :debt, :boolean, :default => false
    add_column :offerings, :term, :integer
    add_column :offerings, :interest_rate, :decimal
    add_column :offerings, :repayment, :integer
    add_column :offerings, :minimum_goal, :integer

    Offering.reset_column_information
    Rake::Task['offerings:update_minimum_goal'].invoke
  end
end
