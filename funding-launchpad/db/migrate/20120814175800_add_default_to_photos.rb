class AddDefaultToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :default, :boolean
  end
end
