class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.integer :owner_id
      t.string :company_name, :null => false
      t.text :bio, :null => false
      t.integer :minimum_goal
      t.integer :pledged
      t.string :phone_number
      t.text :address
      t.string :email
      t.string :contact
      t.boolean :visible, :default => false
      t.boolean :active, :default => false
      t.boolean :funded, :default => false
      t.boolean :closed, :default => false

      t.timestamps
    end
  end
end
