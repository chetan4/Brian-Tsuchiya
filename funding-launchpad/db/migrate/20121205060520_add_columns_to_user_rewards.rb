class AddColumnsToUserRewards < ActiveRecord::Migration
  def change
    add_column :user_rewards, :address_id, :integer
    add_column :user_rewards, :payment_method, :string
    add_column :user_rewards, :fulfillment_status, :string
    add_column :user_rewards, :user_delivery_note, :string
  end
end
