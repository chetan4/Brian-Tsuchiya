class AddEditReasonToUpdates < ActiveRecord::Migration
  def change
    add_column :updates, :edit_summary, :string
  end
end
