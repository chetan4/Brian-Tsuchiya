class ExpandNameInPeople < ActiveRecord::Migration
  def up
    remove_column :people, :name
    add_column :people, :first_name, :string
    add_column :people, :last_name, :string
  end

  def down
    remove_column :people, :first_name
    remove_column :people, :last_name
    add_column :people, name, :string
  end
end
