class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string  :name
      t.string  :email
      t.string  :phone
      t.integer :social_media_subscribers
      t.string  :business_state
      t.boolean :investor_ready
      t.text    :description
      t.string  :category
      t.string  :referral

      t.timestamps
    end
  end
end
