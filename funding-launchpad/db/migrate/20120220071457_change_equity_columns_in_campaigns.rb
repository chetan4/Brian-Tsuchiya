class ChangeEquityColumnsInCampaigns < ActiveRecord::Migration
  def up
    remove_column :campaigns, :equity
    add_column :campaigns, :min_equity, :decimal, :precision => 5, :scale => 2
    add_column :campaigns, :max_equity, :decimal, :precision => 5, :scale => 2
  end

  def down
    add_column :campaigns, :equity, :integer
    remove_column :campaigns, :min_equity
    remove_column :campaigns, :max_equity
  end
end
