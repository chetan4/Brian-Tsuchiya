class AddFieldsToAddresses < ActiveRecord::Migration
  def change
    remove_column :addresses, :content
    add_column :addresses, :address1, :string
    add_column :addresses, :address2, :string
    add_column :addresses, :city, :string
    add_column :addresses, :state, :string
    add_column :addresses, :zip_code, :string

  end
end
