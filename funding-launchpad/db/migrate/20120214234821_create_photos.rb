class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.integer :campaign_id
      t.string :caption
      t.string :image

      t.timestamps
    end
  end
end
