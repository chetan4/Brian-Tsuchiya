class CreateTeamMembers < ActiveRecord::Migration
  def change
    create_table :team_members do |t|
      t.string :name
      t.string :title
      t.text :bio
      t.string :picture
      t.references :campaign

      t.timestamps
    end
  end
end
