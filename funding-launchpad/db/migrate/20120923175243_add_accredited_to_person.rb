class AddAccreditedToPerson < ActiveRecord::Migration
  def change
    add_column :people, :accredited, :boolean
  end
end
