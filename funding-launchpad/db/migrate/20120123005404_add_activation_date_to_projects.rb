class AddActivationDateToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :activation_date, :datetime

  end
end
