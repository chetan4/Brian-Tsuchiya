class ChangeStatusColumnInCampaigns < ActiveRecord::Migration
  def up
    change_column :campaigns, :status, :integer, :default => 0
  end

  def down
    change_column :campaigns, :status, :integer
  end
end
