class CreatePress < ActiveRecord::Migration
  def change
    create_table :press do |t|
      t.string :title
      t.string :link
      t.text :description
      t.datetime :published_date
      t.string :publisher_logo
      t.boolean :publish, :default => false

      t.timestamps
    end
  end
end
