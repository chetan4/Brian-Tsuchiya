class CreateInvestmentStatuses < ActiveRecord::Migration
  def change
    create_table :investment_statuses do |t|
      t.integer :investment_id
      t.string :status

      t.timestamps
    end
  end
end
