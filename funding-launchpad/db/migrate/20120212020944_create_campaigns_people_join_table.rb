class CreateCampaignsPeopleJoinTable < ActiveRecord::Migration
  def change
    create_table :campaigns_people, :id => false do |t|
      t.integer :campaign_id
      t.integer :person_id
    end
  end
end
