class CreateInvestmentTotals < ActiveRecord::Migration
  def self.up
    create_table :investment_totals do |t|
      t.integer :amount, :default => 0
      t.boolean :show, :default => false

      t.timestamps
    end
    InvestmentTotal.reset_column_information
    InvestmentTotal.create
  end

  def self.down
    drop_table :investment_totals
  end

end
