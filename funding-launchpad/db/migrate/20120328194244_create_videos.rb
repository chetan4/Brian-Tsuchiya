class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.text :link
      t.text :link_html
      t.string :description
      t.references :campaign

      t.timestamps
    end
    add_index :videos, :campaign_id
  end
end
