class ChangeMoneyColumnsInProjects < ActiveRecord::Migration
  def up
    change_column :projects, :minimum_goal, :decimal, :precision => 10, :scale => 2
    change_column :projects, :pledged, :decimal, :precision => 10, :scale => 2
  end

  def down
    change_column :projects, :minimum_goal, :integer
    change_column :projects, :pledged, :integer
  end
end
