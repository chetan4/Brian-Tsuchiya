class ModifyShareDefaultsInCampaigns < ActiveRecord::Migration
  def up
    change_column :campaigns, :share_price, :decimal, :precision => 10, :scale => 2, :default => 0.00
    change_column :campaigns, :min_shares, :integer, :default => 0
    change_column :campaigns, :max_shares, :integer, :default => 0
    change_column :campaigns, :shares_issued, :integer, :default => 0
  end

  def down
    change_column :campaigns, :share_price, :decimal, :precision => 10, :scale => 2
    change_column :campaigns, :min_shares, :integer
    change_column :campaigns, :max_shares, :integer
    change_column :campaigns, :shares_issued, :integer
  end
end
