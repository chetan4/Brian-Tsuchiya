class ChangeRewardsColumns < ActiveRecord::Migration
  def change
    rename_column :rewards, :investment_level, :price
    add_column :rewards, :base_cost, :integer
    add_column :rewards, :base_cost_description, :string
    rename_column :rewards, :quantity, :number_available
    rename_column :rewards, :awarded, :number_granted
    add_column :rewards, :ancillary_cost, :integer
    add_column :rewards, :ancillary_cost_description, :string
    add_column :rewards, :note, :string
    add_column :rewards, :enabled, :boolean
    add_column :rewards, :reward_type, :string
  end
end
