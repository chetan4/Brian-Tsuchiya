class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.integer :project_id
      t.string :name
      t.text :bio
      t.string :image_url

      t.timestamps
    end
  end
end
