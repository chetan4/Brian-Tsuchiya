class AddInvestorCommentToInvestments < ActiveRecord::Migration
  def change
    add_column :investments, :comment, :text
    add_column :investments, :anonymous, :boolean
  end
end
