class CreateProjectLayouts < ActiveRecord::Migration
  def change
    create_table :project_layouts do |t|
      t.integer :project_id
      t.string :funding_title, :default => "Funding Information"
      t.string :short_description_title, :default => "Short Project Description"
      t.string :long_description_title, :default => "Detailed Project Description"
      t.string :people_title, :default => "People"
      t.string :picture_title, :default => "Pictures"
      t.string :file_title, :default => "Files"
      t.boolean :display_name, :default => true
      t.boolean :display_funding_info, :default => true
      t.boolean :display_contact_info, :default => true
      t.boolean :display_short_bio, :default => true
      t.boolean :display_long_bio, :default => true
      t.boolean :display_people, :default => true
      t.boolean :display_pictures, :default => true
      t.boolean :display_files, :default => true
      t.string :header_text_color, :default => "white", :null => false
      t.string :header_background_color, :default => "#b9bbc0", :null => false
      t.string :header_background_image
      t.string :body_text_color, :default => "#3779ad", :null => false
      t.string :body_background_color, :default => "white", :null => false
      t.string :body_background_image
      t.string :footer_text_color, :default => "white", :null => false
      t.string :footer_background_color, :default => "#b9bbc0", :null => false
      t.string :footer_background_image

      t.timestamps
    end
  end
end
