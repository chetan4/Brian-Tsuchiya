class AddEmailColumnToUserRewards < ActiveRecord::Migration
  def change
    add_column :user_rewards, :email, :string
  end
end
