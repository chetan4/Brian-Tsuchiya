class AddContributionGoalColumnToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :contribution_goal, :decimal, :default => 0.0
  end
end
