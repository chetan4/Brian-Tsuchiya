class DropProjectsTable < ActiveRecord::Migration
  def up
    drop_table :projects
    drop_table :project_layouts
  end

  def down
  end
end
