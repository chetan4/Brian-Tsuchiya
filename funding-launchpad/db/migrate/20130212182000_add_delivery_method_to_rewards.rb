class AddDeliveryMethodToRewards < ActiveRecord::Migration
  def change
    add_column :rewards, :delivery_method, :string
  end
end
