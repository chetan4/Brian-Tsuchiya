class AddExpirationDateToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :expiration_date, :date

  end
end
