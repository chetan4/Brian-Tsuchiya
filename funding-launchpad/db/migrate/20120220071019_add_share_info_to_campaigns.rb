class AddShareInfoToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :share_price, :decimal, :precision => 10, :scale => 2
    add_column :campaigns, :min_shares, :integer
    add_column :campaigns, :max_shares, :integer
    add_column :campaigns, :shares_issued, :integer
  end
end
