class CreateInvestments < ActiveRecord::Migration
  def change
    create_table :investments do |t|
      t.integer :project_id, :null => false
      t.integer :user_id, :null => false
      t.decimal :amount, :precision => 10, :scale => 2

      t.timestamps
    end
  end
end
