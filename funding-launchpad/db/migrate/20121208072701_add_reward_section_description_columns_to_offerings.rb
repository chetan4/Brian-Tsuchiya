class AddRewardSectionDescriptionColumnsToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :rewards_gift_heading, :string
    add_column :offerings, :rewards_gift_subheading, :string
    add_column :offerings, :rewards_coupon_heading, :string
    add_column :offerings, :rewards_coupon_subheading, :string
    add_column :offerings, :rewards_auction_heading, :string
    add_column :offerings, :rewards_auction_subheading, :string
  end
end
