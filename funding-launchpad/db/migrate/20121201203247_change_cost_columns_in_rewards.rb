class ChangeCostColumnsInRewards < ActiveRecord::Migration
  def up
    change_column :rewards, :base_cost, :numeric, default: 0.00
    change_column :rewards, :ancillary_cost, :numeric, default: 0.00
  end
  
  def down
    change_column :rewards, :base_cost, :integer
    change_column :rewards, :ancillary_cost, :integer
  end
end
