class AddShortBioToProjects < ActiveRecord::Migration
  def change
    rename_column :projects, :bio, :full_bio
    add_column :projects, :short_bio, :text

  end
end
