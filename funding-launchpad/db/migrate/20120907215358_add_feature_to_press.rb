class AddFeatureToPress < ActiveRecord::Migration
  def change
    add_column :press, :feature, :boolean
  end
end
