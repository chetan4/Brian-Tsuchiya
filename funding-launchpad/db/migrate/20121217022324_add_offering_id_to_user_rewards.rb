class AddOfferingIdToUserRewards < ActiveRecord::Migration
  def change
    add_column :user_rewards, :offering_id, :integer
  end
end
