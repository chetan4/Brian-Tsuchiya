class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.text :description
      t.datetime :start_date
      t.datetime :end_date
      t.string :title
      t.references :campaign

      t.timestamps
    end
    add_index :events, :campaign_id
  end
end
