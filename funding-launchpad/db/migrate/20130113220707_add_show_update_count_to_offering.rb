class AddShowUpdateCountToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :show_update_count, :boolean, :default => true
  end
end
