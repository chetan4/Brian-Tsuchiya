class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.string :email
      t.string :name
      t.date :sent_on
      t.date :accepted_on

      t.timestamps
    end
  end
end
