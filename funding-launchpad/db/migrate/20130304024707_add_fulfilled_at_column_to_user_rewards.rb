class AddFulfilledAtColumnToUserRewards < ActiveRecord::Migration
  def change
    add_column :user_rewards, :fulfilled_at, :datetime
  end
end
