class CreateAuctionPreferences < ActiveRecord::Migration
  def change
    create_table :auction_preferences do |t|
      t.integer :user_id
      t.integer :auction_id
      t.boolean :auto_bid
      t.decimal :max_bid

      t.timestamps
    end
  end
end
