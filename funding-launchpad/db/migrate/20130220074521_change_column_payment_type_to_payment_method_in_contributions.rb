class ChangeColumnPaymentTypeToPaymentMethodInContributions < ActiveRecord::Migration
  def change
    rename_column :contributions, :payment_type, :payment_method
  end
end
