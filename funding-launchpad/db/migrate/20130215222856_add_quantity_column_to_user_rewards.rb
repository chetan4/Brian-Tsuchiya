class AddQuantityColumnToUserRewards < ActiveRecord::Migration
  def change
    add_column :user_rewards, :quantity, :integer, :default => 1
  end
end
