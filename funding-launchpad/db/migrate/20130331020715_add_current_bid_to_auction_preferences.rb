class AddCurrentBidToAuctionPreferences < ActiveRecord::Migration
  def change
    add_column :auction_preferences, :current_bid, :decimal
  end
end
