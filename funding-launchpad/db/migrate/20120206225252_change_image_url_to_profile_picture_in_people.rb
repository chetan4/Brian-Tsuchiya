class ChangeImageUrlToProfilePictureInPeople < ActiveRecord::Migration
  def up
    rename_column :people, :image_url, :profile_picture
  end

  def down
    rename_column :people, :profile_picture, :image_url
  end
end
