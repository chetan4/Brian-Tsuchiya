class CreateBidIncrements < ActiveRecord::Migration
  def change
    create_table :bid_increments do |t|
      t.decimal :min_price
      t.decimal :max_price
      t.decimal :increment

      t.timestamps
    end
  end
end
