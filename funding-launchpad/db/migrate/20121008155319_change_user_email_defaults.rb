class ChangeUserEmailDefaults < ActiveRecord::Migration
  def up
    change_column_default(:users, :pcp_email, false)
    change_column_default(:users, :newsletter, false)
  end

  def down
    change_column_default(:users, :pcp_email, true)
    change_column_default(:users, :newsletter, true)
  end
end
