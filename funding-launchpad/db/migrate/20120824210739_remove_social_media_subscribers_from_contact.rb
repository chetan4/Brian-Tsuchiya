class RemoveSocialMediaSubscribersFromContact < ActiveRecord::Migration
  def up
    remove_column :contacts, :social_media_subscribers
  end

  def down
    add_column :contacts, :social_media_subscribers, :integer
  end
end
