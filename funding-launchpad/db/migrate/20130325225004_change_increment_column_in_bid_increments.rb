class ChangeIncrementColumnInBidIncrements < ActiveRecord::Migration
  def up
    rename_column :bid_increments, :increment, :bid_increment
  end

  def down
    rename_column :bid_increments, :bid_increment, :increment
  end
end
