class AddContributionsEnabledColumnToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :contributions_enabled, :boolean, :default => 'false'
  end
end
