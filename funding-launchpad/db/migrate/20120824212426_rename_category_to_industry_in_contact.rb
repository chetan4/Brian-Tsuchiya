class RenameCategoryToIndustryInContact < ActiveRecord::Migration
  def change
    rename_column :contacts, :category, :industry
  end
end
