class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.string :content_type
      t.string :content
      t.references :phonable, :polymorphic => true
      t.timestamps
    end
  end
end
