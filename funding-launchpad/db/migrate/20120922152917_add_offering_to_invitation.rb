class AddOfferingToInvitation < ActiveRecord::Migration
  def change
    add_column :users, :invitation_offering, :integer
  end
end
