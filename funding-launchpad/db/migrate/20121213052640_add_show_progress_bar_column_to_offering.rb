class AddShowProgressBarColumnToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :show_progress_bar, :boolean, :default => true
  end
end
