class AddShowSocialMediaColumnToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :show_social_media, :boolean, :default => true
  end
end
