class ModifyColumnsInInvestments < ActiveRecord::Migration
  def up
    rename_column :investments, :amount, :offered_amount
    add_column :investments, :accepted_amount, :decimal, :precision => 10, :scale => 2
    add_column :investments, :status, :string
  end

  def down
    remove_column :investments, :status
    remove_column :investments, :accepted_amount
    rename_column :investments, :offered_amount, :amount
  end
end
