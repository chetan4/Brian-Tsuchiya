class RemoveCompanyNameFromProjects < ActiveRecord::Migration
  def up
    remove_column :projects, :company_name
  end

  def down
    add_column :projects, :company_name, :string
  end
end
