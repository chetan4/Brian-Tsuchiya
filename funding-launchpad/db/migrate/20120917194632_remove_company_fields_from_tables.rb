class RemoveCompanyFieldsFromTables < ActiveRecord::Migration
  def up
     Company.reset_column_information
     execute "UPDATE companies SET offering_id = offerings.id, description = offerings.long_description FROM offerings WHERE companies.id = offerings.company_id"
     remove_column :people, :company_id
     remove_column :offerings, :company_id
  end

  def down
    add_column :people, :company_id, :integer
    add_column :offerings, :company_id, :integer
  end
end

