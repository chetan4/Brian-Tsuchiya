class ChangeAdminToAccessInUsers < ActiveRecord::Migration
  def up
    remove_column :users, :admin
    add_column :users, :access, :integer, :default => 0
  end

  def down
    remove_column :users, :access
    add_column :users, :admin, :boolean, :default => false
  end
end
