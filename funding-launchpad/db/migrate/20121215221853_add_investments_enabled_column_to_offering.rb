class AddInvestmentsEnabledColumnToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :investments_enabled, :boolean, :default => 'true'
  end
end
