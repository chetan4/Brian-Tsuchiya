class AddRewardsColumnsToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :rewards_enabled, :boolean, :default => false
    add_column :offerings, :rewards_display_orientation, :string, :default => 'V'
    add_column :offerings, :rewards_horizontal_alignment, :string, :default => 'C'
    add_column :offerings, :rewards_max_displayed, :integer, :default => 99
    add_column :offerings, :rewards_default_auction_duration, :integer, :default => 30
  end
end
