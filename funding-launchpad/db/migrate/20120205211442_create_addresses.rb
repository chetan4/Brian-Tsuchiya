class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :content_type
      t.string :content
      t.references :addressable, :polymorphic => true
      t.timestamps
    end
  end
end
