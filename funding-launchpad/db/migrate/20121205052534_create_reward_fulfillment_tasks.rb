class CreateRewardFulfillmentTasks < ActiveRecord::Migration
  def change
    create_table :reward_fulfillment_tasks do |t|
      t.integer :fulfillment_id
      t.string :status

      t.timestamps
    end
  end
end
