class ChangeProjectIdToCampaignIdInInvestments < ActiveRecord::Migration
  def up
    rename_column :investments, :project_id, :campaign_id
  end

  def down
    rename_column :investments, :campaign_id, :project_id
  end
end
