class AddIndexToCompanies < ActiveRecord::Migration
  def change
    add_index :companies, :offering_id
  end
end
