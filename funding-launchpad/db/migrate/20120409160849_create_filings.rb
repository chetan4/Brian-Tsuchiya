class CreateFilings < ActiveRecord::Migration
  def change
    create_table :filings do |t|
      t.string :state
      t.references :campaign

      t.timestamps
    end
    add_index :filings, :campaign_id
  end
end
