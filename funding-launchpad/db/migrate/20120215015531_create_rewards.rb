class CreateRewards < ActiveRecord::Migration
  def change
    create_table :rewards do |t|
      t.integer :campaign_id
      t.decimal :investment_level
      t.integer :quantity
      t.integer :awarded
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
