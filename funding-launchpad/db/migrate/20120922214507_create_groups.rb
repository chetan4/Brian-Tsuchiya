class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.string :subdomain
      t.text :description

      t.timestamps
    end

    add_index :groups, :subdomain, :unique => true
    add_column :offerings, :group_id, :integer
    add_index :offerings, :group_id
  end
end
