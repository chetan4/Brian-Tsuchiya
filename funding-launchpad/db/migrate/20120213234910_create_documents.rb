class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.integer :campaign_id
      t.text :description
      t.boolean :required
      t.string :name

      t.timestamps
    end
  end
end
