class AddRewardTypeColumnToUserRewards < ActiveRecord::Migration
  def change
    add_column :user_rewards, :reward_type, :string
  end
end
