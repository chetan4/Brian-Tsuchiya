class AddTaxIdToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :tax_id, :string
  end
end
