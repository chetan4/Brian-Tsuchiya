class RemoveCalculatedFieldsFromCampaign < ActiveRecord::Migration
  def up
    remove_column :campaigns, :minimum_goal
    remove_column :campaigns, :maximum_goal
    remove_column :campaigns, :min_equity
    remove_column :campaigns, :max_equity
  end

  def down
    add_column :campaigns, :minimum_goal, :decimal, :precision => 10, :scale => 2
    add_column :campaigns, :maximum_goal, :decimal, :precision => 10, :scale => 2
    add_column :campaigns, :min_equity, :decimal, :precision => 5, :scale => 2
    add_column :campaigns, :max_equity, :decimal, :precision => 5, :scale => 2
  end
end
