class AddPpmToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :ppm, :boolean, :default => false
  end
end
