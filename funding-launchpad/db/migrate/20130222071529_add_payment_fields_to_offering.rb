class AddPaymentFieldsToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :access_token, :string
    add_column :offerings, :publishable_key, :string
  end
end
