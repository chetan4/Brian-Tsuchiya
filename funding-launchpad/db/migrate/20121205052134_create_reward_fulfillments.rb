class CreateRewardFulfillments < ActiveRecord::Migration
  def change
    create_table :reward_fulfillments do |t|
      t.integer :user_id
      t.integer :reward_id
      t.integer :address_id
      t.string :status
      t.string :user_note

      t.timestamps
    end
  end
end
