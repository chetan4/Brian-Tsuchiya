class CreateCampaignsFollowingsJoinTable < ActiveRecord::Migration
  def change
    create_table :campaigns_followings, :id => false do |t|
      t.integer :campaign_id
      t.integer :following_id
    end
  end
end
