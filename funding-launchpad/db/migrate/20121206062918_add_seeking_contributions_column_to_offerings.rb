class AddSeekingContributionsColumnToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :seeking_contributions, :boolean, default: false
  end
end
