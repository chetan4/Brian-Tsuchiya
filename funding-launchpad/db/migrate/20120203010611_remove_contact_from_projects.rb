class RemoveContactFromProjects < ActiveRecord::Migration
  def up
    remove_column :projects, :contact
  end

  def down
    add_column :projects, :contact, :string
  end
end
