class AddPcpEmailToUsers < ActiveRecord::Migration
  def change
    add_column :users, :pcp_email, :boolean, :default => true
  end
end
