class AddColumnsToContributions < ActiveRecord::Migration
  def change
    add_column :contributions, :account_number, :string
    add_column :contributions, :account_name, :string
    add_column :contributions, :expiration_date, :string
    add_column :contributions, :security_code, :string
  end
end
