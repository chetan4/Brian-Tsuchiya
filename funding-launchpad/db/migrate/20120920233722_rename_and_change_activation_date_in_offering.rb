class RenameAndChangeActivationDateInOffering < ActiveRecord::Migration
  def change
    rename_column :offerings, :activation_date, :activated_at
    change_column :offerings, :activated_at, :datetime
  end
end
