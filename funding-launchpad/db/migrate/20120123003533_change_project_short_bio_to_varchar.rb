class ChangeProjectShortBioToVarchar < ActiveRecord::Migration
  def up
    change_column :projects, :short_bio, :string
  end

  def down
    change_column :projects, :short_bio, :text
  end
end
