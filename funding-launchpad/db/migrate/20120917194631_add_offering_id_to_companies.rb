class AddOfferingIdToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :offering_id, :integer
  end
end
