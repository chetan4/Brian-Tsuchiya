class AddShortDescriptionColumnToRewards < ActiveRecord::Migration
  def change
    add_column :rewards, :short_description, :string
  end
end
