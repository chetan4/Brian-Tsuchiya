class CreateRewardPayments < ActiveRecord::Migration
  def change
    create_table :reward_payments do |t|
      t.integer :user_id
      t.integer :reward_id
      t.string :payment_method
      t.decimal :amount

      t.timestamps
    end
  end
end
