class ChangeDefaultFollowingOptionDefaults < ActiveRecord::Migration
  def change
    change_column_default :default_following_options, :new_offering_update,      true
    change_column_default :default_following_options, :offering_funded,          true
    change_column_default :default_following_options, :offering_expired,         true
  end
end
