class RemoveDurationFromCampaigns < ActiveRecord::Migration
  def up
    remove_column :campaigns, :duration
  end

  def down
    add_column :campaigns, :duration, :integer
  end
end
