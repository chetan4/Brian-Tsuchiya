class CreateUpdates < ActiveRecord::Migration
  def change
    create_table :updates do |t|
      t.integer :campaign_id
      t.integer :person_id
      t.string :subject
      t.text :content

      t.timestamps
    end
  end
end
