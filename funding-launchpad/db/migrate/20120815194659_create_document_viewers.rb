class CreateDocumentViewers < ActiveRecord::Migration
  def change
    create_table :document_viewers, :id => false do |t|
      t.references :document, :user
      t.timestamps
    end
    add_index :document_viewers, [:document_id, :user_id]
  end
end
