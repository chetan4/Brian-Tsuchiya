class UserMailer < ActionMailer::Base
  default from: "support@fundinglaunchpad.com"

  def welcome_email(user)
    @user = user
    mail(:to => user.email, :subject => "Congratulations! You have successfully registered at Funding Launchpad.")
    headers['X-MC-Track'] = "opens, clicks"
    headers['X-MC-GoogleAnalytics'] = "fundinglaunchpad.com"
    headers['X-MC-Tags'] = "welcome"
  end

  def invite_user(invitation)
    @invitation = invitation
    mail(:to => invitation.email, :subject => "You've been invited!")
  end

  def reinvite_user(invitation)
    @invitation = invitation
    mail(:to => invitation.email, :subject => "What are you waiting for?")
  end

  def private_access(user, offering)
    @user = user
    @offering = offering
    mail(:to => user.email, :subject => "You have been invited to view a private offering!")
  end

  def newsletter_email(user)
    @user = user
    mail(:to => user.email, :subject => "Congratulations! You have successfully signed up for The Funding Launchpad newsletter.")
    headers['X-MC-Track'] = "opens, clicks"
    headers['X-MC-GoogleAnalytics'] = "fundinglaunchpad.com"
    headers['X-MC-Tags'] = "welcome"
  end
end
