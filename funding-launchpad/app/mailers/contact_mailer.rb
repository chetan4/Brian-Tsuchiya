class ContactMailer < ActionMailer::Base

  default :from => "noreply@fundinglaunchpad.com"

  def notification(contact)
    @contact = contact
    mail(:to => "info@fundinglaunchpad.com", :subject => "[FL Issuer Request] #{contact.name}", :reply_to => "#{contact.email}")
  end

  def confirmation(contact)
    @contact = contact
    mail(:to => contact.email, :subject => "Thank you for your interest in Funding Launchpad!", :reply_to => "noreply@fundinglaunchpad.com")
  end
end
