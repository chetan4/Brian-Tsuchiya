class InvestmentMailer < ActionMailer::Base

  default from: "noreply@fundinglaunchpad.com"

  def you_invested(investment)
    @investment = investment
    mail(:to => investment.user.email, :subject => "You've Expressed an Interest in Investing!")
  end

  def new_investment(investment)
    @investment = investment
    mail(:to => investment.offering.contact.email, :bcc => 'investment@fundinglaunchpad.com', :subject => "Someone Has Expressed an Interest in Investing!")
  end

  def offer_acknowledged(investment)
    @investment = investment
    mail(:to => investment.user.email, :subject => "Your investment offer has been acknowledged.")
  end

  def offer_accepted(investment)
    @investment = investment
    mail(:to => investment.user.email, :subject => "Congratulations!  Your investment offer has been accepted.")
  end

  def funds_sent(investment)
    @investment = investment
    mail(:to => investment.offering.contact.email, :subject => "Funds have been sent.")
  end

  def funds_escrowed(investment)
    @investment = investment
    mail(:to => investment.user.email, :subject => "Funds have been escrowed.")
  end

end
