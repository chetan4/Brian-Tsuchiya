class MessageMailer < ActionMailer::Base

  default :from => "noreply@fundinglaunchpad.com"
  default :to => "info@fundinglaunchpad.com"

  def new_message(message)
    @message = message
    mail(:subject => "[FL Contact Page] #{message.subject}", :reply_to => "#{message.email}")
  end

end
