class AdminMailer < ActionMailer::Base
  default from: "noreply@fundinglaunchpad.com"

  def offering_complete(offering)
    @offering = offering
    @url = reviews_url
    @to = []

    # add the vim admins
    @to.concat User.with_role(:vim_admin).map{|user| user.email}

    mail(:to => @to.uniq, :subject => "#{offering.name} is awaiting admin approval!")
  end
end