class AnswerMailer < ActionMailer::Base
  default from: "noreply@fundinglaunchpad.com"

  def answer_update(answer)
    @answer = answer
    @url = offering_questions_url(@answer.question.offering)
    @to = []
    # add the person who is the contact for the offering
    @to << @answer.question.offering.contact.email
    # add the question participants
    @to.concat @answer.question.participants.map{|user| user.email}

    #remove person who answered
    @to.delete @answer.user.email
    mail(:bcc => @to.uniq, :subject => "#{@answer.question.offering.name}: A new answer has been posted")
  end
end
