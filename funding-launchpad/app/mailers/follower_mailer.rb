class FollowerMailer < ActionMailer::Base
  default from: "support@fundinglaunchpad.com"

  def offering_update(offering, user, update)
    @offering = offering
    @user = user
    @update = update
    if PCP_CONFIG.is_pcp?
      subject = "#{offering.name}: A new offering update has been posted"
    else
      subject = "Fundinglaunchpad: A new offering update has been posted"
    end
    mail(:to => user.email, :subject => subject)
  end

  def offering_qa(qa)
    case qa
    when Answer
      @answer = qa
      @question = qa.question
    when Question
      @question = qa
    end

    @offering = @question.offering
    @url = offering_question_url(@offering, @question)
    @followers  = Following.find_all_by_offering_id(@offering.id)
    @to = []

    @followers.each do |follower|
      if follower.user_accepts_new_offering_qa?
        @to << follower.user.email
      end
    end

    #remove the question/answer participants and offering contact
    @to = @to - @question.participants.map{|user| user.email} - [@offering.contact.email]

    if @to.empty?
      self.message.perform_deliveries = false
    else
      subject = "%s: A new %s has been posted" % [@offering.name, @answer ? "answer" : "question"]
      mail(:bcc => @to.uniq, :subject => subject)
    end
  end
end
