class QuestionMailer < ActionMailer::Base
  default from: "support@fundinglaunchpad.com"

  def question_update(question)
    attachments.inline['fl-logo-small.png'] = File.read("#{Rails.root}/app/assets/images/fl-logo-small.png")
    @question = question
    @url = offering_questions_url(@question.offering)
    mail(:to => question.offering.contact.email, :subject => "Fundinglaunchpad: A new question has been asked regarding your offering")
  end
end
