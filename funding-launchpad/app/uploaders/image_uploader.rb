# encoding: utf-8
class ImageUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  include CarrierWave::RMagick

  # Choose what kind of storage to use for this uploader:
  # storage :file
  if Rails.env.test?
    storage :file
  else
    storage :fog
  end

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/offerings/#{model.offering_id}/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Process files as they are uploaded:
  #process :resize_to_limit => [800, 800]

  # Create different versions of your uploaded files:
  version :medium, :from_version => :thumb do
    process resize_to_fill: [20, 20]
  end

  version :small do
    process :resize_to_limit => [150, 150]
  end

  version :thumb, :from_version => :small do
    process :resize_to_fill => [48, 48]
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
   def extension_white_list
     %w(jpg jpeg gif png)
   end
end
