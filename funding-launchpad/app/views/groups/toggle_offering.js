$("#offering_<%= @offering.id%>").fadeOut(function(){
  $(this).replaceWith("<%= escape_javascript(render :partial => 'offering_table_row', :locals => {:offering => @offering}) %>");
  $("#offering_<%= @offering.id%>").hide().fadeIn("slow");
});

Fl.utils.replaceFlash("<%= escape_javascript(render :partial => 'shared/flash' , :locals => { :flash => flash }).html_safe %>");
