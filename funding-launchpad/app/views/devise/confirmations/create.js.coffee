<% if resource.errors.blank? %>
  <% if @current_user_resend %>
    $('#required_modal.confirm-email .modal-body').html "<%= escape_javascript render :partial => 'shared/required_modal/confirm_email_body' %>"
    <% flash.discard(:notice) %>
  <% else %>
    <% @ajaxed = true %>
    $('.modal#login .modal-body').html "<%= escape_javascript render :partial => 'users/registrations/login_and_register' %>"
    $("#login .modal-header h3").html("Login or Signup")
    Fl.pages.users.init()
    Fl.pages.users.addLoginEmail("<%= resource.email %>")
    Fl.pages.users.showLoginTab()
  <% end %>
<% else %>
  $(".modal-body").html("<%= escape_javascript render 'new'%>")

<% end %>