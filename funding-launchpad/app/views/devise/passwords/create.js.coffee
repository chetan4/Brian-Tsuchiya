<% if resource.errors.blank? %>
  <% @ajaxed = true %>
  $('.modal#login .modal-body').html "<%= escape_javascript render :partial => 'users/registrations/login_and_register' %>"
  $("#login .modal-header h3").html("Login or Signup")
  Fl.pages.users.init()
  Fl.pages.users.addLoginEmail("<%= resource.email %>")
  Fl.pages.users.showLoginTab()
<% else %>
  $(".modal-body").html("<%= escape_javascript render 'new'%>")

<% end %>
