<% if resource.errors.blank? %>
  $('.modal#login').modal("hide")
  window.location.href = "<%= @redirect_url %>"
<% else %>
  $('.modal#login .modal-body').html("<%= escape_javascript render :partial => 'users/registrations/login_and_register', :locals => {:resource => resource} %>");
  $("#login .modal-header h3").html("Login or Signup")
  $('#login-tabs a[href="#signup-tab"]').tab("show")
  $('.modal#login').modal("show")
  Fl.pages.users.init()
  Fl.pages.users.removeLoginError()
  $('input, textarea').placeholder()
<% end %>