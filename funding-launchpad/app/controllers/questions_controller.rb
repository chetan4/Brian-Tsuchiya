class QuestionsController < ApplicationController
  skip_before_filter :authenticate_user!, :only => [:show, :index]
  load_and_authorize_resource :offering
  load_and_authorize_resource :question, :through => :offering

  def index
    @questions = Question.where("offering_id IN (?)", @offering.id).order("created_at DESC").page(params[:page]).per(5)
    display_can_invest(@offering)
  end

  def show
  end

  def new
    @show_not_confirmed_message = !current_user.confirmed?
  end

  def create
    @question.user_id = current_user.id
    respond_to do |format|
      if @question.save
        format.html { redirect_to offering_questions_path(@offering), notice: "Question added" }
        format.json { render json: @question, status: :created, location: @question }
      else
        format.html { render :new }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

end
