class DefaultFollowingOptionsController < ApplicationController
  load_and_authorize_resource

  def edit
    @dfo = DefaultFollowingOption.find(params[:id])
  end

  def update
    @dfo = DefaultFollowingOption.find(params[:id])
    respond_to do |format|
      if @dfo.update_attributes(params[:default_following_option])
        format.html { redirect_to root_path, notice: 'Default Following Options updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dfo.errors, status: :unprocessable_entity }
      end
    end
  end
end
