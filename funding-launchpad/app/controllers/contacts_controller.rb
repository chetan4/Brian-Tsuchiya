class ContactsController < ApplicationController
  skip_before_filter :authenticate_user!

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new params[:contact]
    if @contact.save
      redirect_to root_path, :notice =>"Thanks for contacting us! We'll be in touch shortly."
    else
      render :new
    end
  end
end
