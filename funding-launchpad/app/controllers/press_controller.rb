class PressController < ApplicationController
  skip_before_filter :authenticate_user!, :only => [:index]
  load_and_authorize_resource :except => [:index]
  
  # GET /press
  # GET /press.json
  def index
    @press = Press.published.page(params[:page]).per(12)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @press }
      format.js
    end
  end

  # GET /press/1
  # GET /press/1.json
  def show
    @press = Press.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @press }
    end
  end

  # GET /press/new
  # GET /press/new.json
  def new
    @press = Press.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @press }
    end
  end

  # GET /press/1/edit
  def edit
    @press = Press.find(params[:id])
  end

  # POST /press
  # POST /press.json
  def create
    @press = Press.new(params[:press])

    respond_to do |format|
      if @press.save
        format.html { redirect_to @press, notice: 'Press was successfully created.' }
        format.json { render json: @press, status: :created, location: @press }
      else
        format.html { render action: "new" }
        format.json { render json: @press.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /press/1
  # PUT /press/1.json
  def update
    @press = Press.find(params[:id])

    respond_to do |format|
      if @press.update_attributes(params[:press])
        format.html { redirect_to @press, notice: 'Press was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @press.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /press/1
  # DELETE /press/1.json
  def destroy
    @press = Press.find(params[:id])
    @press.destroy

    respond_to do |format|
      format.html { redirect_to press_index_url }
      format.json { head :no_content }
    end
  end

  def admin
    @press = Press.all

    respond_to do |format|
      format.html # admin.html.erb
    end
  end
end
