class VideosController < ApplicationController
  load_and_authorize_resource :offering
  load_and_authorize_resource :through => :offering

  def preview
    video = Video.new(:link => params[:video])
    render :json => video.link_html.to_json
  end

  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @videos }
    end
  end

  # GET /videos/new
  # GET /videos/new.json
  def new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @video }
    end
  end

  # GET /videos/1/edit
  def edit
    #@video = Video.find(params[:id])
  end

  # POST /videos
  # POST /videos.json
  def create
    respond_to do |format|
      if @video.save
        format.html { redirect_to offering_videos_path(@offering), notice: 'Video was successfully created.' }
        format.json { render json: @video, status: :created, location: @video }
      else
        format.html { render action: "new" }
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /videos/1
  # PUT /videos/1.json
  def update
    respond_to do |format|
      if @video.update_attributes(params[:video])
        format.html { redirect_to offering_videos_path(@offering), notice: 'Video was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /videos/1
  # DELETE /videos/1.json
  def destroy
    @video.destroy
    respond_to do |format|
      flash.now[:notice] = 'Video was successfully deleted'
      format.html { redirect_to offering_videos_path(@offering)}
      format.json { head :no_content }
      format.js
    end
  end
end
