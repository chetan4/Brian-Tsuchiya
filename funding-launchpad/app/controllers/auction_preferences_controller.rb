class AuctionPreferencesController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource

  def index
  end

  def show
  end

  def new
    @auction = Auction.find(params[:a])
    @offering = @auction.offering
  end

  def edit
  end

  def create
    @bid = Bid.new
    @bid.create_bid({user_id: current_user.id, auction_id: @auction.id, bid: @auction_preference.current_bid})
    respond_to do |format|
      if @auction_preference.save
        AuctionPreference.update_bids(@auction_preference)
        format.html { redirect_to offering_path(@offering), notice: 'Your bid has been accepted.' }
        format.json { render json: @auction_preference, status: :created, location: @auction_preference }
      else
        format.html { render action: "new" }
        format.json { render json: @auction_preference.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
  end
end
