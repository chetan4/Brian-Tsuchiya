class DocumentsController < ApplicationController
  load_and_authorize_resource :offering
  load_and_authorize_resource :through => :offering, :except => [:show]

  skip_before_filter :authenticate_user!, :only => [:show]

  # GET /documents
  # GET /documents.json
  def index
  end

  # GET /documents/1
  # GET /documents/1.json
  def show
    #redirect_to root_path unless current_user

    # record the docview for this user
    @document = Document.find(params[:id])
    DocumentViewer.create(document: @document, user: current_user) if @document && current_user

    # TODO: research whether this can be moved to Resque on Heroku, it is currently blocking
    # show the document
    @document.document.retrieve_from_store!(File.basename(@document.document.url))
    @document.document.cache_stored_file!
    send_file @document.document.file.path
  end

  # GET /documents/new
  # GET /documents/new.json
  def new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @document }
    end
  end

  # GET /documents/1/edit
  def edit
  end

  # POST /documents
  # POST /documents.json
  def create
    respond_to do |format|
      if @document.save
        format.html { redirect_to offering_documents_path(@offering), notice: 'Document was successfully created.' }
        format.json { render json: @document, status: :created, location: @document }
      else
        format.html { render action: "new" }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /documents/1
  # PUT /documents/1.json
  def update
    respond_to do |format|
      if @document.update_attributes(params[:document])
        format.html { redirect_to offering_documents_path(@offering), notice: 'Document was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    @document.destroy
    respond_to do |format|
      flash.now[:notice] = 'Document was successfully deleted'
      format.html { redirect_to offering_documents_path(@offering) }
      format.json { head :no_content }
      format.js
    end
  end
end
