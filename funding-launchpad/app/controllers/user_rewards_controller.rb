class UserRewardsController < ApplicationController
  load_and_authorize_resource

  def index
    @user_rewards = UserReward.where(:offering_id => Offering.find_by_slug(params[:offering_id]).id).order(:created_at)
    respond_to do |format|
      format.html
      format.csv { send_data @user_rewards.to_csv }
      format.xls # { send_data @user_rewards.to_csv(col_sep: "\t") }
    end
  end
  
  def new
    @user_reward.offering_id = Offering.find_by_slug(params[:offering_id]).id
    @user_reward.reward_id = params[:reward_id]
    @user_reward.price = @user_reward.reward.price
    @user_reward.build_address
    @user_reward.load_address_from_user(current_user)
    @user_reward.email = current_user.email
    case
    when @user_reward.reward.reward_type == 'Reward'
      @available_contributions = current_user.offering_contributions(@user_reward.offering)-current_user.contributions_applied(@user_reward.offering)
      @user_reward.pay_info_charge_amount = @user_reward.reward.price - @available_contributions
    when @user_reward.reward.reward_type == 'Coupon'
    when @user_reward.reward.reward_type == 'Auction'
    else
      # Trouble
    end
  end
  
  def edit
  end

  def create
    if @user_reward.pay_info_charge_amount
      @contribution = Contribution.new
      @contribution.user_id = current_user.id
      @contribution.offering_id = @user_reward.offering_id
      @contribution.amount = @user_reward.pay_info_charge_amount
      @contribution.status = "New"
      # Create Stripe payment here with (account_number, account_name, expiration_date, security_code)
      @contribution.save
    end
    @user_reward.user_id = current_user.id
    @user_reward.fulfillment_status = "New"
    @user_reward.reward_type = @user_reward.reward.reward_type
    respond_to do |format|
      if @user_reward.save
        format.html {redirect_to offering_path(@user_reward.offering), notice: "You now own the #{@user_reward.reward.title}!"}
      else
        render "new"
      end
    end
  end
  
  def update
    respond_to do |format|
      if @user_reward.update_attributes(params[:user_reward])
        format.html { redirect_to mycontributions_path, notice: 'Address successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user_reward.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @user_reward = UserReward.find(params[:id])
    @user_reward.destroy

    respond_to do |format|
      format.html { redirect_to mycontributions_path }
      format.json { head :no_content }
    end
  end

  def fulfill
    @user_reward.update_attributes({fulfillment_status: "Fulfilled", fulfilled_at: Time.now})
    @offering = Offering.find(@user_reward.offering_id)
    redirect_to offering_user_rewards_path(@offering)
  end
end
