class EventsController < ApplicationController
  before_filter :authenticate_user!, except: [:show]
  load_and_authorize_resource :offering, except: [:index]
  load_and_authorize_resource :through => :offering, except: [:index]

  def index
    @offering = Offering.find(params[:offering_id])
    @events = @offering.upcoming_events
  end

  def new
  end

  def edit
  end

  def show
  end

  def admin
    @events = @events.order('start_date Asc')
  end

  def create
    respond_to do |format|
      if @event.save
        format.html { redirect_to offering_events_admin_path(@offering), notice: 'Event was successfully created.' }
        format.json { render json: @event, status: :created }
      else
        format.html { render action: "new" }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @event.update_attributes(params[:event])
        format.html { redirect_to offering_events_admin_path(@offering), notice: 'Event was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @event.destroy
    respond_to do |format|
      flash.now[:notice] = 'Event was successfully deleted'
      format.html { redirect_to offering_events_admin_url(@offering) }
      format.js
      format.json { head :no_content }
    end
  end
end
