class GroupsController < ApplicationController
  load_and_authorize_resource :group
  # GET /groups
  # GET /groups.json
  def index
    @groups = Group.with_role :group_member, current_user

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @groups }
    end
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
    @offerings = (current_user.has_role? :vim_admin) ? Offering.all : Offering.with_role(:offering_admin, current_user)
    @group_offerings = Offering.find_by_group_id(@group)
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @group }
    end
  end

  # GET /groups/new
  # GET /groups/new.json
  def new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @group }
    end
  end

  # GET /groups/1/edit
  def edit
    @group = Group.find(params[:id])
  end

  # POST /groups
  # POST /groups.json
  def create

    respond_to do |format|
      if @group.update_attributes(params[:group])
        current_user.assign_group_roles(@group)
        format.html { redirect_to @group, notice: 'Group was successfully created.' }
        format.json { render json: @group, status: :created, location: @group }
      else
        format.html { render action: "new" }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /groups/1
  # PUT /groups/1.json
  def update

    respond_to do |format|
      if @group.update_attributes(params[:group])
        format.html { redirect_to @group, notice: 'Group was successfully updated.' }
        format.json { head :no_content }
        format.js
      else
        format.html { render action: "edit" }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def members
    @members = User.with_role :group_member, @group

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @members }
    end
  end

  def toggle_offering
    @offering = Offering.find(params[:offering_id])

    new_group = @offering.group.try(:id) == @group.id ? nil : @group
    @offering.group = new_group

    respond_to do |format|
      if @offering.save
        format.js
      else
        format.js
      end
    end
  end

end
