class MycontributionsController < ApplicationController

  def index
    @contributions = Contribution.find_all_by_user_id(current_user.id)
    @user_rewards = UserReward.find_all_by_user_id(current_user.id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: [@contributions, @user_rewards] }
    end
  end

end
