class AuctionsController < ApplicationController
  before_filter :authenticate_user!, except: [:show]
  load_and_authorize_resource :offering, except: [:index]
  load_and_authorize_resource :through => :offering, except: [:index]

  def index
    @offering = Offering.find(params[:offering_id])
    @auctions = @offering.auctions
  end
  
  def admin
    @offering = Offering.find(params[:offering_id])
    @auctions = @offering.auctions.order(:price)
  end

  def show
    @auction = Auction.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @auction }
    end
  end

  def new
    @auction = Auction.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @auction }
    end
  end

  def edit
    @auction = Auction.find(params[:id])
  end

  def create
    respond_to do |format|
      if @auction.save
        format.html { redirect_to offering_auctions_admin_path(@offering), notice: 'Auction was successfully created.' }
        format.json { render json: @reward, status: :created, location: @auction }
      else
        format.html { render action: "new" }
        format.json { render json: @auction.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @auction = Auction.find(params[:id])

    respond_to do |format|
      if @auction.update_attributes(params[:auction])
        format.html { redirect_to offering_auctions_admin_path(@offering), notice: 'Auction was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @auction.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @auction = Auction.find(params[:id])
    @auction.destroy

    respond_to do |format|
      format.html { redirect_to auctions_url }
      format.json { head :no_content }
    end
  end
end
