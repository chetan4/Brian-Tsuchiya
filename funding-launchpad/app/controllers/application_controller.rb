class ApplicationController < ActionController::Base
  protect_from_forgery
  include ApplicationHelper
  include SessionsHelper

  before_filter :authenticate_user!, :except => [:help, :about, :privacy, :terms, :newsletter, :new_message, :create_message, :optin, :unsubscribe]

  rescue_from CanCan::AccessDenied do |exception|
    logger.debug "###### Access denied on #{exception.action} #{exception.subject.inspect}"
    if is_pcp? and !Offering.find(PCPConstraint.id).active?
        sign_out(current_user) if current_user
        return redirect_to new_user_session_path, :alert => 'This offering is not currently active. Please try again later.'
    end

    flash[:error] = exception.message
    #this is needed so the flash messages are not lost from a double redirect.
    if is_pcp?
      redirect_to offering_path(Offering.find(PCPConstraint.id))
    else
      redirect_to root_path
    end
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    flash[:error] = "Record could not be found."
    redirect_to root_url
  end
end
