class FilingsController < ApplicationController
  def index
    @filtered = Carmen::Country.coded('US').subregions.select {|r| r.name.downcase.include? "#{params[:q]}".downcase}
    respond_to do |format|
      format.html
      format.json { render :json => @filtered.map {|r| {:id => r.code, :name => r.name}}}
    end
  end

  def states
    @offering = Offering.find(params[:c])

  end

  def destroy
    @filing = Filing.find_by_offering_id_and_state(params[:cid], params[:state])
    @filing.destroy
    respond_to do |format|
      format.html { redirect_to(users_url) }
      format.js { render :text => "alert('user has been deleted')" }
    end
  end
end
