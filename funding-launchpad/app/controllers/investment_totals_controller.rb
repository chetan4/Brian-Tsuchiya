class InvestmentTotalsController < ApplicationController
  load_and_authorize_resource

  # GET /investment_totals/1/edit
  def edit
  end

  # PUT /investment_totals/1
  # PUT /investment_totals/1.json
  def update

    respond_to do |format|
      if @investment_total.update_attributes(params[:investment_total])
        format.html { redirect_to admin_path, notice: 'Investment total was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @investment_total.errors, status: :unprocessable_entity }
      end
    end
  end

end
