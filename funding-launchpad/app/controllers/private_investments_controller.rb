class PrivateInvestmentsController < ApplicationController
  before_filter  :authenticate_user!
  load_and_authorize_resource :offering

  def new
    @investment = Investment.new
  end

  def create
    @investment = Investment.new(params[:investment])
    @investment.user_id = current_user.id
    @investment.status = Investment.completed
    @investment.offered_amount = params[:investment][:accepted_amount]
    @investment.offering = @offering
    @investment.comment = "added by offering admin"
    if @investment.save!
      flash.now[:notice] = "The investment has been added to your offering."
      render :new
    else
      flash.now[:error] = "There was a problem, please try again!"
      render :new
    end
  end
end
