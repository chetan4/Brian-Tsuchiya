class RewardsController < ApplicationController
  before_filter :authenticate_user!, except: [:show]
  load_and_authorize_resource :offering, except: [:index]
  load_and_authorize_resource :through => :offering, except: [:index]

  def index
    @offering = Offering.find(params[:offering_id])
    @rewards = @offering.rewards
  end
  
  def admin
    @offering = Offering.find(params[:offering_id])
    @rewards = @offering.rewards.order(:price)
  end

  def show
    @reward = Reward.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @reward }
    end
  end

  def new
    @reward = Reward.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @reward }
    end
  end

  def edit
    @reward = Reward.find(params[:id])
  end

  def create
    respond_to do |format|
      if @reward.save
        format.html { redirect_to offering_rewards_admin_path(@offering), notice: 'Reward was successfully created.' }
        format.json { render json: @reward, status: :created, location: @reward }
      else
        format.html { render action: "new" }
        format.json { render json: @reward.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @reward = Reward.find(params[:id])

    respond_to do |format|
      if @reward.update_attributes(params[:reward])
        format.html { redirect_to offering_rewards_admin_path(@offering), notice: 'Reward was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @reward.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rewards/1
  # DELETE /rewards/1.json
  def destroy
    @reward = Reward.find(params[:id])
    @reward.destroy

    respond_to do |format|
      format.html { redirect_to rewards_url }
      format.json { head :no_content }
    end
  end
end
