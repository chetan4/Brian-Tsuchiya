class UpdatesController < ApplicationController
  skip_before_filter :authenticate_user!, :only => [:show, :index]
  load_and_authorize_resource :offering
  load_and_authorize_resource :update, :through => :offering

  respond_to :html

  def index
    @updates = Update.where(:offering_id => @offering.id)
    display_can_invest(@offering)
  end

  def admin
  end

  def new
  end

  def create
    if params[:commit] == "Save"
      save_update
    else
      render :action => 'show'
    end
  end

  def show
  end

  def update
    if params[:commit] == "Save"
      post_update
    else
      @update = Update.new(params[:update])
      render :action => 'show'
    end
  end

  def revisions
  end

private
  def save_update
    @update.person_id = current_user.person.id
    if @update.save
      flash[:notice] = "#{@update.subject} was successfully created."
      redirect_to admin_offering_updates_path(@offering)
    else
      render :action => 'new'
    end
  end

  def post_update
    if @update.update_attributes(params[:update])
      flash[:notice] = "#{@update.subject} was successfully updated."
    end

    respond_with @update, location: admin_offering_updates_path(@offering)
  end

end
