class FollowingOptionsController < ApplicationController
  def edit
    @offering = Offering.find(params[:id])
    @fo = FollowingOption.where(:user_id => current_user.id).find_by_offering_id(@offering.id)
  end

  def update
    @fo = FollowingOption.find(params[:id])
    respond_to do |format|
      if @fo.update_attributes(params[:following_option])
        format.html { redirect_to followings_user_path(current_user), notice: 'Following Options updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @fo.errors, status: :unprocessable_entity }
      end
    end
  end
end
