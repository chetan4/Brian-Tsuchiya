class InvestmentsController < ApplicationController
  before_filter :user_can_invest, :only => [:new]
  load_and_authorize_resource :offering
  load_and_authorize_resource :investment, :through => :offering

  def user_can_invest
    @offering = Offering.find(params[:offering_id])
    if !current_user.can_invest?(@offering)
      redirect_to offering_path(@offering)
    end
  end

  # GET /investments
  # GET /investments.json
  def index
    #getting pg error when these aren't defined?
    @offering = Offering.find(params[:offering_id])
    @investments = @offering.investments
  end

  # GET /investments/new
  # GET /investments/new.json
  def new
    @investment = Investment.new
    @documents  = @offering.required_docs
    @unread_required_docs = @offering.required_docs.dup.delete_if do |document|
      DocumentViewer.find_by_document_id_and_user_id(document.id, current_user.id)
    end

    @show_not_confirmed_message = !current_user.confirmed?
    @show_accredited_message = !@show_not_confirmed_message && @offering.ppm? && !current_user.person.accredited?
    @show_required_message   = @show_accredited_message == false && @unread_required_docs.size > 0
  end

  # POST /investments
  # POST /investments.json
  def create
    #need this since we are skipping cancan load
    @investment = Investment.new(params[:investment])
    @investment.user_id = current_user.id

    @offering = Offering.find(params[:offering_id])
    @investment.offering = @offering

    respond_to do |format|
      if @investment.save
        format.html { redirect_to offering_investments_share_path(@offering)}
        format.json { render json: @investment, status: :created, location: @investment }
      else
        format.html { render action: "new" }
        format.json { render json: @investment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /investments/1
  # PUT /investments/1.json
  def update
    respond_to do |format|
      if @investment.update_attributes(params[:investment])
        format.html { redirect_to offering_investments_path(@investment.offering), notice: 'Investment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @investment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /investments/1
  # DELETE /investments/1.json
  def destroy
    @investment.destroy
    respond_to do |format|
      format.html { redirect_to myinvestments_path }
      format.json { head :no_content }
    end
  end

  def share
    @offering = Offering.find(params[:offering_id])
  end
end
