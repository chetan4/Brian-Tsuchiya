class ReviewsController < ApplicationController

  def index
    @offerings = Offering.all
    authorize! :view, :reviews
  end

  def show
    @offering = Offering.find(params[:id])
    authorize! :view, :reviews
  end
end
