class TeamMembersController < ApplicationController
  respond_to :js, :html
  load_and_authorize_resource :offering
  load_and_authorize_resource :through => :offering

  # GET /team_members
  def index
  end

  # GET /team_members/new
  def new
  end

  # GET /team_members/1/edit
  def edit
  end

  # POST /team_members
  def create
      if @team_member.save
        flash[:notice] = "#{@team_member.name} was successfully created."
      end
      respond_with(@team_member, location: offering_team_members_url(@offering))
  end

  # PUT /team_members/1
  def update
    if params[:id]
      if @team_member.update_attributes(params[:team_member])
        flash[:notice] = "#{@team_member.name} was successfully updated."
      end
      respond_with(@team_member, location: offering_team_members_url(@offering))
    else
      # sort the collection
      @team_members = @offering.team_members
      @team_members.each do |member|
        member.position = params['team_member'].index(member.id.to_s) + 1
        member.save
      end
      render :nothing => true
    end
  end

  # DELETE /team_members/1
  def destroy
    @team_member.destroy

    flash.now[:notice] = "#{@team_member.name} was successfully deleted"
    respond_with(@team_member, location: offering_team_members_url(@offering))
  end
end
