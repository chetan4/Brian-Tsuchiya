class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def all
    omniauth = request.env["omniauth.auth"]
    puts "\n\n\n==========\nomniauth.provider=/#{omniauth.provicer}/\nomniauth.uid=/#{omniauth.uid}/\n==========\n\n\n"

    authentication = Authentication.find_by_provider_and_uid(omniauth.provider, omniauth.uid)
    if authentication
      if current_user
        flash[:notice] = "#{omniauth.provider.titleize} is already added."
        redirect_to settings_path + "#social"
      else
        flash[:notice] = "Signed in successfully."
        sign_in_and_redirect(:user, authentication.user)
      end
    elsif current_user
      omniauth_attr = omniauth_attr(omniauth)
      current_user.authentications.create!(:provider => omniauth.provider, :uid => omniauth.uid)
      current_user.apply_omniauth(omniauth_attr, true)
      current_user.save
      respond_to do |format|
        flash[:notice] = "#{omniauth.provider.titleize} added successfully."
        format.html { redirect_to settings_path + "#social" }
      end
    else
      omniauth_attr = omniauth_attr(omniauth)
      user = find_for_oauth_by_email(omniauth_attr[:email])
      user.apply_omniauth(omniauth_attr)
      if user.valid?
        user.skip_confirmation!
        user.save!
        user.confirm!
        flash[:notice] = "Signed in successfully."
        sign_in_and_redirect(:user, user)
      else
        session["devise.user_attributes"] = user.attributes
        redirect_to new_user_registration_url
      end
    end
  end

  alias_method :linkedin, :all
  alias_method :twitter, :all
  alias_method :facebook, :all

  private

  def omniauth_attr(omniauth)
    case omniauth.provider
    when "linkedin"
      auth_attr = { provider: omniauth.provider,
                    uid: omniauth.uid,
                    email: omniauth.info.email,
                    first_name: omniauth.info.first_name,
                    last_name: omniauth.info.last_name,
                    image: omniauth.info.image,
                    title: omniauth.info.headline }
    when "facebook"
      auth_attr = { provider: omniauth.provider,
                    uid: omniauth.uid,
                    email: omniauth.info.email,
                    first_name: omniauth.info.first_name,
                    last_name: omniauth.info.last_name,
                    image: omniauth.info.image }
    when "twitter"
      first_name, last_name = omniauth.info.name.split
      auth_attr = { provider: omniauth.provider,
                    uid: omniauth.uid,
                    first_name: first_name,
                    last_name: last_name,
                    image: omniauth.info.image }
    else
      raise 'Provider #{omniauth.provider} not handled'
    end
  end

  def find_for_oauth_by_email(email)
    if user = User.find_by_email(email)
      user
    else
      user = User.new
      user.with_person
    end
  end
end