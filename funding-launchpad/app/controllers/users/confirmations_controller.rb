class Users::ConfirmationsController < Devise::ConfirmationsController

  def create
    @current_user_resend = params[:user][:current_user_resend] || false
    super
  end

  protected

  def after_resending_confirmation_instructions_path_for(resource_name)
    if signed_in?
      #stay on the same page
      request.referrer
    else
      new_session_path(resource_name)
    end
  end
end