class Users::RegistrationsController < Devise::RegistrationsController

  # GET /resource/sign_up
  def new
    resource = build_resource({})
    respond_with resource.with_person
  end

  def create
    build_resource
    if resource.person.nil?
      person = Person.new
      person.user = resource
      person.dont_validate_residence = true
      person.addresses.build({content_type: "Residence"})
      resource.person = person
    end
    resource.person.dont_validate_residence = true
    resource.confirmed_at = Time.now

    @is_beta_signup = params['user']['investor']

    # if an uncofirmed resource exists with this address and doesn't have a password, update them. This means
    # the user signed up for beta, and hasn't yet filled out the account info.
    unconfirmed = User.find_by_email_and_confirmed_at_and_encrypted_password(params['user']['email'], nil, "")

    #if its a beta signup, we want to try and save the resource so user see's that their email was already entered. 
    if false # unconfirmed and !@is_beta_signup
      # update with params
      if unconfirmed.update_attributes(params['user'])
        # send confirmation
        unconfirmed.send_confirmation_instructions

        if unconfirmed.active_for_authentication?
          sign_in(User, unconfirmed)
          respond_with_custom unconfirmed, after_sign_up_path_for(unconfirmed)
        else
          expire_session_data_after_sign_in!
          respond_with_custom unconfirmed, after_inactive_sign_up_path_for(unconfirmed)
        end
      else
        clean_up_passwords resource

        #if we respond with unconfirmed, the form is changed to edit, so instead
        #lets respond with resource and just copy validations to it. 
        unconfirmed.errors.each do |key, val|
          resource.errors.add(key, val)
        end

        #needed to carry over person errors...why?
        resource.person = unconfirmed.person
        respond_with_custom resource

      end

    elsif resource.save
      UserMailer.welcome_email(resource).deliver unless PCP_CONFIG.is_pcp?
      if true # resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_in(resource_name, resource)
        respond_with_custom resource, after_sign_up_path_for(resource)
      else
        expire_session_data_after_sign_in!
        respond_with_custom resource, after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      respond_with_custom resource
    end
  end

  def update
    @user = User.find(current_user.id)
    respond_to do |format|
      if @user.update_with_password(params[:user])
        flash.now[:notice] = "Changes successful"
        # Sign in the user by passing validation in case his password changed
        sign_in @user, :bypass => true
        format.html { redirect_to settings_path }
        format.js
      else
        error_msg = ""
        @user.errors.values.each do |vals|
          vals.each do |msg|
            error_msg += "#{msg} <br>"
          end
        end
         flash.now[:error] = error_msg.html_safe
        format.js
        format.html { render action: "edit_form" }
      end
    end
  end

   protected

  #needed to override this devise_inviatable method because it was only checking
  #for empty password, which meant beta signup users get destroyed
  def destroy_if_previously_invited
    hash = params[resource_name]
    if hash && hash[:email]
      resource = resource_class.where(:email => hash[:email], :encrypted_password => '')
                               .where(User.arel_table[:invited_by_id].not_eq(nil)).first
      if resource
        @invitation_info = Hash[resource.invitation_fields.map {|field|
          [field, resource.send(field)]
        }]
        resource.destroy
      end
    end
  end



  private

  #If we are doing beta signup, then respond with different action
  def respond_with_custom(resource, location = nil)
    if @is_beta_signup
      respond_with(resource) do |format|
          format.js { render :action => "create_beta", :location => location }
        end
    else
      #@redirect_url is used in registrations/create.js.coffee to redirect on success
      if !location.nil? and request.xhr?
        @redirect_url = location
      end
      respond_with resource, :location => location
    end
  end
end
