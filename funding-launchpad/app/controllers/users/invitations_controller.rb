class Users::InvitationsController < Devise::InvitationsController
  before_filter :authenticate_user!
  before_filter :authorize_offering, :only => [:new_offering_invite, :create_offering_invite, :destroy_offering_invite, :resend_offering_invite]

  # GET /resource/invitation/new
  def new
    super
  end

  # POST /resource/invitation
  def create
    if emails_validate?(params[:user][:email])
      params[:user][:email].split(",").each do |email|
        User.invite!(:email => email)
      end
      redirect_to my_offerings_path(params[:invitation_offering]), notice: 'Invite was successful'
    else
      flash.now[:notice] = "Please check that all email addresses are valid."
      render :new
    end
  end

  # GET /resource/invitation/accept?invitation_token=abcdef
  def edit
    if params[:invitation_token] && self.resource = resource_class.to_adapter.find_first( :invitation_token => params[:invitation_token] )
      resource.person ||= Person.new
      resource.person.addresses.build
      render :edit
    else
      set_flash_message(:alert, :invitation_token_invalid)
      redirect_to after_sign_out_path_for(resource_name)
    end
  end

  # PUT /resource/invitation
  def update
    self.resource = resource_class.accept_invitation!(params[resource_name])

    if resource.errors.empty?
      # was user invited to an offering
      if !resource.invitation_offering.nil?
        set_flash_message :notice, :signed_up
        sign_in(resource_name, resource)
        offering = Offering.find(resource.invitation_offering)
        current_user.add_role :private_viewer, offering
        redirect_to offering_path(offering)
      else
        # handles all users signing up for the beta
        if resource.active_for_authentication?
          UserMailer.welcome_email(resource).deliver
          set_flash_message :notice, :updated if is_navigational_format?
          sign_in(resource_name, resource)
          respond_with resource, :location => after_accept_path_for(resource)
        else
          set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
          expire_session_data_after_sign_in!
          respond_with resource, :location => after_inactive_sign_up_path_for(resource)
        end
      end
    else
      respond_with_navigational(resource){ render :edit }
    end
  end

  def new_offering_invite
    @invitations = User.with_role(:private_viewer, @offering)
    build_resource

    render :new_offering
  end

  # POST /resource/invitation
  def create_offering_invite
    @invitations = User.with_role(:private_viewer, @offering)

    resource = resource_class.find_by_email(params[resource_name][:email])

    #If user already exists in the system and they don't exist just because they have an invitation,
    #we just want to send them an email notifying them they now have access
    #to view the private offering
    if resource.present? and !resource.try(:invited_to_sign_up?)
      UserMailer.private_access(resource, @offering).deliver
    else
      resource = resource_class.invite!(params[resource_name], current_inviter)
    end

    if resource.errors.empty?
      #give user permission to view offering
      resource.add_role(:private_viewer, @offering)

      respond_to do |format|
        set_flash_message :notice, :send_instructions, :email => resource.email
        format.html {redirect_to new_offering_invitation_path(@offering)}
      end
    else
      respond_with_navigational(resource) { render :new_offering }
    end
  end

  def destroy_offering_invite
    @user = User.find(params[:user_id])
    @user.remove_role :private_viewer, @offering

    respond_to do |format|
      flash.now[:notice] = 'Invite was successfully removed'
      format.js { render :action => "destroy" }
    end
  end

  def resend_invite
    @invitations = current_user.try(:invitations)
    resource = resource_class.find(params[:id])
    resource.invite!

    respond_to do |format|
      if resource.errors.empty?
        set_flash_now_message :notice, :resend_instructions, :email => resource.email
        format.js
      else
        flash.now[:error] = "There was an error resending the invite."
        format.js
      end
    end
  end

  def resend_offering_invite
    resource = resource_class.find(params[:id])
    resource.try(:invited_to_sign_up?) ? resource.invite! : UserMailer.private_access(resource, @offering).deliver

    if resource.errors.empty?
      set_flash_message :notice, :resend_instructions, :email => resource.email
      redirect_to(:back)
    end
  end

  protected

  # def add_roles(company)
    # #invitees can manage the company info
    # resource.add_role(:company_admin, company)
#
    # #invitees can manage any of the company offerings
    # company.offerings.each{|offering| resource.add_role(:offering_admin, offering)}
  # end

  def authorize_offering
    @offering = Offering.find(params[:offering_id])
    authorize! :manage, @offering
  end

  def set_flash_now_message(key, kind, options={})
    options[:scope] = "devise.#{controller_name}"
    options[:default] = Array(options[:default]).unshift(kind.to_sym)
    options[:resource_name] = resource_name
    options = devise_i18n_options(options) if respond_to?(:devise_i18n_options, true)
    message = I18n.t("#{resource_name}.#{kind}", options)
    flash.now[key] = message if message.present?
  end

  def emails_validate?(list)
    list.split(',').each do |email|
      return false if email[/^([^\s]+)((?:[-a-z0-9]\.)[a-z]{2,})$/i].nil?
    end
    return true
  end
end
