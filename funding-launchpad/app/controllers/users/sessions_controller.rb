class Users::SessionsController < Devise::SessionsController

  def create
    resource = warden.authenticate!(:scope => resource_name, :recall => "users/sessions#failure")
    return sign_in_and_redirect(resource_name, resource)
  end

  def sign_in_and_redirect(resource_or_scope, resource=nil)
    scope = Devise::Mapping.find_scope!(resource_or_scope)
    resource ||= resource_or_scope
    sign_in(scope, resource) unless warden.user(scope) == resource
    set_flash_message(:notice, :signed_in, :resource_name => 'sessions') if is_navigational_format?
    return render :json => {:success => true, :redirect => stored_location_for(scope) || after_sign_in_path_for(resource)}
  end

  def failure
    user = User.find_by_email(params[:user][:email])
    if user && user.encrypted_password.blank?
      error_key = "devise.failure.incomplete_registration"
      incomplete = true
    else
      error_key = "devise.failure.invalid"
      incomplete = false
    end
    return render:json => {:success => false, :errors => [I18n.t(error_key)], :incomplete => incomplete }
  end
end