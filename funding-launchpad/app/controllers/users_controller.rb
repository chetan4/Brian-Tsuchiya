class UsersController < ApplicationController
  load_and_authorize_resource except: [:newsletter, :unsubscribe]

  # GET /users
  # GET /users.json
  def index
    @users = User.order(:created_at).page params[:page]
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  def invite
    # We're here because somebody invited us.  We have a password and a registration key
    # in the query string.
    # Look up this invitation to be sure it's real and unused.
    registration_key = params[:r]
    password = params[:p]
    @invitation = Invitation.find_by_registration_key(registration_key)
    if @invitation && @invitation.password == password && @invitation.accepted_on.nil?
      # Good record.  Create a user and register them.
      redirect_to new_invitation_path(:i => @invitation.registration_key)
    else
      redirect_to root_path, :alert => "Illegal Registration Attempt"
    end
  end

  def followings
    user = User.find(params[:id])
    @followed_offerings = []
    user.followings.each do |f|
      @followed_offerings << Offering.find(f.offering_id)
    end
  end

  def unfollow
    @offering = Offering.find(params[:id])
    current_user.unfollow!(@offering)
    redirect_to followings_user_path(current_user)
  end

  def default_following_options
    @dfo = User.find(params[:id]).default_following_option
  end

  def newsletter
    return redirect_to root_path if params[:user].nil?

    # create a new user or add attribute to a new one
    @user = User.new(email: params[:user][:email])
    if @user.save
      UserMailer.newsletter_email(@user).deliver
      flash[:notice] = "Thank you for signing up for the newsletter. You will be the first to hear exciting news from The Funding Launchpad."
    else
      if @user.errors.messages[:email]
        flash[:warning] = "That email address is already subscribed."
      else
        flash[:warning] = "There was a problem, please try again later."
      end
    end
    redirect_to root_path
  end

  def confirm
    user = User.find(params[:id])
    if user.send_confirmation_instructions
      flash[:notice] = "#{user.email} was sent confirmation instructions."
    else
      flash[:warning] = "#{user.email} could not be sent confirmation instructions, try again."
    end
    redirect_to users_path
  end

  def unsubscribe
    return redirect_to root_path if request.env['HTTP_USER_AGENT'].downcase != 'mailchimp.com'
    begin
      user = User.find_by_email(params[:data][:email])
      user.update_attribute(:newsletter, 0)
      render :text => "Email has been unsubscribed", :status => 200
    rescue
      render :text => "Email not found!", :status => 400
    end
  end

  def accredited
    if current_user && current_user.person
      current_user.person.dont_validate_residence = true
      current_user.person.update_attributes(:accredited => true)
    end
    redirect_to :back
  end
end
