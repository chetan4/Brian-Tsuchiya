class ContributionsController < ApplicationController

  load_and_authorize_resource :offering
  load_and_authorize_resource :contribution, :through => :offering

  def index
    @offering = Offering.find(params[:offering_id])
    @contributions = @offering.contributions
  end
  
  def new
    @contribution = Contribution.new
  end

  def create

    @contribution = Contribution.new(params[:contribution])
    @contribution.user_id = current_user.id
    @contribution.status = 'New'
    @offering = Offering.find(params[:offering_id])
    @contribution.offering = @offering
    
    respond_to do |format|
      if save_with_payment(@offering)
        format.html { redirect_to offering_path(@offering), :notice => 'Contribution successfully created.' }
        format.json { render json: @contribution, status: :created, location: @contribution }
      else
        format.html { render action: "new" }
        format.json { render json: @contribution.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  
  def save_with_payment(offering)
    # Our internal amount is in dollars.  Convert it to cents for Stripe
    stripe_amount = @contribution.amount * 100
    # Calculate the minimum allowable charge (rounded up to nearest whole dollar) for comparison
    min_allowed_charge = (STRIPE_PER_TRANS_FEE / (FL_USAGE_FEE - STRIPE_PER_TRANS_PERCENTAGE)).round(-2)
    if stripe_amount < min_allowed_charge
      flash.now[:error] = "Sorry, the minimum allowable contribution is #{helpers.number_to_currency(min_allowed_charge/100.0)}."
      return false
    end
    # Calculate the transaction cost (orginally %2.9 + $.30)
    transaction_cost = stripe_amount * STRIPE_PER_TRANS_PERCENTAGE + STRIPE_PER_TRANS_FEE
    # Our fee is 8%, but we are eating the transaction costs so we subtract those
    fl_fee = stripe_amount * FL_USAGE_FEE - transaction_cost
    begin
      charge = Stripe::Charge.create(
        {
          :amount => stripe_amount,
          :currency => "usd",
          :card => @contribution.stripe_card_token,
          :description => "Contribution to Offering #{offering.id}: #{offering.name}",
          :application_fee => fl_fee.to_i
        },
        offering.access_token
      )
    rescue Stripe::CardError, Stripe::InvalidRequestError, Stripe::AuthenticationError, Stripe::APIConnectionError, Stripe::StripeError => e
      body = e.json_body
      err = body[:error]
      message = err[:message]
      @stripe_error = message
      flash.now[:error] = @stripe_error
      return false
    end
    
    @contribution.save!
  end

  def helpers
    ActionController::Base.helpers
  end
end
