class AuthenticationsController < ApplicationController
  respond_to :html, :js

  def index
    @authentications = current_user.authentications if current_user
  end

  def destroy
    @authentications = current_user.authentications
    @authentication = @authentications.find(params[:id])
    @authentication.destroy

    respond_to do |format|
      flash[:notice] = "Successfully removed #{@authentication.provider.titleize}"
      format.html {  redirect_to authentications_path }
      format.js
    end

  end
end
