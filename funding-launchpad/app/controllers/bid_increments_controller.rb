class BidIncrementsController < ApplicationController
  before_filter :authenticate_user!, except: [:show]
  load_and_authorize_resource

  def index
  end
  
  def new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bid_increment }
    end
  end

  def edit
  end

  def create
    respond_to do |format|
      if @bid_increment.save
        format.html { redirect_to bid_increments_path, notice: 'Bid increment was successfully created.' }
        format.json { render json: @reward, status: :created, location: @bid_increment }
      else
        format.html { render action: "new" }
        format.json { render json: @bid_increment.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @bid_increment = BidIncrement.find(params[:id])

    respond_to do |format|
      if @bid_increment.update_attributes(params[:bid_increment])
        format.html { redirect_to bid_increments_path, notice: 'Bid increment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @bid_increment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @bid_increment = BidIncrement.find(params[:id])
    @bid_increment.destroy

    respond_to do |format|
      format.html { redirect_to bid_increments_url }
      format.json { head :no_content }
    end
  end
end
