class OfferingsController < ApplicationController
  skip_before_filter :authenticate_user!, :only => [:show, :index, :investors]
  load_and_authorize_resource :except => [:index, :follow]

  def index

    if params[:filter] and Offering.valid_filter?(params[:filter])
      if params[:filter] == "by_state"
        @offerings = Offering.send(params[:filter], current_user.person.state_of_residence)
      else
        @offerings = Offering.send(params[:filter])
      end
      #@offerings = case
      #  when params[:filter] == "by_state" then Offering.send(params[:filter], current_user.person.state_of_residence)
      #  else Offering.send(params[:filter])
      #  end
    else
      @offerings = Offering.active_and_invited(current_user)
    end
    
    # PCP code not used in franchise system
    # #filter out any pcp offerings so they aren't shown on main site
    # if !is_pcp?
    #   pcp_names = PCP_CONFIG.map{|pcp| pcp[1]['name']}
    #   @offerings = @offerings.find_all{|offering| !pcp_names.include?(offering.company.try(:name))}
    # end

  end

  def admin
    redirect_to root_path unless signed_in?
    @offering = Offering.find(params[:id])
    if request.path != admin_offering_path(@offering)
      redirect_to @offering, status: :moved_permanently
    end
  end

  def show
    redirect_to root_path unless current_user
    @offering = Offering.find(params[:id])
    # redirect_to root_path unless @offering.active?

    # don't show pcp offerings on the main site
    if !is_pcp?
      PCP_CONFIG.each do |pcp|
        redirect_to root_path if pcp[1]['name'] == @offering.company.try(:name)
      end
    end
    #only show the pcp's offerings
    if is_pcp?
      redirect_to root_path unless @offering.id == PCPConstraint.id
    end

    display_can_invest(@offering)
  end

  def investors
    @offering = Offering.find(params[:id])
    @investments = Investment.escrowed.find_all_by_offering_id(@offering.id)
    display_can_invest(@offering)
  end
  
  def contributors
    @offering = Offering.find(params[:id])
    @contributions = Contribution.find_all_by_offering_id(@offering.id)
  end

  def investment_history
    @statuses = InvestmentStatus.order("created_at desc").find_all_by_investment_id(params[:ii])
  end

  def change_status
    @investment = Investment.find(params[:ii])
  end

  def new
    redirect_to signin_path unless signed_in?
    @offering = Offering.new
  end

  def edit
    @offering = Offering.find(params[:id])
  end

  def create
    @offering = Offering.new(params[:offering])
    @offering.contact_id = current_user.id
    @offering.status = IN_DEVELOPMENT

    respond_to do |format|
      if @offering.save
        current_user.add_role :offering_admin, @offering
        format.html { redirect_to admin_offering_path(@offering), notice: "Congratulations!  Your offering was created. Please fill out the remaining sections" }
        format.json { render json: @offering, status: :created, location: @offering }
      else
        format.html { render action: "new" }
        format.json { render json: @offering.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @offering = Offering.find(params[:id])
    @offering.attributes = params[:offering]
    @offering.updating_versioned_fields = @offering.versioned_fields_changed?

    respond_to do |format|
      if @offering.save
        format.html { redirect_to admin_offering_path(@offering), notice: 'Offering successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @offering.errors, status: :unprocessable_entity }
      end
    end
  end

  def complete
    @offering = Offering.find(params[:id])
    new_status = @offering.status & ~IN_DEVELOPMENT
    new_status |= DEVELOPMENT_COMPLETED
    respond_to do |format|
      if @offering.update_attribute(:status, new_status)
        AdminMailer.offering_complete(@offering).deliver
        format.html { redirect_to admin_offering_path(@offering), 
                      notice: 'You offering has been submitted for approval to launch.' }
        format.json { head :no_content }
      else
        format.html { redirect_to @offering, alert: 'Failed to Update Status'}
        format.json { render json: @offering.errors, status: :unprocessable_entity }
      end
    end
  end

  # Flip status bits so approval is required before making active again, making sure not
  # to reset the HAS_BEEN_ACTIVE bit since versioning support relies on it!
  def uncomplete
    @offering = Offering.find(params[:id])
    new_status = @offering.status & ~DEVELOPMENT_COMPLETED & ~APPROVED_FOR_LAUNCH & ~ACTIVE
    new_status |= IN_DEVELOPMENT
    respond_to do |format|
      if @offering.update_attribute(:status, new_status)
        format.html { redirect_to admin_offering_path(@offering), notice: 'Your approval request has been retracted.' }
        format.json { head :no_content }
      else
        format.html { redirect_to @offering, alert: 'Failed to Update Status'}
        format.json { render json: @offering.errors, status: :unprocessable_entity }
      end
    end
  end

  def approve
    @offering = Offering.find(params[:id])
    new_status = @offering.status | APPROVED_FOR_LAUNCH
    respond_to do |format|
      if @offering.update_attribute(:status, new_status)
        format.html { redirect_to reviews_path, notice: 'Offering approved for launch.' }
        format.json { head :no_content }
      else
        format.html { redirect_to reviews_path, alert: 'Offering not approved!' }
        format.json { render json: @offering.errors, status: :unprocessable_entity }
      end
    end
  end

  def activate
    @offering = Offering.find(params[:id])
    new_status = @offering.status | ACTIVE | HAS_BEEN_ACTIVE

    respond_to do |format|
      if @offering.update_attributes(:status => new_status, :activated_at => @offering.activated_at || Time.now)
        format.html { redirect_to admin_offering_path(@offering), notice: 'Your offering has been activited!' }
        format.json { head :no_content }
      else
        format.html { redirect_to @offering, alert: 'Your offering has not been activated!' }
        format.json { render json: @offering.errors, status: :unprocessable_entity }
      end
    end
  end

  def follow
    # To follow a offering we create a following record and a set of following options.
    # The options are set to the default following options for this user.  They can alter
    # these defaults from their "current followings" page if desired.
    @offering = Offering.find(params[:id])
    current_user.follow!(@offering)
    @dfo = DefaultFollowingOption.find_by_user_id(current_user)
    @fo = FollowingOption.new
    @fo.update_from_defaults(@dfo)
    @fo.offering_id = @offering.id
    @fo.save!
    redirect_to offering_path(@offering)
  end

  def unfollow
    @offering = Offering.find(params[:id])
    current_user.unfollow!(@offering)
    redirect_to offering_path(@offering)
  end

  def followers
    @offering = Offering.find(params[:id])
  end

  def revisions
  end

end
