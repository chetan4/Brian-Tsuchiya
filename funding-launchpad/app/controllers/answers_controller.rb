class AnswersController < ApplicationController
  load_resource :offering
  load_and_authorize_resource :question
  load_and_authorize_resource :answer, :through => :question

  def new
    @show_not_confirmed_message = !current_user.confirmed?
  end

  def create
    @answer.user_id = current_user.id
    if @answer.save
      redirect_to offering_question_path(@offering, @question)
    else
      render "new"
    end
  end
end
