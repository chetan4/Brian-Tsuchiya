class ProjectsController < ApplicationController

  def index
    @projects = Project.active
  end

  def show
    @project = Project.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @project }
    end
  end

  def new
    @project = Project.new
    @title = "Creating a New Project"

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @project }
    end
  end

#  def edit
#    @project = Project.find(params[:id])
#  end

  def create
    @project = Project.new(params[:project])
    @project.user_id = current_user.id
    @project.company_id = current_user.person.company.id
    @project.project_layout = ProjectLayout.new

    respond_to do |format|
      if @project.save
        format.html { redirect_to root_path, notice: 'Project was successfully created.' }
        format.json { render json: @project, status: :created, location: @project }
      else
        format.html { render action: "new" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @project = Project.find(params[:id])

    respond_to do |format|
      if @project.update_attributes(params[:project])
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

end
