require 'oauth2'

class PagesController < ApplicationController

  def admin
    redirect_to root_path unless signed_in? and vim_admin?
  end
  
  def financials
    # Provides review/management access for the overall site
  end

  def help
  end

  def my_offerings
    @offerings = Offering.with_role(:offering_admin, current_user)
  end

  def my_investments
    @investments = Investment.find_all_by_user_id(current_user.id)
  end

  def privacy
  end

  def settings
  end

  def terms
  end

  def new_message
    @message = Message.new
    render :about
  end

  def create_message
    @message = Message.new(params[:message])

    respond_to do |format|
      if @message.valid?
        @success = true
        flash.now[:success] = "Thanks for your input! Your message has been sent and we will get back to you as soon as possible."
        MessageMailer.new_message(@message).deliver
        #redirect_to(root_path, :notice => "Message was successfully sent.")
        format.html { redirect_to about_path, anchor: "contact", notice: "Your message was sent!" }
        format.js
      else
        flash.now[:error] = "Please fill all the fields."
        format.js
      end
    end
  end

  # subscribes user to the "offerings" list in Mailchimp
  def optin
    begin
      # user may not exist in our database
      user = User.find_by_email params[:email]
      user.update_attribute(:pcp_email, true)
      if user.send(:add_to_offerings)
        flash[:success] = "Thank you for opting in! You will be notified as new investment opportunities launch on Funding Launchpad."
        redirect_to root_path
      else
        flash[:error] = "There was an error, please try again."
        redirect_to root_path
      end
    # major hack
    rescue
      mailchimp = Hominid::API.new(FLP["MAILCHIMP_API_KEY"])
      result    = mailchimp.list_subscribe(FLP["MAILCHIMP_OFFERRING_LIST_ID"], params[:email], {}, 'html', false, true, false, true)
      Rails.logger.info("MAILCHIMP SUBSCRIBE: result #{result.inspect} for #{params[:email]}")
      flash[:success] = "Thank you for opting in! You will be notified as new investment opportunities launch on Funding Launchpad."
      redirect_to root_path
    end
  end
  
  # Stripe validation
  def stripe_validation
    @offering = Offering.find params[:state]
    @stripe_code = params[:code]

    client = OAuth2::Client.new(STRIPE_CLIENT_ID, STRIPE_SECRET_KEY, :site => STRIPE_ACCESS_TOKEN_URL)
    
    # Use Bearer Authorization with our secret API key
    stripe_params = {
      :headers => {'Authorization' => "Bearer #{STRIPE_SECRET_KEY}"}
    }
    
    # Make a request to the access_token_uri endpoint to get an access_token and publishable_key
    @token = client.auth_code.get_token(@stripe_code, stripe_params)
    @access_token = @token.token
    @stripe_publishable_key = @token.params["stripe_publishable_key"]
    
    # Store info in offering for handling contributions
    @offering.update_attributes ({:access_token => @access_token, :publishable_key => @stripe_publishable_key})
    
    # Go back to offering admin page
    redirect_to admin_offering_path(@offering)

    ##<OAuth2::AccessToken:0x007fcc904bfe10 @client=#<OAuth2::Client:0x007fcc8c76d580 @id="ca_1GdjKRK978WQZLD1ilouvfP5phyWmLsc", 
    #  @secret="sk_test_ldUUHRpvOCWgplIeWzwnoAE2", 
    #  @site="https://connect.stripe.com/oauth/token", 
    #  @options={:authorize_url=>"/oauth/authorize", 
    #    :token_url=>"/oauth/token", 
    #    :token_method=>:post, 
    #    :connection_opts=>{}, 
    #    :connection_build=>nil, 
    #    :max_redirects=>5, 
    #    :raise_errors=>true}, 
    #  @auth_code=#<OAuth2::Strategy::AuthCode:0x007fcc91868f20 @client=#<OAuth2::Client:0x007fcc8c76d580 ...>>, 
    #  @connection=#<Faraday::Connection:0x007fcc91868ae8 @headers={}, 
    #  @params={}, 
    #  @options={}, 
    #  @ssl={}, 
    #  @parallel_manager=nil, 
    #  @default_parallel_manager=nil, 
    #  @builder=#<Faraday::Builder:0x007fcc918689d0 @handlers=[Faraday::Request::UrlEncoded, Faraday::Adapter::NetHttp]>, 
    #  @url_prefix=#<URI::HTTPS:0x007fcc8c5f48c0 URL:https://connect.stripe.com/oauth/token>, 
    #  @proxy=nil, 
    #  @app=#<Faraday::Request::UrlEncoded:0x007fcc8c2a2a28 @app=#<Faraday::Adapter::NetHttp:0x007fcc8c2a2b18 @app=#<Proc:0x007fcc8c2a3360@/Users/cdoyle/.rvm/gems/ruby-1.9.3-p374@funding-launchpad/gems/faraday-0.8.4/lib/faraday/connection.rb:74 (lambda)>>>>>, 
    #  @token="sk_test_oPoMdmHPD5BiUFzZtmF2VSOP", 
    #  @refresh_token="rt_1KuyozWqV3quT4ULlrcNH4ntD2kiKsFdpPr1dSYi6KV8i84J", 
    #  @expires_in=nil, 
    #  @expires_at=nil, 
    #  @options={:mode=>:header, 
    #    :header_format=>"Bearer %s", 
    #    :param_name=>"bearer_token"}, 
    #  @params={"livemode"=>false, 
    #    "token_type"=>"bearer", 
    #    "stripe_publishable_key"=>"pk_test_3h5x9RZUBv5ugJOH2WkQr6Y6", 
    #    "stripe_user_id"=>"acct_1GHoITg5gubvXP0Vnybt", 
    #    "scope"=>"read_only"}> 
  end
  
end
