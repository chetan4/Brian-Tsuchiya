class PeopleController < ApplicationController
  before_filter :define_new_person, :only => [:new, :create]
  load_and_authorize_resource :except => [:show]

  # GET /people
  # GET /people.json
  def index
  end

  # GET /people/1
  # GET /people/1.json
  def show
    @person = Person.find(params[:id])

    respond_to do |format|
      format.html
    end
  end

  def define_new_person
    @person = Person.new
    @person.user = current_user
    @person.dont_validate_residence = true
  end

  # GET /people/new
  # GET /people/new.json
  def new
    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /people/1/edit
  def edit
  end

  # POST /people
  # POST /people.json
  def create
    respond_to do |format|
      if @person.update_attributes(params[:person])
        @offering = Offering.find(@person.offering_id)
        @offering.people<<@person

        format.html { redirect_to people_path(:offering => "#{@person.offering_id}"), notice: "A new person was successfully created." }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /people/1
  # PUT /people/1.json
  def update
    respond_to do |format|
      if @person.update_attributes(params[:person])
        format.html { redirect_to people_path(:offering => @person.offering_id), notice: 'Person was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    @person = Person.find(params[:id])
    offering_id = @person.offering_id
    @person.destroy

    respond_to do |format|
      format.html { redirect_to people_path(:offering => offering_id), notice: 'Person was successfully removed.' }
    end
  end

  # GET /people/1/profile
  def profile
    respond_to do |format|
      format.js
      format.html { redirect_to settings_path + "#personal" }
    end
  end

  def profile_edit
    if @person.addresses.first == nil
      @person.addresses.build
    end
    respond_to do |format|
      format.js
      format.html { redirect_to settings_path + "#personal" }
    end
  end

  def profile_update
    @person.dont_validate_residence = true
    respond_to do |format|
      if @person.update_attributes(params[:person])
        flash.now[:notice] = 'Profile successfully updated.'
        format.html { redirect_to profile_person_path(@person) }
        format.js
      else
        error_msg = ""
        @person.errors.values.each do |vals|
          vals.each do |msg|
            error_msg += "#{msg} <br>"
          end
        end
        @person.reload
        flash.now[:error] = error_msg.html_safe
        format.js
        format.html { render action: "profile_edit" }
      end
    end
  end
end
