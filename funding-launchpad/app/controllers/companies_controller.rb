class CompaniesController < ApplicationController
  load_and_authorize_resource :offering
  load_and_authorize_resource :company, :through => :offering, :singleton => true

  #currently there is no template for this, so disable
  # def show
    # @company = Company.find(params[:id])
    # respond_to do |format|
      # format.html # show.html.haml
      # format.json { render json: @company }
    # end
  # end

  def new
    @company.build_address
    @company.build_phone
    @company.build_email
  end

  def edit
  end

  def create
    respond_to do |format|
      if @company.save
        format.html {redirect_to admin_offering_path(@offering), notice: "'#{@company.name}' was successfully created"}
      else
        render "new"
      end
    end
  end

  def update
    respond_to do |format|
      if @company.update_attributes(params[:company])
        format.html {redirect_to admin_offering_path(@offering), notice: 'Company was successfully updated.'}
      else
        format.html { render action: "edit" }
      end
    end

  end

end
