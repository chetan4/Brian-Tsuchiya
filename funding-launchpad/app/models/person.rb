class Person < ActiveRecord::Base
  resourcify
  before_save :set_residence, :unless => :dont_validate_residence
  has_many :addresses, :as => :addressable, :dependent => :destroy
  has_many :phones, :as => :phonable, :dependent => :destroy
  has_many :emails, :as => :emailable, :dependent => :destroy
  has_many :details, :as => :detailable, :dependent => :destroy
  has_one :user
  has_and_belongs_to_many :offerings
  has_many :updates

  mount_uploader :profile_picture, ProfilePictureUploader

  accepts_nested_attributes_for :user, :allow_destroy => true
  accepts_nested_attributes_for :addresses

  attr_accessor   :invite, :resident, :dont_validate_residence

  attr_accessible :offering_id,
                  :first_name,
                  :last_name,
                  :bio,
                  :profile_picture,
                  :title,
                  :user_attributes,
                  :invite,
                  :addresses_attributes,
                  :resident,
                  :company_id,
                  :state,
                  :accredited

  validates_presence_of :resident, :message => "must be selected", :unless => :dont_validate_residence
  validate :check_state_presence, :unless => :dont_validate_residence
  validate :check_country_presence, :unless => :dont_validate_residence

  #we need this because attr_accessor stores values as string
  def resident?
    if !self.resident.blank?
      resident.to_bool
    else
      false
    end
  end

  #Set the country if resident and clear the state if not a resident
  def set_residence
    if self.resident?
      self.addresses[0].country_code = "US"
    else
      self.addresses[0].state = nil
    end
  end

  def check_state_presence
    if (self.resident? || self.addresses[0].country_code == "US") && !self.addresses[0].state.present?
      self.errors.add(:resident, "")
      self.addresses[0].errors.add(:state, "must be selected")
    end
  end

  def check_country_presence
    if !self.resident? && !self.addresses[0].country_code.present?
      self.errors.add(:resident, "")
      self.addresses[0].errors.add(:country_code, "must be selected")
    end
  end

  def us_resident?
    addresses.us_residence.any?
  end

  def state_of_residence
    addresses.residence.first.state unless addresses.residence.blank?
  end
  alias_method :state, :state_of_residence

  def has_residence?
    addresses.has_residence?
  end

  def full_name
    "%s %s" % [first_name, last_name]
  end

  def state=(state)
    if !addresses.empty?
      addresses.residence.first.update_attribute(:state, state)
    else
      address = Address.new
      address.state = state
      addresses << address
    end
  end
end
