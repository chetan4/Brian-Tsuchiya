# Generic class for handling Meracord interaction

# <ns0:AccountTypeId>3</ns0:AccountTypeId>
# <ns0:Address>1001 Pacific Avenue Suite 300</ns0:Address>
# <ns0:City>Tacoma</ns0:City>
# <ns0:ClientId>MyUniqueClientID</ns0:ClientId>
# <ns0:EMailAddress>john.doe@noemail.com</ns0:EMailAddress>
# <ns0:GroupNumber>99999</ns0:GroupNumber>
# <ns0:HoldTypeId>0</ns0:HoldTypeId>
# <ns0:Name2>Jane Doe</ns0:Name2>
# <ns0:NameFirst>John</ns0:NameFirst>
# <ns0:NameLast>Doe</ns0:NameLast>
# <ns0:PhoneNumber>2535551212</ns0:PhoneNumber>
# <ns0:State>WA</ns0:State>
# <ns0:TaxId>987685978</ns0:TaxId>
# <ns0:Zip>98402</ns0:Zip>
# 

class Meracord

  MERACORD_ACCOUNT_WSDL = "https://sandbox.meracord.com/api/data/Account.svc?wsdl"
  MERACORD_USERNAME = "RandomSPAC66F65F"
  MERACORD_PASSWORD = "?!cH1`#l-2"
  MERACORD_GROUP_NUMBER = "50100"
  
  attr_accessor :response_error
  
  # Account Management
  
  def build_meracord_account_client ( wsdl = MERACORD_ACCOUNT_WSDL )
    client = Savon.client(wsdl)
    client.wsse.credentials MERACORD_USERNAME, MERACORD_PASSWORD
    client
  end
    
  def can_authenticate?
    client = build_meracord_account_client MERACORD_ACCOUNT_WSDL
    response = client.request :can_authenticate
    response.to_array(:can_authenticate_response, :can_authenticate_result).first
  end
  
  def create_account ( client_id )
    # Use the client_id as an offering_id and go get the stuff we'll need

    # Build the Meracord call
    # <ns0:AccountTypeId>3</ns0:AccountTypeId>
    # <ns0:Address>1001 Pacific Avenue Suite 300</ns0:Address>
    # <ns0:City>Tacoma</ns0:City>
    # <ns0:ClientId>MyUniqueClientID</ns0:ClientId>
    # <ns0:EMailAddress>john.doe@noemail.com</ns0:EMailAddress>
    # <ns0:GroupNumber>99999</ns0:GroupNumber>
    # <ns0:HoldTypeId>0</ns0:HoldTypeId>
    # <ns0:Name2>Jane Doe</ns0:Name2>
    # <ns0:NameFirst>John</ns0:NameFirst>
    # <ns0:NameLast>Doe</ns0:NameLast>
    # <ns0:PhoneNumber>2535551212</ns0:PhoneNumber>
    # <ns0:State>WA</ns0:State>
    # <ns0:TaxId>987685978</ns0:TaxId>
    # <ns0:Zip>98402</ns0:Zip>

    client = build_meracord_account_client MERACORD_ACCOUNT_WSDL
    response = client.request :create do
      soap.namespaces["xmlns:tns"] = "http://tempuri.org/"
      soap.namespaces["xmlns:ns0"] = "http://schemas.datacontract.org/2004/07/NoteWorld.DataServices.Common.Transport"

#      soap.body do |xml|
#        @offering = Offering.find(client_id)
#        xml.AccountTypeId(3)
#        xml.Address(@offering.company.address.address1)
#        xml.City(@offering.company.address.city)
#        xml.ClientId("Client#{@offering.id}")
#        xml.EmailAddress(@offering.company.email.content)
#        xml.GroupNumber(MERACORD_GROUP_NUMBER)
#        xml.HoldTypeId(0)
#        xml.Name2("#{@offering.contact.person.first_name} #{@offering.contact.person.last_name}")
#        xml.NameFirst(@offering.contact.person.first_name)
#        xml.NameLast(@offering.contact.person.last_name)
#        xml.PhoneNumber(@offering.company.phone.content)
#        xml.State(@offering.company.address.state)
#        xml.TaxId(@offering.company.tax_id)
#        xml.Zip(@offering.company.address.zip_code)
#      end

      @offering = Offering.find(client_id)
      soap.body = {
        "tns:account" => {
          "ns0:AccountTypeId" => "3",
          "ns0:Address" => @offering.company.address.address1,
          "ns0:City" => @offering.company.address.city,
          "ns0:ClientId" => "Client#{@offering.id}",
          "ns0:EmailAddress" => @offering.company.email.content,
          "ns0:GroupNumber" => MERACORD_GROUP_NUMBER,
          "ns0:HoldTypeId" => "0",
          "ns0:Name2" => "#{@offering.contact.person.first_name} #{@offering.contact.person.last_name}",
          "ns0:NameFirst" => @offering.contact.person.first_name,
          "ns0:NameLast" => @offering.contact.person.last_name,
          "ns0:PhoneNumber" => @offering.company.phone.content,
          "ns0:State" => @offering.company.address.state,
          "ns0:TaxId" => @offering.company.tax_id,
          "ns0:Zip" => @offering.company.address.zip_code
        }
      }
    end
    response.to_array(:create_response, :create_result).first
  end
  
  def find_account ( client_id )
    begin
      client = build_meracord_account_client MERACORD_ACCOUNT_WSDL
      response = client.request :find do
        soap.namespaces["xmlns:tem"] = "http://tempuri.org"
        soap.body = "<tns:groupNumber>#{MERACORD_GROUP_NUMBER}</tns:groupNumber><tns:clientId>Client#{client_id}</tns:clientId>"
      end
    rescue Savon::SOAP::Fault => fault
      @response_error = fault
    end
    response.to_array(:find_response, :find_result).first unless response.nil?
  end
end
