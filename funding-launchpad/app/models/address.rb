class Address < ActiveRecord::Base
  belongs_to :addressable, :polymorphic => true

  scope :residence, where('addresses.content_type = ?', 'Residence')
  scope :us_residence, residence.where(country_code: 'US')

  def self.has_residence?
    residence.present? && residence.where(country_code: ['', nil]).empty?
  end
end
