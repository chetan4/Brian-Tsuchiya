class Contact < ActiveRecord::Base
  BUSINESS_STATES = ['Generating Revenue', 'Prototype', 'Pre-Prototype/Revenue']
  INDUSTRIES      = ['Consumer Goods', 'Restaurant', 'Retail', 'Software', 'Green/Sustainable/Local', 'Health', 'B2B', 'Sports and Leisure', 'Other']
  REFERRERS       = ['Presentation', 'Search Engine', 'Referral', 'Twitter', 'Facebook', 'LinkedIn', 'Industry Group', 'Media']

  attr_accessor :industry_input, :newsletter

  validates_presence_of :name,
                        :business_state,
                        :description,
                        :industry,
                        :raise_amount

  validates :email, :presence => true, :email => true
  validates_inclusion_of :business_state, :in => BUSINESS_STATES
  validates_inclusion_of :investor_ready, :in => [true, false]
  validates_inclusion_of :referral,       :in => REFERRERS, :allow_nil => true, :allow_blank => true

  before_validation :set_industry
  after_create      :add_user_to_mailchimp

private

  def set_industry
    self.industry = self.industry_input unless self.industry_input.blank?
  end

  def define_info
    {}.tap do |results|
      results[:FNAME]     = self.name.split(" ").first || ""
      results[:LNAME]     = self.name.split(" ")[1..-1].join(" ") || ""
      results[:GROUPINGS] = [ {'name' => 'Interested in...', 'groups' => 'Issuing'} ]
    end
  end

  def add_user_to_mailchimp
    return true unless self.newsletter == "1"
    begin
      mailchimp = Hominid::API.new(FLP["MAILCHIMP_API_KEY"])
      result    = mailchimp.list_subscribe(FLP["MAILCHIMP_NEWSLETTER_LIST_ID"], self.email, define_info, 'html', false, true, false, false) if Rails.env.production?
      Rails.logger.info("MAILCHIMP SUBSCRIBE: result #{result.inspect} for #{self.email}")
    rescue Hominid::APIError => e
      Rails.logger.info("MAILCHIMP ERROR: #{e.message}")
    end
  end
end
