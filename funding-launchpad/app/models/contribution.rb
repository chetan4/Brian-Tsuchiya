class Contribution < ActiveRecord::Base
  attr_accessor :stripe_card_token
  attr_accessible :amount, :offering_id, :payment_method, :status, :user_id, :stripe_card_token
  belongs_to :offering
  belongs_to :user
  
end
