class Reward < ActiveRecord::Base
  belongs_to :offering
  has_many :user_rewards

  mount_uploader :photo, RewardsPhotoUploader
  
  scope :rewards, where(reward_type: 'Reward').order(:price)
  scope :coupons, where(reward_type: 'Coupon')
  scope :auctions, where(reward_type: 'Auction')

  validates_presence_of :title
  validates_numericality_of :price, greater_than_or_equal_to: 0.00
  validates_numericality_of :number_available, greater_than_or_equal_to: 1, :unless => Proc.new { |reward| reward.number_available.nil? }
  validates_numericality_of :number_granted, 
                            :less_than_or_equal_to => Proc.new { |reward| reward.number_available }, 
                            :unless => Proc.new { |reward| reward.number_available.nil? }
  validates_numericality_of :base_cost, greater_than_or_equal_to: 0.00
  validates_numericality_of :ancillary_cost, greater_than_or_equal_to: 0.00
                            
  
  def price_label
    if reward_type == 'Reward'
      "Contribution Requirement:"
    elsif reward_type == 'Coupon'
      "Purchse Price:"
    else
      "Current Bid:"
    end
  end
  
  def rewards_left
    if number_available.nil?
      "Unlimited"
    else
      "#{number_available-UserReward.where(reward_id: id).count} of #{number_available} left"
    end
  end
  
end
