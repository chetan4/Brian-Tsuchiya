class Investment < ActiveRecord::Base

  STATES = [
    'Expression of Interest Received from Investor',          # sends email to Issuer and FundingLaunchpad to start document signing process
    'Signed Subscription Document Received from Investor',    # just a visual notification
    'Counter-signed Subscription Document Sent to Investor',  # sends email to Investor the instructions for sending money for escrow
    'Investor Funds Received by Escrow Agent',                # sends email to Investor notifying them of funds received
    'Investment Offer Declined',                              # sends email to Investor citing their declined offer (eg offering closed)
  ]

  belongs_to :offering
  belongs_to :user
  has_many :investment_statuses

  before_save :define_status

  validates :offered_amount,
            :presence => true,
            :numericality => {
              :greater_than_or_equal_to => 0
            }
  validate :greater_than_share_price
  validate :greater_than_min_investment
  validate :no_remainder

  scope :escrowed, lambda { where ({status: self.completed}) }

  def self.initiated
    STATES.first
  end

  def self.signed
    STATES.second
  end

  def self.countersigned
    STATES.third
  end

  def self.declined
    STATES.last
  end

  def self.completed
    STATES.fourth
  end

  def greater_than_share_price
    if offered_amount && (self.offering.share_price > offered_amount)
      errors.add(:offered_amount, "must be greater than or equal to the share price")
    end
  end

  def greater_than_min_investment
    if offered_amount && offered_amount < self.offering.min_investment
      errors.add(:offered_amount, "must be greater than or equal to the minimum investment")
    end
  end

  def no_remainder
    if offered_amount && (offered_amount % self.offering.share_price != 0)
      errors.add(:offered_amount, "must be a multiple of the share price")
    end
  end

  def num_shares_offered
    (offered_amount / self.offering.share_price).to_i
  end

  def define_status
    if new_record?
      self.status = Investment.initiated
    end
  end
end
