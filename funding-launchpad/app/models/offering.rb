class Offering < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: [:slugged, :history]

  resourcify

  before_save :clear_fields

  FILTERS = ["by_state", "ending_soon", "funded"]
  VERSIONED_FIELDS = [:short_description, :edit_summary]
  VERSIONED_FIELDS_NO_SUMMARY = VERSIONED_FIELDS - [:edit_summary]
  REPAYMENTS = {:monthly => 12, :quarterly => 4, :semi_annual => 2, :annual => 1}

  #We want paper_trail to store the edit_summary in the verison,
  #therefore we need to include it in the fields it tracks.
  has_paper_trail :class_name => 'OfferingVersion',
                  :if => Proc.new { |o| o.has_been_active? and o.versioned_fields_changed? },
                  :only => VERSIONED_FIELDS


  has_one :company
  belongs_to :contact, :class_name => "User"
  belongs_to :group
  has_many :contributions
  has_many :followings
  has_and_belongs_to_many :people
  has_many :photos
  has_many :documents
  has_many :links
  has_many :rewards
  has_many :auctions
  has_many :user_rewards
  has_many :investments
  has_many :updates
  has_many :questions
  has_many :videos
  has_many :filings
  has_many :events
  has_many :team_members, :order => 'position ASC'
  mount_uploader :logo, LogoUploader
  attr_accessible :name,
                  :investments_enabled,
                  :share_price,
                  :min_investment,
                  :min_shares,
                  :max_shares,
                  :shares_issued,
                  :desired_start_date,
                  :expiration_date,
                  :formatted_expiration_date,
                  :short_description,
                  :long_description,
                  :logo,
                  :instructions,
                  :status,
                  :activated_at,
                  :people_attributes,
                  :photos_attributes,
                  :documents_attributes,
                  :links_attributes,
                  :rewards_attributes,
                  :state_tokens,
                  :private,
                  :ppm,
                  :edit_summary,
                  :debt,
                  :term,
                  :interest_rate,
                  :repayment,
                  :minimum_goal,
                  :contributions_enabled,
                  :contribution_goal,
                  :rewards_enabled,
                  :rewards_display_orientation,
                  :rewards_horizontal_alignment,
                  :rewards_max_displayed,
                  :rewards_default_auction_duration,
                  :rewards_gift_heading,
                  :rewards_gift_subheading,
                  :rewards_coupon_heading,
                  :rewards_coupon_subheading,
                  :rewards_auction_heading,
                  :rewards_auction_subheading,
                  :show_investor_count,
                  :show_social_media,
                  :show_progress_bar,
                  :access_token,
                  :publishable_key

  attr_accessor :updating_versioned_fields
  attr_reader :state_tokens
  accepts_nested_attributes_for :filings, :allow_destroy => true

  validates :name,              :presence => true
  
  unless CONTRIBUTIONS_ONLY
    validates :share_price,       :numericality => {
                                    :greater_than_or_equal_to => 0
                                  }, :unless => :debt
    validates :min_shares,        :numericality => {
                                    :greater_than_or_equal_to => 1,
                                    :less_than_or_equal_to => :max_shares,
                                    :message => "must be greater than zero and less than or equal to Maximum shares."
                                  }, :unless => :debt
    validates :max_shares,        :numericality => {
                                    :greater_than_or_equal_to => :min_shares,
                                    :message => "must be greater than or equal to Minimum shares."
                                  }, :unless => :debt
    validates :shares_issued,     :numericality => {
                                    :greater_than_or_equal_to => 0,
                                    :message => "must be greater than or equal to zero"
                                  }, :unless => :debt
    validates :min_investment,    :numericality => {
                                    :greater_than_or_equal_to => :share_price,
                                    :message => "must be greater than or equal to share price."
                                  }, :unless => :debt

    validates :min_investment,    :numericality => {
                                    :greater_than => 0,
                                    :message => "must be greater than zero"
                                  }, :if => :debt

    validates :interest_rate,     :numericality => {
                                    :greater_than_or_equal_to => 0,
                                    :message => "must be greater than or equal to zero"
                                  }, :if => :debt

    validates :minimum_goal,    :numericality => {
                                    :greater_than_or_equal_to => :min_investment,
                                    :message => "must be greater than or equal to minimum investment"
                                  }, :if => :debt

    validates_presence_of :term, :if => :debt
  end

  before_save :chronic_duration, :if => :debt

  validates_presence_of :edit_summary, :on => :update, :if => :should_validate_edit_summary

  scope :in_development, where("(offerings.status & #{IN_DEVELOPMENT}) > 0")
  scope :development_completed, where("(offerings.status & #{DEVELOPMENT_COMPLETED}) > 0")
  scope :approved, where("(offerings.status & #{APPROVED_FOR_LAUNCH}) > 0")
  scope :unapproved, where("(offerings.status & #{APPROVED_FOR_LAUNCH}) = 0")
  scope :public, where("private = ?", false)
  scope :private, where("private = ?", true)
  scope :active, public.where("(offerings.status & #{ACTIVE}) > 0")
  scope :active_or_upcoming, public.where("(offerings.status & (#{ACTIVE} | #{APPROVED_FOR_LAUNCH})) > 0")
  scope :funding, public.where("(offerings.status & #{FUNDING}) > 0")
  scope :funded, public.where("(offerings.status & #{FUNDED}) > 0")
  scope :disabled, public.where("(offerings.status & #{DISABLED}) > 0")
  scope :closed, public.where("(offerings.status & #{CLOSED}) > 0")
  scope :has_been_active, public.where("(offerings.status & #{HAS_BEEN_ACTIVE}) > 0")
  scope :by_state, lambda{ |state| joins(:filings).where("filings.state = ?", state).active}
  scope :ending_soon, active.where("expiration_date < ?", Date.today + 7.days)

  def investable?
    investments_enabled
  end
  
  def contributable?
    contributions_enabled
  end
  
  def clear_fields
    if debt?
      self.shares_issued = nil
      self.min_shares = nil
      self.max_shares = nil
      self.share_price = nil
    else
      self.term = nil
      self.interest_rate = nil
      self.repayment = nil
      self.minimum_goal = self.share_price * self.min_shares
    end
  end

  def should_validate_edit_summary
    has_been_active? and updating_versioned_fields
  end

  def self.active_and_invited(user)
    if (user && user.invitation_offering.nil?) || user.nil?
      active_or_upcoming
    else
      active_or_upcoming << Offering.find(user.invitation_offering)
    end
  end

  def self.valid_filter?(filter)
    FILTERS.include?(filter) and respond_to?(filter)
  end

  #we don't care about tracking a version if the edit_summary field is changed,
  #so exclude it
  def versioned_fields_changed?
    VERSIONED_FIELDS_NO_SUMMARY.map{ |f| send(f.to_s + "_changed?")}.include? true
  end

  def formatted_expiration_date=(date)
    self[:expiration_date] = formatted_date(date)
  end

  def formatted_expiration_date
    self[:expiration_date]
  end

  def share_price=(amount)
    amount = amount.gsub(",", "") if amount.is_a?(String)
    self[:share_price] = amount
  end

  def shares_issued=(amount)
    amount = amount.gsub(",", "") if amount.is_a?(String)
    self[:shares_issued] = amount.to_i
  end

  def current_state_tokens
    self.filings.map {|r| r.state.blank? ? nil : {:id => r.state, :name => Carmen::Country.coded('US').subregions.coded(r.state).name}}.to_json
  end

  def state_tokens=(ids)
    self.filings.destroy_all
    ids.split(",").each {|s| self.filings.build({:state => s})}
  end

  def desired_start
    desired_start_date.strftime("%m/%d/%Y") unless desired_start_date.nil?
  end

  def activation
    activated_at.strftime("%m/%d/%Y") unless activated_at.nil?
  end

  def expiration
    expiration_date.strftime("%m/%d/%Y") unless expiration_date.nil?
  end

  def invested
    investments.sum(:accepted_amount)
  end
  
  def contributed
    contributions.sum(:amount)
  end
  
  def contributed_percent
    contribution_goal == 0 ? 0 : contributed.to_f / contribution_goal * 100
  end
  
  def maximum_goal
    share_price * max_shares if max_shares and share_price
  end

  def min_equity
    return unless shares_issued && min_shares
    if shares_issued + min_shares == 0
      0.0
    else
      sprintf("%0.2f", min_shares / (shares_issued + min_shares).to_f * 100)
    end
  end

  def max_equity
    return unless shares_issued && max_shares
    if shares_issued + max_shares == 0
      0.0
    else
      sprintf("%0.2f", max_shares / (shares_issued + max_shares).to_f * 100)
    end
  end

  def valuation_pre
    shares_issued * share_price
  end

  #Min Post Valuation =  (shares issued * share_price) + (share_price * min_shares)
  def min_valuation_post
    valuation_pre + minimum_goal
  end

  #Max Post Valuation =  (shares issued * share_price) + (share_price * max_shares)
  def max_valuation_post
    valuation_pre + maximum_goal
  end

  def min_funded_percent
    invested > 0 ? ((invested/minimum_goal) * 100) : 0
  end

  def max_funded_percent
    invested > 0 ? ((invested/maximum_goal) * 100).round : 0
  end

  def min_of_max_percent
    if minimum_goal > 0 && maximum_goal > 0
      (minimum_goal/maximum_goal * 100).round
    else
      0
    end
  end

  def offering_status
    case
    when status & CLOSED > 0
      "Closed"
    when status & DISABLED > 0
      "Disabled"
    when status & ACTIVE > 0
      "Active"
    when status & APPROVED_FOR_LAUNCH > 0
      "Approved For Launch"
    when status & DEVELOPMENT_COMPLETED > 0
      "Development Completed"
    when status & IN_DEVELOPMENT > 0
      "In Development"
    when status & HAS_BEEN_ACTIVE > 0
      "Has Been Active"
    end
  end

  def offering_status?(flag)
    status & flag > 0
  end

  def in_development?
    offering_status?(IN_DEVELOPMENT)
  end

  def development_completed?
    offering_status?(DEVELOPMENT_COMPLETED)
  end

  def approved_for_launch?
    offering_status?(APPROVED_FOR_LAUNCH)
  end

  def active?
    offering_status?(ACTIVE)
  end

  def funded?
    offering_status?(FUNDED)
  end

  def has_been_active?
    offering_status?(HAS_BEEN_ACTIVE)
  end

  def filed_in_state?(state)
    filings.map {|f| f.state}.include?(state)
  end

  def upcoming_events(limit=nil)
      limit.nil? ? events.upcoming : events.upcoming.limit(limit)
  end

  def num_followers
    followings.size
  end

  def num_investors
    investments.escrowed.select('distinct user_id').size
  end
  
  def num_contributors
    contributions.select('distinct user_id').size
  end

  def filed_in_states
    filings.map(&:state).sort
  end

  def min_goal_reached?
    invested >= minimum_goal
  end

  def required_docs
    documents.select { |doc| doc.required? }
  end

  def final_weeks?
    return false if self.expiration_date.nil?
    return false if self.expiration_date < Date.today
    time_diff = Time.diff(Time.now.utc, self.expiration_date.end_of_day, '%w')
    time_diff[:diff][/\d+/].to_i <= 4
  end

  def expired?
    return false if expiration_date.nil?
    expiration_date < Date.today
  end

  def repayment_text
    REPAYMENTS.key(self.repayment).to_s.titlecase
  end

private

  def formatted_date(amount)
    return Date.strptime(amount, '%m/%d/%Y') if /\d{2}\/\d{2}\/\d{4}/.match(amount)
    return Date.strptime(amount, '%Y/%m/%d') if /\d{4}\/\d{2}\/\d{2}/.match(amount)
  end

  def chronic_duration
    begin
      self.term = ChronicDuration::parse(self.term_before_type_cast) if self[:term]
    rescue ChronicDuration::DurationParseError => e
      self.term = nil
      self.errors.add :term, e.message
      return false
    end
  end

  def term
    ChronicDuration::output(self[:term], :format => :long) if self[:term]
  end
end
