class UserReward < ActiveRecord::Base
  belongs_to :user
  belongs_to :reward
  belongs_to :offering
  has_one :address, :as => :addressable, :dependent => :destroy

  accepts_nested_attributes_for :address

  attr_accessor   :pay_info_credit_card_number, :pay_info_name_on_card, :pay_info_expiration_date, :pay_info_security_code, 
                  :pay_info_charge_amount
  attr_accessible :reward_id,
                  :user_id,
                  :offering_id,
                  :price,
                  :email,
                  :payment_method,
                  :fulfillment_status,
                  :user_delivery_note,
                  :quantity,
                  :fulfilled_at,
                  :pay_info_credit_card_number,
                  :pay_info_name_on_card,
                  :pay_info_expiration_date,
                  :pay_info_security_code,
                  :pay_info_charge_amount,
                  :address_attributes
                  
  validates_numericality_of :quantity, 
                            :greater_than_or_equal_to => 1
     
  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |user_reward|
        csv << user_reward.attributes.values_at(*column_names)
      end
    end
  end
                         
  def load_address_from_user(user)
    if user && user.person.addresses.first
      user_address = user.person.addresses.first      
      address.address1 = user_address.address1
      address.address2 = user_address.address2
      address.city = user_address.city
      address.state = user_address.state
      address.zip_code = user_address.zip_code
      address.content_type = user_address.content_type
      address.country_code = user_address.country_code
    end
  end
  
  def address_as_text(address)
    text_address = ""
    text_address << address.address1
    text_address << "<br>" unless address.address1.blank?
    text_address << address.address2
    text_address << "<br>" unless address.address2.blank?
    text_address << address.city
    text_address << ", " unless address.city.blank?
    text_address << address.state
    text_address << " " unless address.state.blank?
    text_address << address.zip_code
    text_address
  end

  def address_as_xls_text(address)
    text_address = ""
    text_address << address.address1
    text_address << "\n" unless address.address1.blank?
    text_address << address.address2
    text_address << "\n" unless address.address2.blank?
    text_address << address.city
    text_address << ", " unless address.city.blank?
    text_address << address.state
    text_address << " " unless address.state.blank?
    text_address << address.zip_code
    text_address
  end

end
