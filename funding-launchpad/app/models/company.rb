class Company < ActiveRecord::Base
  resourcify
  belongs_to :offering

  has_one :address, :as => :addressable
  has_one :phone, :as => :phonable
  has_one :email, :as => :emailable

  attr_accessible :name,
                  :description,
                  :address_attributes,
                  :phone_attributes,
                  :email_attributes

  accepts_nested_attributes_for :address, :allow_destroy => true
  accepts_nested_attributes_for :phone, :allow_destroy => true
  accepts_nested_attributes_for :email, :allow_destroy => true

  validates :name, :presence => true

end
