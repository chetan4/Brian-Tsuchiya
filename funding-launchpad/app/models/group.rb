class Group < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: [:slugged, :history]

  resourcify

  has_many :offerings
  attr_accessible :description, :name, :subdomain

  validates_presence_of :name
  validates_uniqueness_of :subdomain

end
