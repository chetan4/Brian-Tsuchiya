class Ability
  include CanCan::Ability
  include ApplicationHelper

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities

    user ||= User.new # guest user (not logged in)

    if user.has_role? :vim_admin
      can :manage, :all
    else
      can [:investors, :show, :follow, :unfollow], Offering.public.active
      can [:investors, :show, :follow, :unfollow], Offering.private.active, id: Offering.with_role(:private_viewer, user).map{|offering| offering.id}
      can [:read, :new], [Question, Answer]
      can [:create], [Question, Answer] if user.confirmed?
      can :create, Investment, :offering => {:ppm => false} if user.confirmed?
      can :create, Investment, :offering => {:ppm => true} if user.confirmed? && user.try(:person).try(:accredited?)
      can [:new, :share], Investment #user shouldn't see index, only new and share page
      can :read, Event
      can :revisions, Update

      can :index, Update

      can [:manage], User, :id => user.id
      cannot :index, User

      can :manage, [Person, DefaultFollowingOption], :user => {:id => user.id}
      can [:manage], [Update, Video, Photo, Document, Link, Investment, Event, TeamMember, Company], offering: {:id => Offering.with_role(:offering_admin, user).map{ |offering| offering.id }}
      can :read, Event

      can :manage, Offering, :id => Offering.with_role(:offering_admin, user).map{ |offering| offering.id }
      cannot :approve, Offering #Only vim_admin can approve
      can [:new, :create], Offering if not_pcp_or_pcp_admin?(user)
      can :read, Offering # Anybody can view an offering

      can :index, Group
      can [:new, :create], Group if user.has_role? :group_creator
      can [:show, :update, :members, :toggle_offering], Group, :id => Group.with_role(:group_admin, user).map{ |group| group.id }
      
      can :manage, Reward
      can :manage, Auction
      can :manage, UserReward
      can [:create, :read], Contribution
    end
  end
end

