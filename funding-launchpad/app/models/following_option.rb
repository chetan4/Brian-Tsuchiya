class FollowingOption < ActiveRecord::Base
  belongs_to :user

  def update_from_defaults(dfo)
    self.user_id = dfo.user_id
    self.new_offering_update = dfo.new_offering_update
    self.new_offering_qa = dfo.new_offering_qa
    self.investment_offered = dfo.investment_offered
    self.investment_accepted = dfo.investment_accepted
    self.investment_declined = dfo.investment_declined
    self.investment_funds_sent = dfo.investment_funds_sent
    self.investment_escrowed = dfo.investment_escrowed
    self.investment_returned = dfo.investment_returned
    self.offering_funded = dfo.offering_funded
    self.offering_content_changed = dfo.offering_content_changed
  end
end
