class RewardPayment < ActiveRecord::Base
  attr_accessible :amount, :payment_method, :reward_id, :user_id
end
