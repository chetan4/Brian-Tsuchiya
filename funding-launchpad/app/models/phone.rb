class Phone < ActiveRecord::Base
  belongs_to :phonable, :polymorphic => true
  validates_presence_of :content
end
