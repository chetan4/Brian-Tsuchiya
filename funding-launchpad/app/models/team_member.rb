class TeamMember < ActiveRecord::Base
  acts_as_list :scope => 'offering_id = #{offering_id}'

  belongs_to :offering
  attr_accessible :bio, :name, :picture, :title, :position

  validates_presence_of :name
  mount_uploader :picture, ProfilePictureUploader
end
