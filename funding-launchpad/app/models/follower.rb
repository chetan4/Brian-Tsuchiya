class Follower < ActiveRecord::Base
  validates_presence_of :offering_id, :user_id
end
