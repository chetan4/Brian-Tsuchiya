require 'file_size_validator'

class Photo < ActiveRecord::Base
  include ResetDefault
  belongs_to :offering
  mount_uploader :image, ImageUploader

  validates_presence_of :caption
  validates :image,
    :presence => true,
    :file_size => {
      :maximum => 5.megabytes.to_i
    }

  before_save :reset_defaults
end
