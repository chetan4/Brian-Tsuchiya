class Update < ActiveRecord::Base
  has_paper_trail :class_name => 'UpdateVersion'
  belongs_to :offering
  belongs_to :person

  validates_presence_of :subject, :content
  validates_presence_of :edit_summary, :on => :update

  default_scope :order => 'created_at DESC'
end
