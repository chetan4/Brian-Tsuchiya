class Question < ActiveRecord::Base
  belongs_to :offering
  belongs_to :user
  has_many :answers
  accepts_nested_attributes_for :answers, :reject_if => lambda { |a| a[:content].blank? }

  validates :content, :presence => true

  #Returns an array of users who have participated by asking the question or answering it
  def participants
    users = []
    # add this user
    users << user

    #add each user that has contributed to the thread by answering the question
    users.concat answers.map{|answer| answer.user} if answers.present?

    #return users
    users
  end
end
