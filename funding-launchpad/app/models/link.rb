class Link < ActiveRecord::Base
  belongs_to :offering

  before_save :add_protocol_if_needed

  def add_protocol_if_needed
    self.link = "http://" + self.link unless self.link =~ /.+:\/\/.+/
  end
end
