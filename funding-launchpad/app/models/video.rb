class Video < ActiveRecord::Base
  include ResetDefault
  belongs_to :offering

  auto_html_for :link do
    html_escape
    image
    youtube(:width => 320, :height => 195, :wmode => 'opaque')
    vimeo()
    link :target => "_blank", :rel => "nofollow"
    simple_format
  end

  validates_presence_of :description, :link

  before_save :reset_defaults
end
