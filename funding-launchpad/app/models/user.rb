class User < ActiveRecord::Base
  before_save :create_default_following
  rolify

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :omniauthable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  belongs_to :person, :dependent => :destroy
  has_many :investments
  has_many :contributions
  has_many :questions
  has_many :answers
  has_many :followings
  has_one :default_following_option
  has_many :following_options
  has_many :invitations, :class_name => "User", :as => :invited_by
  has_many :document_viewers
  has_many :documents, :through => :document_viewers
  has_many :authentications, :dependent => :destroy
  has_many :user_rewards
  has_many :auction_preferences

  accepts_nested_attributes_for :person

  # Setup accessible (or protected) attributes for your model
  attr_accessor   :investor
  attr_accessible :email, :password, :password_confirmation,
                  :remember_me, :company_id, :person_attributes,
                  :investor, :newsletter, :pcp_email, :invitation_offering

  after_create    :add_user_to_mailchimp_lists
  before_destroy  :remove_user_from_mailchimp_lists

  validates_uniqueness_of :email

  #TODO: remove these overriding of Devise methods post launch
  # override Devise methods
  # no password is required when the account is created; validate password when the user sets one
  def password_required?
    if PCP_CONFIG.is_pcp?
      authentications.blank? && (!persisted? or (persisted? && encrypted_password.blank?))
    elsif !persisted?
      false
    else
      !password.nil? || !password_confirmation.nil?
    end
  end

  # override Devise method - this prevents confirmation email from sending
  # to beta users
  def confirmation_required?
   PCP_CONFIG.is_pcp? ? true : false
  end

  # override Devise method - this only allows beta users to sign in if they
  # have been confirmed
  def active_for_authentication?
   return super if PCP_CONFIG.is_pcp?
   #
   (confirmed? || confirmation_period_valid?)
  end

  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"], without_protection: true) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end

  def apply_omniauth(omniauth, no_auth=false)
    self.email = omniauth[:email] if omniauth[:email].present? and email.blank?
    self.person.first_name = omniauth[:first_name] if person.first_name.blank?
    self.person.last_name = omniauth[:last_name] if person.last_name.blank?
    #linkedin image doesn't have extension, so it causes validation error with carrierwave so don't use it.
    self.person.remote_profile_picture_url = omniauth[:image] if person.profile_picture.blank? && omniauth[:provider] != 'linkedin'
    self.person.title = omniauth[:title] if omniauth[:title] if person.title.blank?
    self.person.dont_validate_residence = true
    authentications.build(:provider => omniauth[:provider], :uid => omniauth[:uid]) unless no_auth
  end

  def create_default_following
    self.default_following_option ||= DefaultFollowingOption.new
  end

  def following?(offering)
    followings.find_by_offering_id(offering)
  end

  def follow!(offering)
    followings.create!(:offering_id => offering.id)
  end

  def unfollow!(offering)
    followings.find_all_by_offering_id(offering).each {|f| f.destroy}
    following_options.find_all_by_offering_id(offering).each {|f| f.destroy}
  end

  def can_invest?(offering)
    if offering.debt?
      person.accredited?
    else
      person.has_residence? and (!person.us_resident? or offering.filed_in_state?(person.state_of_residence))
    end
  end

  def with_person
    self.build_person
    self.person.addresses.build
    self
  end

  def assign_group_roles(group)
    add_role(:group_admin, group)
    add_role(:group_member, group)
  end

  def offering_contributions(offering)
    offering.contributions.where(:user_id => id).sum(:amount)
  end

  def contributions_applied(offering)
    #user_rewards.where({:user_id => id, :offering_id => offering.id}).sum(:price)
    user_rewards.where({:offering_id => offering.id, :reward_type => "Reward"}).sum(:price)
  end

  def has_bid_on?(auction)
    if auction.bids.count > 0
      user_bids = auction.bids.where(user_id == id)
      if user_bids.count > 0
        true
      else
        false
      end
    else
      false
    end
  end
  
  private

    def add_user_to_mailchimp_lists
      define_list_ids
      add_to_newsletter if self.newsletter?
      add_to_offerings  if self.pcp_email?
    end

    def define_list_ids
      if Rails.env.test?
        FLP["MAILCHIMP_NEWSLETTER_LIST_ID"] = FLP["MAILCHIMP_TESTING_LIST_ID"]
        FLP["MAILCHIMP_OFFERRING_LIST_ID"]  = FLP["MAILCHIMP_TESTING_LIST_ID"]
      end
    end

    def add_to_newsletter
      info = {}.tap do |results|
        group  = self.investor == "true" ? 'Investing' : 'Issuing'
        results[:GROUPINGS] = [ {'name' => 'Interested in...', 'groups' => group} ]
      end
      add_user_to_mailchimp(FLP["MAILCHIMP_NEWSLETTER_LIST_ID"], define_info.merge(info))
    end

    def add_to_offerings
      add_user_to_mailchimp(FLP["MAILCHIMP_OFFERRING_LIST_ID"], define_info)
    end

    def define_info
      {}.tap do |results|
        results[:FNAME] = self.person.first_name || "" if self.person && self.person.first_name
        results[:LNAME] = self.person.last_name || ""  if self.person && self.person.last_name
      end
    end

    def add_user_to_mailchimp(list_id, info, test=false)
      begin
        mailchimp = Hominid::API.new(FLP["MAILCHIMP_API_KEY"])
        result    = mailchimp.list_subscribe(list_id, self.email, info, 'html', false, true, false, false) if Rails.env.production? || test == true
        Rails.logger.info("MAILCHIMP SUBSCRIBE: result #{result.inspect} for #{self.email}")
      rescue Hominid::APIError => e
        Rails.logger.info("MAILCHIMP ERROR: #{e.message}")
      end
    end

    def remove_user_from_mailchimp_lists
      remove_user_from_mailchimp(FLP["MAILCHIMP_NEWSLETTER_LIST_ID"])
      remove_user_from_mailchimp(FLP["MAILCHIMP_OFFERRING_LIST_ID"])
    end

    def remove_user_from_mailchimp(list_id)
      begin
        mailchimp = Hominid::API.new(FLP["MAILCHIMP_API_KEY"])
        result = mailchimp.list_unsubscribe(list_id, self.email, true, false, true) if Rails.env.production?
        Rails.logger.info("MAILCHIMP UNSUBSCRIBE: result #{result.inspect} for #{self.email}")
      rescue Hominid::APIError => e
        Rails.logger.info("MAILCHIMP ERROR: #{e.message}")
      end
    end
    
      

end
