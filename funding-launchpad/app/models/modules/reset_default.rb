module ResetDefault
  def reset_defaults
    if self.default == true
      self.offering.photos.each do |photo|
        photo.update_attributes(default: false) if photo.default?
      end
      self.offering.videos.each do |video|
        video.update_attributes(default: false) if video.default?
      end
    end
  end
end
