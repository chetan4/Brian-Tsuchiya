class BidIncrement < ActiveRecord::Base

  validates_numericality_of  :min_price, greater_than_or_equal_to: 0.00
  validates_numericality_of  :max_price, greater_than: Proc.new { |bid_increment| bid_increment.min_price },
                                        :unless => Proc.new { |bid_increment| bid_increment.max_price.nil? }
  validates_numericality_of  :bid_increment, greater_than: 0.00

  attr_accessible :bid_increment, :max_price, :min_price
end
