class AuctionPreference < ActiveRecord::Base
  belongs_to :user
  belongs_to :auction

  attr_accessible :auction_id, 
                  :user_id,
                  :auto_bid, 
                  :max_bid_notification,
                  :current_bid,
                  :max_bid

  def self.update_bids(new_auction_preference)
    # When somebody creates a new bid we need to account for existing bidders having set 'auto_bid' true.
    # In this event, all other bids have to be adjusted.

    # Find the highest existing bid.  This is the only one we care about.
    current_high_bidder_preference = AuctionPreference.find_by_auction_id(new_auction_preference.auction_id)

    # We know we are higher than the winning bid, but we may not be higher than the max bid. So...
    # If our new bid is higher than the current high bidder's max bid:
    if new_auction_preference.current_bid > current_high_bidder_preference.max_bid
      if current_high_bidder_preference.auto_bid?
        current_high_bidder_preference.create_bid(current_high_bidder_preference.max_bid)
        current_high_bidder_preference.notify_of_max_bid_loss
      end
    else
      # We're below the leader's max bid, but are we above his current bid?
      if new_auction_preference.current_bid > current_high_bidder_preference.current_bid

    # => 4. Increase the current high bidder's current bid to his max bid.
    # => 5. We're done.
    # 3. If our new bid is less than or equal to the high bidder's max bid:
    # => 4. If our max bid is higher than the current high bidder's max bid:
    # =>   5. Set current high bidder's current bid to his max bid.
    # =>   6. Set our new current bid to the current high bidder's max bid + appropriate bid increment.
    # =>   7. We're done.
    # => 4. If our max bid is not higher than the current high bidder's max bid:
    # =>   5. Set our current bid to our max bid.
    # =>   6. Set current high bidder's current bid to min ( our current bid + appropriate bid increment or current high bidder's max bid)
    # =>   7. We're done.
    # We assume all other bids were lower whether they are auto-bidding or not.

  end

  def auto_bid?
    auto_bid
  end

  def create_bid(bid_amount)
    Bid.new.create_bid({user_id: user_id,
                    auction_id: auction_id,
                    bid: bid_amount})
    update_attribute(current_bid: bid_amount)
  end

  def notify_of_max_bid_loss
    if max_bid_notification
      # Send this guy an email telling him somebody just blew through his max bid.
    end
  end

end
