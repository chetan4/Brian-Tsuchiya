class DefaultFollowingOption < ActiveRecord::Base
  belongs_to :user
  
  attr_accessible :new_offering_update,
                  :new_offering_qa,
                  :investment_offered,
                  :investment_accepted,
                  :investment_declined,
                  :investment_funds_sent,
                  :investment_escrowed,
                  :investment_invested,
                  :investment_returned,
                  :offering_funded,
                  :offering_expired,
                  :offering_content_changed
end
