class Document < ActiveRecord::Base
  belongs_to :offering
  has_many :document_viewers
  has_many :viewers, :class_name => "User", :through => :document_viewers
  mount_uploader :document, DocumentUploader

  validates_presence_of :document, :description
end
