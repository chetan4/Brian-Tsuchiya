class Following < ActiveRecord::Base
  belongs_to :user
  belongs_to :offering

  validates_presence_of :user_id, :offering_id

  def user_accepts_new_offering_qa?
    !user.following_options.find_by_offering_id(offering.id).nil? &&
      user.following_options.find_by_offering_id(offering.id).new_offering_qa
  end

  def user_accepts_updates?
    !user.following_options.find_by_offering_id(offering.id).nil? &&
      user.following_options.find_by_offering_id(offering.id).new_offering_update
  end
end
