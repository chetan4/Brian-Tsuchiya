class Bid < ActiveRecord::Base
  belongs_to :auction
  belongs_to :user

  validates_presence_of     :amount
  validates_numericality_of :amount, greater_than_or_equal_to: 0.00

  attr_accessible :amount, :auction_id, :user_id

  def create_bid(args)
    @user_id = args[:user_id]
    @auction_id = args[:auction_id]
    @bid = args[:bid]
    save
  end
end
