class InvestmentTotal < ActiveRecord::Base
  attr_accessible :amount, :show

  validates :amount, :presence => true,
                    :numericality => {
                      :greater_than_or_equal_to => 0
                    }
end
