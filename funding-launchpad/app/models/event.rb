class Event < ActiveRecord::Base
  belongs_to :offering
  attr_accessible :description,
                  :title,
                  :time_string,
                  :date_string,
                  :formatted_date_string,
                  :end_time_string

  validates_presence_of :formatted_date_string, :time_string, :title, :description
  validate :custom_validate

  scope :upcoming, where('start_date >= ? ', Date.today).order('start_date Asc')

  def formatted_date_string=(date)
    if date.blank?
      @date_nil = true
      return nil
    end
    self.start_date = self.end_date = formatted_date(date)
  end

  def formatted_date_string
    return @date_val if @date_invalid
    return nil if start_date.nil? or @date_nil
    start_date.to_s(:date_only)
  end

  def time_string
    return @time_val if @time_invalid
    return nil if @time_nil
    start_date.to_s(:time_only_leading_zero) if start_date
  end

  def time_string=(t)
    #if time is blank we want to return nil and set variable so the getter
    #knows to return nil on not use the startdate value.
    if t.blank?
      @time_nil = true
      return nil
    end

    t = Time.parse(t)
    original = start_date || Time.now
    self.start_date = DateTime.new(original.year, original.month, original.day, t.hour, t.min, t.sec)
  rescue ArgumentError
    @time_invalid = true
    @time_val = t
  end

  def end_time_string
    return @end_time_val if @end_time_invalid
    end_date.to_s(:time_only_leading_zero) if end_date
  end

  def end_time_string=(t)
    if t.blank?
      self.end_date = nil
    else
      t = Time.parse(t)
      original = start_date || Time.now #we want same day as start day
      self.end_date = DateTime.new(original.year, original.month, original.day, t.hour, t.min, t.sec)
    end
  rescue ArgumentError
    @end_time_invalid = true
    @end_time_val = t
  end

  def custom_validate
    errors.add(:formatted_date_string, "is invalid") if @date_invalid
    errors.add(:time_string, "is invalid") if @time_invalid
    errors.add(:end_time_string, "is invalid") if @end_time_invalid
  end

private
  def formatted_date(amount)
    return Date.strptime(amount, '%m/%d/%Y') if /\d{2}\/\d{2}\/\d{4}/.match(amount)
    return Date.strptime(amount, '%Y/%m/%d') if /\d{4}\/\d{2}\/\d{2}/.match(amount)
    @date_invalid = true
    @date_val = amount
    nil
  end
end
