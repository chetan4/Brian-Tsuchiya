class Press < ActiveRecord::Base
  attr_accessible :description, :link, :published_date, :publisher_logo, :title, :publish, :feature

  mount_uploader :publisher_logo, PressLogoUploader

  scope  :published, where("publish = ?", true)
  scope  :featured, where("feature = ?", true)

  default_scope :order => 'published_date DESC'
end
