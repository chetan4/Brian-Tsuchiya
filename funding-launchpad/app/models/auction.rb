class Auction < ActiveRecord::Base
  belongs_to :offering
  has_many :bids
  has_many :auction_preferences

  validates_presence_of     :title
  validates_numericality_of :price, greater_than_or_equal_to: 0.00
  validates_numericality_of :base_cost, greater_than_or_equal_to: 0.00, :unless => Proc.new { |auction| auction.base_cost.nil? }
  validates_numericality_of :ancillary_cost, greater_than_or_equal_to: 0.00, :unless => Proc.new { |auction| auction.ancillary_cost.nil? }

  attr_accessible :ancillary_cost, 
                  :ancillary_cost_description, 
                  :auction_end_time, 
                  :base_cost, 
                  :base_cost_description, 
                  :bid_increment, 
                  :delivery_method, 
                  :description, 
                  :enabled, 
                  :note, 
                  :offering_id, 
                  :price, 
                  :primary_photo_id, 
                  :title

  def has_bids?
    if bids.nil? or bids.count == 0
      false
    else
      true
    end
  end

  def current_bid
    bids.last
  end

end
