module UsersHelper
  def confirm_user(user)
    if user.confirmed?
      user.confirmed_at.to_s
    elsif !user.confirmation_sent_at.nil?
      "confirmation email sent"
    else
      link_to "confirm", confirm_user_path(user), :method => :put
    end
  end
end
