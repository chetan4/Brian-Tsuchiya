module ApplicationHelper
  include SessionsHelper
  #used to convert keys to proper bootstrap classes
  def flash_class(level)
    case level
    when :info then "alert alert-info"
    when :notice then "alert alert-success"
    when :success then "alert alert-success"
    when :error then "alert alert-error"
    when :warning then "alert alert-error"
    when :alert then "alert alert-error"
    end
  end

  def title
    base_title = "Funding Launchpad"
    if @title.nil?
      base_title
    else
      "#{base_title} | #{@title}"
    end
  end

  def static_page_menu(page_name)
    @page = Page.find_by_name(page_name)
    if @page
      @page.title
    else
      page_name
    end
  end

  def select_tag_for_filter(model, nvpairs, params)
    options = {:query => params[:query]}
    _url = url_for(eval("#{model}_url(options)"))
    _html = %{<select name="show" id="show"}
    _html << %{onchange="window.location='#{_url}' + '?show=' + this.value">}
    nvpairs.each do |pair|
      _html << %{<option value="#{pair[:scope]}"}
      if params[:show] == pair[:scope] || ((params[:show].nil? || params[:show].empty?) && pair[:scope] == "all")
        _html << %{ selected="selected"}
      end
      _html << %{>#{pair[:label]}}
      _html << %{</option>}
    end
    _html << %{</select>}
    _html << %{<label for="show"> Offerings</label>}
    _html.html_safe
  end

  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end

  def link_to_submit(text)
    link_to_function text, "$(this).closest('form').submit()"
  end

  def can_edit_this_offering?(offering)
    signed_in? and offering and (current_user.has_role?(:offering_admin, offering) or current_user.has_role?(:vim_admin))
  end

  def comma_delimited_number(number)
    number.to_s.gsub(/(\d)(?=\d{3}+(\.\d*)?$)/, '\1,')
  end

  def nav_link(link_text, options={}, html_options={})
    class_name = current_page?(options) ? 'active' : ''

    content_tag(:li, :class => class_name) do
        link_to link_text, options, html_options
    end
  end

  def filter_link(link_text, options={}, html_options={})
    if params[:filter]
      class_name = options.include?(params[:filter]) ? 'active' : ''
    else
      class_name = !options.include?('filter') ? 'active' : ''
    end

    content_tag(:li, :class => class_name) do
        link_to link_text, options, html_options
    end
  end

  def icon_tag(icon_text, icon_class, options={})
    opts = options.merge({:class => icon_class})

    if options[:reverse_text]
      icon_text + content_tag(:i, "", opts)
    else
      content_tag(:i, "", opts) + icon_text
    end
  end

  def info_tag(options={})
    icon_tag "", "icon-info-sign popover-icon", options
  end

  def form_actions(submit_text="Submit", cancel_text="Cancel", options={})

    btn_submit_opts = { :class => "btn btn-primary"}
    btn_submit_opts = btn_submit_opts.merge(options[:btn_submit]) if options[:btn_submit]
    cancel_path = options[:btn_cancel_path].blank? ? :back : options[:btn_cancel_path]

    content_tag(:hr) +
    content_tag(:div, :class => "form-actions-custom") do
      button_to(submit_text, "", btn_submit_opts) +
      link_to(cancel_text, cancel_path, :class => "btn btn-cancel")

    end

  end

  def accredited_popover(text="", reverse_text=false)
    icon_tag  text,
              "icon-info-sign accredited-popover",
              :data => {title: 'Accredited Investor', content: "#{render 'shared/definitions/accredited'}"},
              :reverse_text => reverse_text
  end

  def display_can_invest(offering)
    if signed_in?
      unless offering.contributable? # Should be no restrictions if the offering accepts contributions
        if offering.investable? # Should only be restrictions if the offering accepts investments
          if offering.ppm?
            flash.now[:'can-invest-error'] = "You must be an accredited investor to invest in this offering." if !current_user.person.accredited?
          elsif current_user && !current_user.person.has_residence?
            flash.now[:'no-residence-info'] = "We need your address to determine if you are eligible to invest in this offering."
          elsif current_user && !current_user.can_invest?(offering)
            flash.now[:'can-invest-error'] = "Sorry, but you are unable to to invest in this offering."
          end
        end
      end
    end
  end

  def action_btns(parent, child)
    parent_name = parent.class.name.underscore
    child_name = child.class.name.underscore

    button_to('Delete', send("#{parent_name}_#{child_name}_path", parent, child),
      :data => { :confirm => 'Are you sure?'},
      :method => :delete,
      :class => "btn btn-danger",
      :remote => true,
      :disable_with => 'deleting...',
      :form_class => "ib") + "\n" +
    link_to('Edit', send("edit_#{parent_name}_#{child_name}_path",parent, child), :class => "btn btn-primary")
  end

  def default_action_btns(thing)
    thing_name = thing.class.name.underscore
    button_to('Delete', send("#{thing_name}_path", thing),
      :data => { :confirm => 'Are you sure?'},
      :method => :delete,
      :class => "btn btn-danger",
      :remote => true,
      :disable_with => 'deleting...',
      :form_class => "ib") + "\n" +
    link_to('Edit', send("edit_#{thing_name}_path", thing), :class => "btn btn-primary")
  end

  def return_add_btns(parent, child_class_name)
    parent_name = parent.class.name.underscore
    child_name = child_class_name.underscore

    link_to('Return to %s' % parent_name.humanize.titleize, send("admin_#{parent_name}_path", parent), class: "btn") + "\n" +
    link_to('Add a new %s' % child_name.humanize.titleize, send("new_#{parent_name}_#{child_name}_path",parent), class: "btn btn-primary")
  end

  def dates_to_human(start_date, end_date=nil)
    end_date.blank? ? "#{start_date.to_s(:time_only_lower)}" : "#{start_date.to_s(:time_no_meridian)} &ndash; #{end_date.to_s(:time_only_lower)}".html_safe
  end
  
  # Are we in contributions only mode?
  def contributions_only?
    CONTRIBUTIONS_ONLY
  end

  def is_pcp?
    PCP_CONFIG.is_pcp?
  end

  def is_pcp_admin?(user=current_user)
    (pcp_admin?(user) and PCP_CONFIG.admin_email == user.email) or vim_admin?(user)
  end

  #sometimes we only want to allow things if its
  #not a pcp site. If it is a pcp, then the user
  #needs to be a pcp admin.
  def not_pcp_or_pcp_admin?(user=current_user)
    !is_pcp? or is_pcp_admin?(user)
  end

  def logo
    logo = PCP_CONFIG.logo || "fl-logo-small.png"
    logo_alt = PCP_CONFIG.logo_alt || "Funding Launchpad"

    link_to image_tag(logo, :alt => logo_alt), root_path, {:id => 'fl-logo', :class => 'brand'}
  end

  def pcp_twitter
    icon_tag link_to("Twitter", "https://twitter.com/#{PCP_CONFIG.twitter_name}", target: "_blank"), "icon-twitter"
  end

  def pcp_blog
    icon_tag link_to("Blog", PCP_CONFIG.blog_url, target: "_blank"), "icon-heart"
  end

  def pcp_email
    icon_tag mail_to(PCP_CONFIG.email, "Email", :encode => "javascript"), "icon-envelope"
  end

  def pcp_facebook
    icon_tag link_to("Facebook", PCP_CONFIG.facebook_url, target: "_blank"), "icon-facebook"
  end

  def set_analytics_tracker
    pcp_tracker = PCP_CONFIG.ga_tracker
    GA.tracker = pcp_tracker unless pcp_tracker.blank?
  end

  def default_og_tags
    logo = PCP_CONFIG.logo || "square_color_logo.png"
    hash = {:'og:type' => 'website',
            :'og:url' => root_url,
            :'og:title' => PCP_CONFIG.name || 'Funding Launchpad',
            :'og:description' => PCP_CONFIG.description || 'Rewards crowdfunding to raise money to launch or grow your restaurant, retail, or local services business. Contact us to raise money today.',
            :'og:keywords' => 'crowdfunding, Main Street, restaurant, retail, boutique, rewards, gym, yoga, fitness, cleaners, crowd funding, capital, money',
            :'og:image' => root_url + path_to_image(logo)}

    og_tags(hash)
  end

  def og_tags(hash)
    tags = hash.map do |key, val|
      %(<meta property="#{key}" content="#{Rack::Utils.escape_html(strip_tags(val))}"/>)
    end
    tags = tags.join("\n").html_safe
  end

end
