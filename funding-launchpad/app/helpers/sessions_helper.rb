module SessionsHelper

  def have_projects?
    if user_signed_in?
      if Project.find_all_by_user_id(current_user.id).count > 0
        return true
      end
    end
    return false
  end

  def have_investments?
    if user_signed_in?
      if Investment.find_all_by_user_id(current_user.id).count > 0
        return true
      end
    end
    return false
  end

  # Access methods

  def valid_user?(user, user_type)
    if user_signed_in?
      user.access & user_type > 0
    else
      false
    end
  end

  def vim_admin?(user=current_user)
    user.has_role? :vim_admin
  end

  def pcp_admin?(user=current_user)
    user.has_role? :pcp_admin
  end

  def editor?(user=current_user)
    valid_user?(user, EDITOR_USER)
  end

  def new_project_count
    Project.unapproved.count
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  private

  def signed_in_status
    if user_signed_in?
      link_to "#{current_user_full_name}", profile_person_path(current_user.person)
    end
  end

  def current_user_full_name
    if current_user.person
      "#{current_user.person.first_name} #{current_user.person.last_name}"
    else
      "#{current_user.email}"
    end
  end

  def you_own_one(reward)
    if UserReward.find_by_user_id_and_reward_id(current_user.id, reward.id)
      "<div class=\"span5 muted\"> <i class=\"icon-hand-right\"></i>You own one!</div>".html_safe
    end
  end
end
