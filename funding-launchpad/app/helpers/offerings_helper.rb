module OfferingsHelper

  def basic_section_complete_link
    if basic_section_complete_amt == "0"
      "Start"
    else
      "Update"
    end
  end

  def basic_section_complete_amt
    count = 0
    if @offering.name
      count+= 1
    end
    if @offering.minimum_goal
      count+= 1
    end
    if @offering.maximum_goal
      count+= 1
    end
    if @offering.desired_start_date
      count+= 1
    end
    if @offering.expiration_date
      count+= 1
    end
    if @offering.short_description
      count+= 1
    end
    if @offering.logo
      count+= 1
    end
    sprintf("%1.0f", count / 7.0 * 100)
  end

  def bank_section_complete_link
    if bank_section_complete_amt == "0"
      "Start"
    else
      "Update"
    end
  end

  def bank_section_complete_amt
    if @offering.access_token.blank?
      "0"
    else
      "100"
    end
  end

  def description_section_complete_link
    if description_section_complete_amt == "0"
      "Start"
    else
      "Update"
    end
  end

  def description_section_complete_amt
    if @offering.long_description
      sprintf('%0.1f',[@offering.long_description.length/1000.0*100, 100].min)
    else
      "0"
    end
  end

  def people_section_complete_link
    if people_section_complete_amt > 0
      "Update"
    else
      "Start"
    end
  end

  def people_section_complete_amt
    if @offering.people and @offering.people.count > 0
      100
    else
      0
    end
  end

  def photos_section_complete_link
    if photos_section_complete_amt > 0
      "Update"
    else
      "Start"
    end
  end

  def photos_section_complete_amt
    if @offering.photos and @offering.photos.count > 0
      100
    else
      0
    end
  end

  def videos_section_complete_link
    if videos_section_complete_amt > 0
      "Update"
    else
      "Start"
    end
  end

  def videos_section_complete_amt
    if @offering.videos and @offering.videos.count > 0
      100
    else
      0
    end
  end

  def documents_section_complete_link
    if documents_section_complete_amt > 0
      "Update"
    else
      "Start"
    end
  end

  def documents_section_complete_amt
    if @offering.documents and @offering.documents.count > 0
      100
    else
      0
    end
  end

  def links_section_complete_link
    if links_section_complete_amt > 0
      "Update"
    else
      "Start"
    end
  end

  def links_section_complete_amt
    if @offering.links and @offering.links.count > 0
      100
    else
      0
    end
  end

  def rewards_section_complete_link
    if rewards_section_complete_amt > 0
      "Update"
    else
      "Start"
    end
  end

  def rewards_section_complete_amt
    if @offering.rewards and @offering.rewards.count > 0
      100
    else
      0
    end
  end

  def auctions_section_complete_link
    if auctions_section_complete_amt > 0
      "Update"
    else
      "Start"
    end
  end

  def auctions_section_complete_amt
    if @offering.auctions and @offering.auctions.count > 0
      100
    else
      0
    end
  end

  def instructions_section_complete_link
    if instructions_section_complete_amt == "0"
      "Start"
    else
      "Update"
    end
  end

  def instructions_section_complete_amt
    if @offering.instructions
      sprintf('%0.1f',[@offering.instructions.length/1000.0*100,100].min)
    else
      "0"
    end
  end

  def updates_section_complete_link
    if updates_section_complete_amt == "0"
      "Start"
    else
      "Update"
    end
  end

  def updates_section_complete_amt
    if @offering.updates.count == 0
      "0"
    else
      "100"
    end
  end

  def can_invest?
    current_user && current_user.can_invest?(@offering)
  end

  def time_diff(from_time, to_time)
    %w(year month day hour minute second).each do |interval|
      distance_in_seconds = (to_time.to_i - from_time.to_i).round(1)
      delta = (distance_in_seconds / 1.send(interval)).floor
      delta -= 1 if from_time + delta.send(interval) > to_time
      from_time += delta.send(interval)

      if delta > 0
        return delta, interval
      end

    end
  end

  def equity_diff
    @offering.max_equity.to_i - @offering.min_equity.to_i
  end

  def offering_og_tags
    hash = {:'og:type' => 'website',
            :'og:url' => offering_url(@offering),
            :'og:title' => @offering.name,
            :'og:description' => @offering.short_description,
            :'og:image' => @offering.logo_url(:small)}

    og_tags(hash)
  end

  def funded_label(funded_percent, invested)
    funded = "#{number_to_percentage(funded_percent, :precision => 1, :strip_insignificant_zeros => true)} Funded"
    invested > 0 ?  funded + " (#{number_to_currency(invested, :precision => 0)})" : funded
  end

  def contributed_label(contributed_percent, contributed)
    if contributed > 0 && contributed_percent < 1
      "#{number_to_currency(contributed, :precision => 0)} Contributed (<1%)"
    else
      funded = "#{number_to_percentage(contributed_percent, :precision => 1, :strip_insignificant_zeros => true)} Contributed"
      contributed > 0 ?  funded + " (#{number_to_currency(contributed, :precision => 0)})" : funded
    end
  end

end
