class InvestmentObserver < ActiveRecord::Observer
  def after_save(investment)
    InvestmentStatus.create(investment_id: investment.id, status: investment.status)
  end

  def after_create(investment)
    InvestmentMailer.you_invested(investment).deliver
    InvestmentMailer.new_investment(investment).deliver
  end
end
