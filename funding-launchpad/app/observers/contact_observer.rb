class ContactObserver < ActiveRecord::Observer
  def after_save(contact)
    ContactMailer.confirmation(contact).deliver
    ContactMailer.notification(contact).deliver
  end
end
