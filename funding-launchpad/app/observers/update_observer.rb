class UpdateObserver < ActiveRecord::Observer
  def after_save(update)
    @offering   = update.offering
    @followers  = Following.find_all_by_offering_id(@offering.id)
    @followers.each do |follower|
      if follower.user_accepts_updates?
        FollowerMailer.offering_update(@offering, follower.user, update).deliver
      end
    end
  end

end
