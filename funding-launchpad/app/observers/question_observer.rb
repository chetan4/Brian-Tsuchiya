class QuestionObserver < ActiveRecord::Observer
  def after_save(question)
    QuestionMailer.question_update(question).deliver
    FollowerMailer.offering_qa(question).deliver
  end
end
