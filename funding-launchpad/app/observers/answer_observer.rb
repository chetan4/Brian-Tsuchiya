class AnswerObserver < ActiveRecord::Observer
  def after_save(answer)
    AnswerMailer.answer_update(answer).deliver
    FollowerMailer.offering_qa(answer).deliver
  end
end
