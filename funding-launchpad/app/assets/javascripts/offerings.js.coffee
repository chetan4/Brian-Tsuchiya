((Fl, $, undefined_) ->
  Fl.pages = Fl.pages or {}
  Fl.pages.offerings = Fl.pages.offerings or {}

  Fl.pages.offerings.init = ->
    $("input[name='offering[debt]']").on "change", ->
      Fl.pages.offerings.toggleFields()
    Fl.pages.offerings.toggleFields()

  Fl.pages.offerings.toggleFields = ->
    if $("input[name='offering[debt]']:checked").val() == "true"
      $(".debt-fields").show()
      $(".equity-fields").hide()
    else
      $(".debt-fields").hide()
      $(".equity-fields").show()

    Fl.pages.offerings.moveMinInvestment()
    Fl.pages.offerings.clearFields()

  #move the min investment field if its not visible
  Fl.pages.offerings.moveMinInvestment = ->
    if !$(".min-investment-holder:visible").has('input').length
      $(".min-investment-holder:hidden .control-group").prependTo($(".min-investment-holder:visible"))

  Fl.pages.offerings.clearFields = ->
    #$(".equity-fields:hidden, .debt-fields:hidden").each


) window.Fl = window.Fl or {}, jQuery

$ ->
  Fl.pages.offerings.init()