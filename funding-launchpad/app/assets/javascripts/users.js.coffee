


((Fl, $, undefined_) ->
  Fl.pages = Fl.pages or {}
  Fl.pages.users = Fl.pages.users or {}

  Fl.pages.users.init = ->
    Fl.pages.users.focus()
    Fl.pages.users.toggleResidence()
    Fl.pages.users.loginCallback()
    Fl.pages.users.popoverResidence()

    # validations show on both forms, so this hides it on signin fields
    # and changes focus
    if document.location.pathname.split("/")[1] == "signup"
      Fl.pages.users.removeLoginError()
      $('#signup-tab').find('.well.social-logins').focusin()

    if document.location.pathname.split("/")[1] == "login"
      Fl.pages.users.showLoginTab()
      $('#login-tab').find('.well.social-logins').focusin()

    $('#login-tabs a[href="#login-tab"], #login-tabs a[href="#signup-tab"] ').on "shown", (e)->
      $(e.target.hash).find('.well.social-logins').focusin()

  Fl.pages.users.focus = ->
    $(".well.signup, .well.login, .well.social-logins").on "focusin", ->
      $(".well").not(this).removeClass("focus")
      $(this).addClass('focus')

  Fl.pages.users.toggleResidence = ->
    $("#residence input").on "change", ->
      if $(this).val() is "true"
        $("#wrap-country").hide()
        $("#wrap-state").show()
      else
        $("#wrap-country").show()
        $("#wrap-state").hide()

    selected = $("#residence input:checked").val()
    if selected is "true"
      $("#wrap-country").hide()
      $("#wrap-state").show()
    else if selected is "false"
      $("#wrap-country").show()
      $("#wrap-state").hide()

  Fl.pages.users.loginCallback = ->
    #Redirect on success and show error on failure
    $("form#signin").on "ajax:success", (e, data, status, xhr) ->
      if data.success
        window.location.href = data.redirect
      else
        $(".signin_error").html(data.errors.join()).addClass "alert alert-error"
        #user signed up for beta, and hasn't created password yet so copy the 
        #password over to the account creation form
        if data.incomplete
          $("#user_email").focus().val $("#signin_email").val()
          $("#signin_email").val ""
          $('#login-tabs a[href="#signup-tab"]').tab("show")
          $(".signin_error").prependTo("#new_user .well")

  Fl.pages.users.removeLoginError =  ->
    $("#signin_email").val('')
    $("form#signin").find('.control-group').removeClass('error')

  #Used to add email address after submitting forgotten password or confirmation form
  Fl.pages.users.addLoginEmail = (email) ->
    $(".well.login #signin_email").val(email).focus()

  Fl.pages.users.showLoginTab = ->
    $('#login-tabs a[href="#login-tab"]').tab("show")

  Fl.pages.users.popoverResidence = ->
    $(".residence").popover
      trigger: 'hover'
      html: true
      content: ->
        $("#residence_popover_content").html()


) window.Fl = window.Fl or {}, jQuery

$ ->
  Fl.pages.users.init()


