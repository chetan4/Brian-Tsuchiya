
((Fl, $, undefined_) ->
  Fl.pages = Fl.pages or {}
  Fl.pages.settings = Fl.pages.settings or {}

  Fl.pages.settings.init = ->
    hash = document.location.hash;

    if hash
      Fl.utils.clickTab(hash)
    else
      $('#settings-tabs a:first').click()

    $("#settings-tabs-content").on "click", ".btn-cancel", (e) ->
      Fl.utils.clickTab(e.target.hash)

    Fl.pages.settings.tabShow()
    Fl.pages.settings.toggleStateAndCountry()


  Fl.pages.settings.tabShow = ->
    $('#settings-tabs').on "show", (e) ->
      previous = $(e.relatedTarget).data("target")

      #remove previous tab content to keep us from seeing a "blinking" effect
      $(previous).empty()

      #change url to match tab shown
      window.location.hash = $(e.target).data("target")

  Fl.pages.settings.toggleStateAndCountry = ->
    $('#settings-tabs-content').on "change", "#wrap-state select", (e) ->
      $('#wrap-country select').val('US')

    $('#settings-tabs-content').on "change", "#wrap-country select", (e) ->
      $('#wrap-state select').val('')

) window.Fl = window.Fl or {}, jQuery



$ ->
  Fl.pages.settings.init()
