
((Fl, $, undefined_) ->
  Fl.utils = Fl.utils or {}

  Fl.utils.typewatch = (->
    timer = 0
    (callback, ms) ->
      clearTimeout timer
      timer = setTimeout(callback, ms)
  )()

  Fl.utils.replaceFlash = (partial) ->
    $("#flash").replaceWith partial
    window.setTimeout (->
      $(".alert-slide").slideUp()
    ), 5000

  Fl.utils.deleteRow = (id, partial) ->
    $("#" + id).fadeOut "slow", ->
      $("#flash").replaceWith partial

    window.setTimeout (->
      $(".alert-slide").slideUp()
    ), 5000

  Fl.utils.clickTab = (target) ->
    $('[data-target=' + target + ']').click()

  Fl.utils.removeHash = (->
    history.pushState("", document.title, window.location.pathname + window.location.search)
  )


) window.Fl = window.Fl or {}, jQuery
