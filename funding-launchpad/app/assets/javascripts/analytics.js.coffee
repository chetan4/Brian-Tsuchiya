
$ ->
  $(".btn").on "click", ->
    href = $(this).attr("href")
    if $(this).data("ga")
      _gaq.push [ "_trackEvent", "Button", "Click", $(this).data("ga") ]
    else if href and href != "#"
      _gaq.push [ "_trackEvent", "Button", "Click", $(this).attr("href") ]


  $('#investor-invite').on "click", ->
    _gaq.push(['_trackPageview', '/home/beta-signup']);

  $('#request-access-submit').on "click", ->
    _gaq.push(['_trackPageview', '/home/beta-signup-submit']);

  $('#request-access-cancel').on "click", ->
    _gaq.push(['_trackPageview', '/home/beta-signup-cancel']);

  $('#contact-submit').on "click", ->
    _gaq.push(['_trackPageview', '/contact/submit']);