# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/


load = (request) ->
  $(".link_html").html request
  $(".link_html").fitVids();
preview = (value) ->
  url = "/offerings/" + $("#video_offering_id").val() + "/videos/preview?callback?"
  $.getJSON url, value, (data) ->
    load data
previewComment = ->
  preview video: $("#video_link").val()

$("#video_examples a").live "click", ->
  $("#video_link").focus()
  $("#video_link").val $(this).attr("href")
  previewComment()
  false

$ ->
  if $("#video_link").length > 0
    $("#video_link").focus()
    previewComment()
  
    $("#video_link").keyup ->
      Fl.utils.typewatch (->
        previewComment()
      ), 750
  

