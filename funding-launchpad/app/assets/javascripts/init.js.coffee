((Fl, $, undefined_) ->
  Fl.init = Fl.init or {}

  Fl.init.all = (->
    $('.timepicker-me').timepicker
      showInputs: false
      defaultTime: 'value'
    $('.datepicker-me').datepicker()
    $('.carousel').carousel()
    $('.tip').tooltip()
    $('.auto-ellipsis').ellipsis()

    $('.wysi').wysihtml5()
    $('input, textarea').placeholder()

    Fl.init.popovers()
    Fl.init.stateTokens()
    Fl.init.requiredModal()
    Fl.init.aboutTabs()
    Fl.init.validateImageSize()
    Fl.init.betaSignup()
    Fl.init.requiredModal()
    Fl.init.accreditedModal()
    Fl.init.masonry()
    Fl.init.accordionCarets()
    Fl.init.pjax()
    Fl.init.changeStatus()
    Fl.init.fitVids()
    Fl.init.resizeModal()

    #open linked question
    $("#faq_jobsfit").on "click", ->
      $("#jobsfit_accordion").click()

    $("#new-account-success").modal()

    $(".alert-slide").slideDown()
    window.setTimeout (->
      $("#flash .alert-slide").slideUp()
    ), 7500
  )

  Fl.init.popovers = (->
    $('.popover-icon').popover 
      trigger: 'hover'
      html: true
    $('.accredited-popover').popover
      trigger: 'click'
      template: '<div class="popover accredited"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'
      html: true

    $(".no-invest-reason").popover
      trigger: 'hover'
      html: true
      placement: "bottom"
      content: ->
        $("#no_invest_reason").html()
      )

  Fl.init.stateTokens = (->
    $(".state-tokens").tokenInput "/filings.json",
      crossDomain: false
      theme: "bootstrap"
      preventDuplicates: true
      hintText: "Type a state name"
      prePopulate: $(this).data("pre")
      tokenFormatter: (item) ->
        "<li class=\"btn\"><p>" + item.name + "</p></li>"
  )

  Fl.init.requiredModal = (->
    $('#required_modal').modal()

    $('#required_modal').on('hide', ->
      location.reload()
    )
  )

  Fl.init.accreditedModal = (->
    $('.not-accredited').click(->
      $('#accredited-modal').modal("show")
    )
  )

  Fl.init.aboutTabs = (->
    hash = document.location.hash
    prefix = "tab_"

    if hash
      hash = hash.replace(prefix, "")
      selector = "a[href=" + hash + "]"
      selector = "a[data-target=" + hash + "]" if hash is "#press"
      $("#about-tabs " + selector).click()

    $("#about-tabs a").on "shown", (e) ->
      window.location.hash = if e.target.hash is "" or hash is "#press" then "#tab_press" else e.target.hash.replace("#", "#" + prefix)

  )

  Fl.init.validateImageSize = ->
    $('#photo_image').on 'change', ->
      if @files[0].size >= 5242880
        alert "This image is larger than the max 5Mb, please reduce its file size."

  Fl.init.betaSignup = (->

    if document.location.hash == '#beta-signup'
      $("a[href='#investor-invite']").click()
      Fl.utils.removeHash()
  )

  Fl.init.masonry = ->
    $container = $(".thumbnails.use-masonry")
    $container.imagesLoaded ->
      $container.masonry
        itemSelector: "li"
        columnWidth: 1

  Fl.init.accordionCarets = ->
    $(".accordion").on "show hide", (e) ->
      $(e.target).siblings(".accordion-heading").find(".accordion-toggle i").toggleClass "icon-caret-down icon-caret-right"

  Fl.init.pjax = ->
    $('.pjax-me').pjax('[data-pjax-container]', {fragment: "[data-pjax-container]"})
    $("[data-pjax-container]").on "pjax:success", ->
      stButtons.locateElements()
      Fl.init.fitVids()
      Fl.init.accordionCarets()
      Fl.init.masonry()

  Fl.init.changeStatus = ->
    $('#investment_status').on 'change', ->
      if $(this).val() == "Investor Funds Received by Escrow Agent"
        $('#offered_amount').show()
      else
        $('#offered_amount').hide()

      $("#investment_accepted_amount").val("")

  Fl.init.fitVids = ->
    # resize videos to fit container
    $(".link_html").fitVids()

  Fl.init.resizeModal = ->
    $("#login.modal").css
      "margin-left": ->
        -($(this).width() / 2)
      "margin-top": ->
        -($(this).height() / 2)



) window.Fl = window.Fl or {}, jQuery

$ ->
  Fl.init.all();


