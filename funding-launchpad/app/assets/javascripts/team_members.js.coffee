$(document).ready ->
  $(".team_members").disableSelection
  $(".team_members").sortable
    axis: "y"
    dropOnEmpty: false
    handle: ".handle"
    cursor: "move"
    items: "li"
    opacity: 0.4
    scroll: true
    update: ->
      $.ajax
        type: "put"
        data: $(".team_members").sortable("serialize")
        dataType: "script"
        complete: (request) ->
          $("#team_members").effect "highlight"

        url: $(".team_members").data('update_url')
