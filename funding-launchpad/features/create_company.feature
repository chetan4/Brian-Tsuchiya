Feature: Create Company

  Scenario: No company exists for current user
    Given a user does not have a company
    When he enters company information
    Then a company record is created
    And the user is associated with that company
    