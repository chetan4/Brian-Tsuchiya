namespace :users do
  desc "Adds person record to each user that doesn't have one"
  task :add_person => :environment do
    count = 0

    User.find_all_by_person_id(nil).each do |user|
      person = Person.new
      person.dont_validate_residence = true
      person.addresses.build({content_type: "Residence"})
      user.person = person
      user.save
      count += 1
    end
    puts "#{count} users were updated with a person record"
  end

  desc "Adds address record to each person that doesn't have one"
  task :add_address => :environment do
    count = 0
    Person.all.each do |person|
      if person.addresses.empty?
        if not person.user.nil?
          p person.user.email
          person.addresses.build({content_type: "Residence"})
          person.dont_validate_residence = true
          person.save
        end
        count += 1
      end
    end
    puts "#{count} people were given an address"
  end

end
