namespace :carrierwave do
  desc "reprocess all documents after carrierwave changes"
  task :reprocess => :environment do
    Document.all.each do |doc|
      begin
        doc.document.cache_stored_file!
        doc.document.retrieve_from_cache!(doc.document.cache_name)
        doc.document.recreate_versions!
        doc.save!
      rescue => e
        puts  "ERROR: #{e.message}: File may have been uploaded in the seeds file: #{doc.id} -> #{doc.description}"
      end
    end
  end
end
