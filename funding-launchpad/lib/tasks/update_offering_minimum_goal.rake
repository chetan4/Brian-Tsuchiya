namespace :offerings do
  desc "Updates existing offerings to store the new minimum goal db field"
  task :update_minimum_goal => :environment do
    Offering.all.each do |offering|
      offering.update_attribute(:minimum_goal, offering.share_price * offering.min_shares)
    end
    puts "Offerings now have a minimum goal saved in db!"
  end
end
