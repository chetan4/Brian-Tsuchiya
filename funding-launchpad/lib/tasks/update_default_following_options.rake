namespace :default_following_options do
  desc "Updates existing DefaultFollowingOptions defaults to true"
  task :update => :environment do
    DefaultFollowingOption.all.each do |dfo|
      dfo.update_attributes({new_offering_update: true, offering_funded: true, offering_expired: true})
    end
    puts "All existing defaults have been updated to receive notifications for new offering update, offering funded, and offering expired"
  end
end
