namespace :investments do
  desc "Updates existing Investments to use new status states"
  task :update => :environment do
    Investment.all.each do |investment|
      case investment.status
      when "Investment request sent to campaign owner"
        investment.update_attribute(:status, Investment.initiated)
      when "Investment request acknowledged by campaign owner"
        investment.update_attribute(:status, Investment.initiated)
      when "Investment request accepted"
        investment.update_attribute(:status, Investment.initiated)
      when "Investment offer declined by campaign owner"
        investment.update_attribute(:status, Investment.declined)
      when "Funds sent to campaign"
        investment.update_attribute(:status, Investment.countersigned)
      when "Funds escrowed for campaign"
        investment.update_attribute(:status, Investment.completed)
      when "Funds distributed to campaign.  You're invested!"
        investment.update_attribute(:status, Investment.completed)
      when "Funds returned"
        investment.update_attribute(:status, Investment.declined)
      end
    end

    InvestmentStatus.all.each do |investment|
      case investment.status
      when "Investment request sent to campaign owner"
        investment.update_attribute(:status, Investment.initiated)
      when "Investment request acknowledged by campaign owner"
        investment.update_attribute(:status, Investment.initiated)
      when "Investment request accepted"
        investment.update_attribute(:status, Investment.initiated)
      when "Investment offer declined by campaign owner"
        investment.update_attribute(:status, Investment.declined)
      when "Funds sent to campaign"
        investment.update_attribute(:status, Investment.countersigned)
      when "Funds escrowed for campaign"
        investment.update_attribute(:status, Investment.completed)
      when "Funds distributed to campaign.  You're invested!"
        investment.update_attribute(:status, Investment.completed)
      when "Funds returned"
        investment.update_attribute(:status, Investment.declined)
      end
    end

    puts "Your investments are all up to date. Now go be awesome!"
  end
end
