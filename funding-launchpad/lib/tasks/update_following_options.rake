namespace :following_options do
  desc "Updates existing FollowingOptions defaults to true"
  task :update => :environment do
    FollowingOption.all.each do |fo|
      fo.update_attributes({new_offering_update: true, offering_funded: true, offering_expired: true})
    end
    puts "All existing defaults have been updated to receive notifications for new offering update, offering funded, and offering expired"
  end
end
