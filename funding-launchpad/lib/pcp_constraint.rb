class PCPConstraint
  def initialize
    @id = PCPConstraint.id
  end

  def matches?(request)
    @id != nil
  end

  def self.id
    return nil unless PCP_CONFIG.has_key? ENV['PCP']
    key       = ENV['PCP'].to_sym
    pcp_name  = PCP_CONFIG[key][:name]

    #big time hack, why is this happening?!?
    if defined?(Company).nil?
      require File.dirname(__FILE__) + "/../app/models/company"
    end

    company = Company.find_by_name pcp_name
    Company.method_defined?(:offering_id) ? company.try(:offering_id) : nil

  end

  def name
    Offering.find(@id).try(:friendly_id) unless @id.nil?
  end
end
