require "spec_helper"

describe LinksController do
  describe "routing" do

    it "routes to #index" do
      get("offerings/1/links").should route_to("links#index", offering_id: "1")
    end

    it "routes to #new" do
      get("offerings/1/links/new").should route_to("links#new", offering_id: "1")
    end

    it "routes to #show" do
      get("offerings/1/links/1").should route_to("links#show", :id => "1", offering_id: "1")
    end

    it "routes to #edit" do
      get("offerings/1/links/1/edit").should route_to("links#edit", :id => "1", offering_id: "1")
    end

    it "routes to #create" do
      post("offerings/1/links").should route_to("links#create", offering_id: "1")
    end

    it "routes to #update" do
      put("offerings/1/links/1").should route_to("links#update", :id => "1", offering_id: "1")
    end

    it "routes to #destroy" do
      delete("offerings/1/links/1").should route_to("links#destroy", :id => "1", offering_id: "1")
    end

  end
end
