require "spec_helper"

describe DocumentsController do
  describe "routing" do

    it "routes to #index" do
      get("/offerings/1/documents").should route_to("documents#index", offering_id: "1")
    end

    it "routes to #new" do
      get("/offerings/1/documents/new").should route_to("documents#new", offering_id: "1")
    end

    it "routes to #show" do
      get("/offerings/1/documents/1").should route_to("documents#show", offering_id: "1", id: "1")
    end

    it "routes to #edit" do
      get("/offerings/1/documents/1/edit").should route_to("documents#edit", offering_id: "1", id: "1")
    end

    it "routes to #create" do
      post("/offerings/1/documents").should route_to("documents#create", offering_id: "1")
    end

    it "routes to #update" do
      put("/offerings/1/documents/1").should route_to("documents#update", offering_id: "1", id: "1")
    end

    it "routes to #destroy" do
      delete("/offerings/1/documents/1").should route_to("documents#destroy", offering_id: "1", id: "1")
    end

  end
end
