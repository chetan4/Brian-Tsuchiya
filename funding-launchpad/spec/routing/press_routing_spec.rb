require "spec_helper"

describe PressController do
  describe "routing" do

    it "routes to #index" do
      get("/press").should route_to("press#index")
    end

    it "routes to #new" do
      get("/press/new").should route_to("press#new")
    end

    it "routes to #show" do
      get("/press/1").should route_to("press#show", :id => "1")
    end

    it "routes to #edit" do
      get("/press/1/edit").should route_to("press#edit", :id => "1")
    end

    it "routes to #create" do
      post("/press").should route_to("press#create")
    end

    it "routes to #update" do
      put("/press/1").should route_to("press#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/press/1").should route_to("press#destroy", :id => "1")
    end

  end
end
