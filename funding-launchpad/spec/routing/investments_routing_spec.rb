require "spec_helper"

describe InvestmentsController do
  describe "routing" do

    it "routes to #index" do
      get("/offerings/1/investments").should route_to("investments#index", :offering_id => "1")
    end

    it "routes to #new" do
      get("/offerings/1/investments/new").should route_to("investments#new", :offering_id => "1")
    end

    it "routes to #create" do
      post("/offerings/1/investments").should route_to("investments#create", :offering_id => "1")
    end

    it "routes to #update" do
      put("/offerings/1/investments/1").should route_to("investments#update", :offering_id => "1", :id => "1")
    end

    it "routes to #destroy" do
      delete("/offerings/1/investments/1").should route_to("investments#destroy", :offering_id => "1", :id => "1")
    end

  end
end
