require "spec_helper"

describe PhotosController do
  describe "routing" do

    it "routes to #index" do
      get("/offerings/1/photos").should route_to("photos#index", :offering_id => "1")
    end

    it "routes to #new" do
      get("/offerings/1/photos/new").should route_to("photos#new", :offering_id => "1")
    end

    it "routes to #show" do
      get("/offerings/1/photos/1").should route_to("photos#show", :offering_id => "1", :id => "1")
    end

    it "routes to #edit" do
      get("/offerings/1/photos/1/edit").should route_to("photos#edit", :offering_id => "1", :id => "1")
    end

    it "routes to #create" do
      post("/offerings/1/photos").should route_to("photos#create", :offering_id => "1")
    end

    it "routes to #update" do
      put("/offerings/1//photos/1").should route_to("photos#update", :offering_id => "1", :id => "1")
    end

    it "routes to #destroy" do
      delete("/offerings/1/photos/1").should route_to("photos#destroy", :offering_id => "1", :id => "1")
    end

  end
end
