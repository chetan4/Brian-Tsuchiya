require "spec_helper"

describe ContactsController do
  describe "routing" do

    it "routes to #new" do
      get("/contacts/new").should route_to("contacts#new")
    end

    it "routes to #create" do
      post("/contacts").should route_to("contacts#create")
    end
  end
end
