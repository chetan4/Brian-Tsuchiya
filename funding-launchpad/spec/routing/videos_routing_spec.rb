require "spec_helper"

describe VideosController do
  describe "routing" do

    it "routes to #index" do
      get("/offerings/1/videos").should route_to("videos#index", :offering_id => "1")
    end

    it "routes to #new" do
      get("/offerings/1/videos/new").should route_to("videos#new", :offering_id => "1")
    end

    it "routes to #show" do
      get("/offerings/1/videos/1").should route_to("videos#show", :offering_id => "1", :id => "1")
    end

    it "routes to #edit" do
      get("/offerings/1/videos/1/edit").should route_to("videos#edit", :offering_id => "1", :id => "1")
    end

    it "routes to #create" do
      post("/offerings/1/videos").should route_to("videos#create", :offering_id => "1")
    end

    it "routes to #update" do
      put("/offerings/1//videos/1").should route_to("videos#update", :offering_id => "1", :id => "1")
    end

    it "routes to #destroy" do
      delete("/offerings/1/videos/1").should route_to("videos#destroy", :offering_id => "1", :id => "1")
    end

  end
end
