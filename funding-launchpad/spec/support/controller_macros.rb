module ControllerMacros
  def login_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @user = FactoryGirl.create(:user, confirmed_at: Time.now, person: FactoryGirl.create(:person_with_address))
      sign_in @user
    end
  end

  def login_vim_admin_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @user = FactoryGirl.create(:user, confirmed_at: Time.now, person: FactoryGirl.create(:person_with_address))
      @user.add_role :vim_admin
      sign_in @user
    end
  end
end
