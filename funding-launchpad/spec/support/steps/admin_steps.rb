step "I choose :menu_option from the user drop down" do |menu_option|
  click_link "user-dropdown"
  click_link menu_option.capitalize
end

step "I should see a list of unapproved registered users" do
  page.should have_content "User Maintenance"
end
