step "I fill in :field_name with :content" do |field_name, content|
  fill_in field_name, with: content
end

step "I choose :field_label" do |field_label|
  choose field_label
end

step "I select :option from :dropdown" do |option, dropdown|
  select(option, :from => dropdown)
end

step "I press :button_text" do |button_text|
  click_link_or_button button_text
end

step "I should see a notification saying :notification_text" do |notification_text|
  page.should have_content notification_text
end

step "I should see a not authorized notification" do
  page.should have_content "You are not authorized to access this page."
end

step "I will receive the welcome email" do
  email = ActionMailer::Base.deliveries.last
  email.to.should include @email
  email.should have_body_text /Congratulations/
end

step "I will receive the welcome email at :email_address" do |email_address|
  email = ActionMailer::Base.deliveries.last
  email.to.should include email_address
  email.should have_body_text /Congratulations/
end

step "I will receive the newsletter email at :email_address" do |email_address|
  email = ActionMailer::Base.deliveries.last
  email.to.should include email_address
  email.should have_body_text /newsletter/
end

step "I submit the newsletter form" do
  within('#newsletter') do
    fill_in 'user_email', with: 'foo@fundinglaunchpad.com'
    click_link_or_button 'Submit'
  end
end

step "I submit the investor request form" do
  @email = FactoryGirl.build(:user).email
  within('#investor-invite') do
    fill_in 'user_email', with: @email
    click_link_or_button 'Request Access!'
  end
end

step "I should see content :content_text" do |content_text|
  page.should have_content content_text
end

step "I should see a focus around login well" do
  within("form#signin") do
    page.should have_css(".focus")
  end
end

step "I should see a focus around signup well" do
  within("form#new_user") do
    page.should have_css(".focus")
  end
end

step "I should see an optin for the newsletter" do
  within("form#new_user") do
    page.should have_css("input#user_newsletter")
  end
end

step "I should not see an optin for the newsletter" do
  within("form#new_user") do
    page.should_not have_css("input#user_newsletter")
  end
end

step "I should see an optin for the pcp email" do
  within("form#new_user") do
    page.should have_css("input#user_pcp_email")
  end
end

step "I fill out the form opting into both emails" do
  @user = FactoryGirl.build(:user,:email => 'jcousteau@gmail.com')
  fill_in 'user_email', with: @user.email
  fill_in 'user_password', with: 'secret'
  fill_in 'user_password_confirmation', with: 'secret'
  check   'user_pcp_email'
  check   'user_newsletter'
  choose 'user_person_attributes_resident_true'
  select('California', :from => 'user_person_attributes_addresses_attributes_0_state')
  click_button 'Create Account'
end

step "I fill out the form opting into pcp email" do
  @user = FactoryGirl.build(:user,:email => 'jcousteau@gmail.com')
  fill_in 'user_email', with: @user.email
  fill_in 'user_password', with: 'secret'
  fill_in 'user_password_confirmation', with: 'secret'
  check   'user_pcp_email'
  choose 'user_person_attributes_resident_true'
  select('California', :from => 'user_person_attributes_addresses_attributes_0_state')
  click_button 'Create Account'
end

step "I fill out the form not opting into pcp email" do
  @user = FactoryGirl.build(:user,:email => 'jcousteau@gmail.com')
  fill_in 'user_email', with: @user.email
  fill_in 'user_password', with: 'secret'
  fill_in 'user_password_confirmation', with: 'secret'
  uncheck   'user_pcp_email'
  choose 'user_person_attributes_resident_true'
  select('California', :from => 'user_person_attributes_addresses_attributes_0_state')
  click_button 'Create Account'
end

step "I should be opted in" do
  user = User.find_by_email @user.email
  user.newsletter.should eq true
  user.pcp_email.should eq true
end

step "I should be opted in to pcp email" do
  user = User.find_by_email @user.email
  user.newsletter.should eq false
  user.pcp_email.should eq true
end

step "I fill out the form not opting into both emails" do
  @user = FactoryGirl.build(:user,:email => 'jcousteau@gmail.com')
  fill_in 'user_person_attributes_first_name', with: 'Jacques'
  fill_in 'user_person_attributes_last_name', with: 'Cousteau'
  fill_in 'user_email', with: @user.email
  fill_in 'user_password', with: 'secret'
  fill_in 'user_password_confirmation', with: 'secret'
  uncheck 'user_pcp_email'
  uncheck 'user_newsletter'
  choose 'user_person_attributes_resident_true'
  select('California', :from => 'user_person_attributes_addresses_attributes_0_state')
  click_button 'Create Account'
end

step "I should not be opted in" do
  user = User.find_by_email @user.email
  user.newsletter.should == false
  user.pcp_email.should == false
end

step "I submit the new investment form" do
  current_path.should == new_offering_investment_path(@offering)
  within('#new_investment') do
    fill_in 'investment_offered_amount', with: 10
    click_on 'Submit'
  end
end

step "I should see sharethis buttons" do
  page.has_css?("span.st_sharethis_vcount")
end

step "I fill out the contact form" do
  @user = FactoryGirl.build(:user)
  fill_in I18n.t(:name, :scope => [:simple_form, :labels, :contact]), with: 'John Smith'
  fill_in 'Email', with: @user.email
  fill_in I18n.t(:raise_amount, :scope => [:simple_form, :labels, :contact]), with: '100'
  select  Contact::BUSINESS_STATES.first, from: I18n.t(:business_state, :scope => [:simple_form, :labels, :contact])
  select  Contact::INDUSTRIES.first, from: 'Industry'
  fill_in I18n.t(:description, :scope => [:simple_form, :labels, :contact]), with: 'lorem ipsum'
  click_button 'Submit my details!'
end

step "I select :option as a :dropdown" do |option, dropdown|
  select(option, :from => dropdown)
end

step "I should see the :dom_id input" do |dom_id|
  assert find(".#{dom_id}").visible?
end

step "a contact should be created" do
  page.should have_content("Thanks for contacting us! We'll be in touch shortly.")
  Contact.all.size.should == 1
end

step "I should be sent a confirmation email" do
  email = ActionMailer::Base.deliveries[-2]
  email.to.should include @user.email
  email.should have_body_text /Thank you for contacting Funding Launchpad/
end

step "the team should receive a notification" do
  email = ActionMailer::Base.deliveries.last
  email.to.should include 'info@fundinglaunchpad.com'
  email.should have_body_text /interested Issuer/
end
