step "I run the app as a pcp" do
  ENV['PCP_ENV'] = 'true'
  @offering = FactoryGirl.create(:offering, status: 14, company: FactoryGirl.create(:company))
end

step "I run the app instance as :instance_name" do |instance_name|
  ENV['PCP_ENV'] = 'true'
  ENV['PCP'] = instance_name
  @offering = FactoryGirl.create(:offering, status: 14)
  @company  = FactoryGirl.create(:company, :name => PCP_CONFIG[ENV['PCP']][:name], :offering_id => @offering.id)
end

step "I should have a global object for that pcp" do
  PCP_CONFIG[ENV['PCP']].empty?.should == false
end

step "I should be redirected to the offering page" do
  page.current_path.should eq offering_path(@offering)
end

step "I don't run the app as a pcp" do
  ENV['PCP_ENV'] = 'false'
  ENV['PCP'] = "dont_match_anything"
end

step "I submit the investment form" do
  fill_in :accepted_amount, with: 1000
  click_link_or_button 'Submit Investment'
end

step "there should be an investment saved" do
  @investment = Investment.last
  @investment.accepted_amount.should == 1000
  @offering.invested.should == 1000
end

step "I have signed up for the beta" do
  @user = FactoryGirl.build(:unconfirmed_user, confirmed_at: nil)
  @user.save(:validate => false)
end

step "I sign up again at a PCP" do
  visit new_user_registration_path
  fill_in 'user_email', with: @user.email
  fill_in 'user_password', with: 'secret'
  fill_in 'user_password_confirmation', with: 'secret'
  choose 'user_person_attributes_resident_true'
  select('California', :from => 'user_person_attributes_addresses_attributes_0_state')
  click_button 'Create Account'
  page.current_path.should eq new_user_registration_path
end

step "I sign up at a PCP" do
  visit new_user_registration_path
  fill_in 'user_email', with: @user.email
  fill_in 'user_password', with: 'secret'
  fill_in 'user_password_confirmation', with: 'secret'
  choose 'user_person_attributes_resident_true'
  select('California', :from => 'user_person_attributes_addresses_attributes_0_state')
  click_button 'Create Account'
  page.current_path.should eq root_path
end

step "I should receive the confirmation email" do
  email = ActionMailer::Base.deliveries.last
  email.to.should include @user.email
  email.should have_body_text(/confirm/)
end

step "the confirmation email should have pcp language" do
  email = ActionMailer::Base.deliveries.last
  email.should have_body_text(/#{PCP_CONFIG.name}/)
end

step "I have completed registration" do
  @user = FactoryGirl.create(:user)
end

step "I visit the pcp sign in page" do
  visit new_user_session_path(email: @user.email)
end

step "I should be able to sign in" do
  page.current_path.should eq new_user_session_path
end

step "I should be redirected to sign up" do
  page.current_path.should eq new_user_registration_path
end

step "I am an incomplete user from the list" do
  @user = FactoryGirl.build(:unconfirmed_user)
end

step "I can edit the various offering sections" do
  page.should have_selector("div#offering-accordion")
  page.should have_content('Update')
end

step "I should see a WYSIWYG" do
  page.should have_selector("ul.wysihtml5-toolbar")
end

step "I have already uploaded photos" do
  @photo1 = FactoryGirl.create(:photo, offering: @offering)
  @photo2 = FactoryGirl.create(:photo, offering: @offering, default: true)
end

step "I mark a photo as the default" do
  within("tr#photo_#{@photo1.id}") do
    click_button("Make Default")
  end
end

step "I should see it featured on the offering page" do
  @photo1.reload
  @photo1.default.should == true
  visit offering_path(@offering)
  page.should have_css("img[src$='#{@photo1.image_url.split('/').last}']")
end

step "I should not see it listed in the photos section" do
  within("ul.thumbnails") do
    all("li").size.should == 1 # uploaded two photos and made one the default
  end
end
