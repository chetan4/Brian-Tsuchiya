
step "I visit the :page_name" do |page_name|
  case page_name
    when 'home page'
      visit root_path
    when 'sign up page'
      visit new_user_registration_path
    when 'administration page'
      visit admin_path
    when 'settings page'
      visit settings_path
    when 'new company page'
      visit new_offering_company_path(@offering)
    when 'new offering page'
      visit new_offering_path
    when 'root page'
      visit root_path
    when 'private investment page'
      visit new_offering_private_investment_path(@offering)
    when 'updates admin page'
      visit admin_offering_updates_path(@offering)
    when 'offering page'
      visit offering_path(@offering)
    when 'login page'
      visit new_user_session_path
    when 'investment share page'
      visit offering_investments_share_path(@offering)
    when 'offering admin page'
      visit admin_offering_path(@offering)
    when 'offering edit page for section d'
      visit edit_offering_path(@offering, section: 'D')
    when 'offering admin photo page'
      visit offering_photos_path(@offering)
    when 'new investment page'
      visit new_offering_investment_path(@offering)
    when 'offerings page'
      visit offerings_path
    when 'contact page'
      visit new_contact_path
    when 'updates page'
      visit offering_updates_path(@offering)
    else
      puts "path not available for provided page"
  end
end

step "I should see a button :button_text" do |button_text|
  page.should have_content button_text
end

step "I click a button :button_text" do |button_text|
  click_link_or_button button_text
end

step "I click the link :link_text" do |link_text|
  click_link_or_button link_text
end

step "I should see a form with a field :field_name" do |field_name|
  within("form") do
    page.has_field? field_name
  end
end

step "I should be on the :page_name" do |page_name|
  case page_name
    when "sign up page"
      page.current_path.should eq new_user_registration_path
    when "administration page"
      page.current_path.should eq admin_path
    when "Users page"
      page.current_path.should eq users_path
    when "new company page"
      page.current_path.should eq new_offering_company_path(@offering)
    when "new offering page"
      page.current_path.should eq new_offering_path
    when 'updates admin page'
      page.current_path.should eq admin_offering_updates_path(@offering)
    when 'root page'
      page.current_path.should eq root_path
    when "new investment page"
      page.current_path.should eq new_offering_investment_path(@offering)
    when "updates page"
      page.current_path.should eq offering_updates_path(@offering)
    when 'offerings page'
      visit offerings_path
    else
      puts "page name not found"
  end
end

step "I should see a tab :tab_text" do |tab_text|
  within(".tabs") do
    page.should have_selector("a", :text => tab_text)
  end
end

step "I should not see a tab :tab_text" do |tab_text|
  within(".tabs") do
    page.should_not have_selector("a", :text => tab_text)
  end
end

step "I click a tab :tab_text" do |tab_text|
  click_link tab_text
end

step "I should see a link :link_text" do |link_text|
  page.should have_link(link_text)
end

step "I should not see a link :link_text" do |link_text|
  page.should_not have_link(link_text)
end

step "I should see a dropdown with a link :link_text" do |link_text|
  within(".dropdown-menu") do
    page.should have_link(link_text)
  end
end

step "I should see a dropdown without a link :link_text" do |link_text|
  within(".dropdown-menu") do
    page.should_not have_link(link_text)
  end
end

step "I should see the pcp logo" do
  within("#fl-logo") do
    page.should have_image PCP_CONFIG[ENV['PCP']][:logo]
  end
end

step "I should see the normal logo" do
  within("#fl-logo") do
    page.should have_image "fl-logo-small.png"
  end
end

step "I should :not_text see :content in the footer" do |not_text, content|
  within(".footer-main") do
    not_text.blank? ? page.should(have_selector("h3", text: content)) : page.should_not(have_selector("h3", text: content))
  end
end

step "I should :not_text see the pcp links" do |not_text|
  within(".footer-main") do
    if not_text.blank?
      page.should have_link "Twitter", href: "https://twitter.com/#{PCP_CONFIG.twitter_name}"
      page.should have_link "Facebook", href: PCP_CONFIG.facebook_url
    else
      page.should_not have_link "Twitter"
      page.should_not have_link "Facebook"
    end
  end
end

step "I should :not_text see the nav links" do |not_text|
  within(".navbar") do
    if not_text.blank?
      page.should have_link "Blog"
      page.should have_link "About"
    else
      page.should_not have_link "Blog"
      page.should_not have_link "About"
    end
  end
end

step "I should :not_text see the login links" do |not_text|
  within(".navbar") do
    if not_text.blank?
      page.should have_link "Log In"
    else
      page.should_not have_link "Log In"
    end
  end
end

step "I should see :content" do |content|
  page.should have_content content
end

step "I should see an open graph :tag_type tag with :content" do |tag_type, content|
  within("head") do
    og = "og:#{tag_type}"
    page.should have_xpath("//meta[@property='#{og}' and contains(@content,'#{content}')]")
  end
end

step "I should see a modal with :content" do |content|
  within(".modal") do
    page.should have_content content
  end
end

step "I should see a modal with translation :key" do |key|
  within(".modal") do
    page.should have_content I18n.t(key)
  end
end

step "I click the num edits link" do
  within(".update-header") do
    click_link "(2 Edits)"
  end
end

step "I should see update revisions content" do
  page.should have_content "You are viewing the edit history"
  page.should have_link "Return to Updates"
  page.should have_css(".version-edit-person", :text => @user2.person.first_name)
  page.should have_css(".version-edit-person", :text => @user.person.first_name)
  page.all('.accordion-group').size.should eq 3
  page.should have_content "Original"
  page.should have_content "Version 1"
  page.should have_content "Current"

  #current revision since 'within' matches first instance
  within(".accordion-footer") do
    page.should have_content "I like bacon!"
  end

  page.should have_content "I like turtles!"
end

step "show me the page" do
  save_and_open_page
end


