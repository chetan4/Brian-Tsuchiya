step "there is an offering" do
  @offering = FactoryGirl.create(:offering, status: 14)
end

step "I should see offering details" do
  page.should have_content "Offering Overview"
end

step "I visit a pcp offering page on the main site" do
  @offering = FactoryGirl.create(:offering, name: PCP_CONFIG.first[1]['name'])
  visit offering_path(@offering)
end

step "I should be redirected to the root" do
  page.current_path.should eq root_path
end

step "the offering has not met its minimum goal" do
  @investment = FactoryGirl.create(:investment, accepted_amount: 5, offering_id: @offering.id)
end

step "the offering has met its minimum goal" do
  @investment = FactoryGirl.create(:investment, accepted_amount: 50000, offering_id: @offering.id)
end

step "the offering has an update with versions" do
  with_versioning do
    PaperTrail.whodunnit = @user.id
    @person2 = FactoryGirl.create(:person, first_name: "Bob", last_name: "Smith")
    @user2 = FactoryGirl.create(:user, person_id: @person2.id)

    @update = FactoryGirl.create(:update, offering_id: @offering.id)
    @update.update_attributes({subject: "Weird", content: "Guy", edit_summary: "I like turtles!"})

    PaperTrail.whodunnit = @user2.id
    @update.update_attributes({subject: "Crispy", content: "Bacon", edit_summary: "I like bacon!"})

  end
end

step "I have been invited to an offering" do
  @private_offering = FactoryGirl.create(:offering, status: 14, name: 'private offering example', private: 1)
  @user.update_attributes(invitation_offering: @private_offering.id)
end

step "I should see that private offering" do
  page.should have_content @private_offering.name
end
