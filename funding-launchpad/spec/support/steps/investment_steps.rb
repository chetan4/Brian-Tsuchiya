step "the offering has a required doc" do
  document = FactoryGirl.create(:document, :offering_id => @offering.id, :description => 'lorem ipsum', :required => true)
  @offering.documents << document
end

step "I click the required doc" do
  within("#documents") do
    click_on("#{@offering.documents.first.description}")
  end
end

step "I click the :button_text button" do |button_text|
  click_on(button_text)
end

step "I should see a required disclaimer" do
  page.should have_content("Investor Requirement!")
  page.should have_css?("#required_modal")
end

step "I should not see a required disclaimer" do
  page.should_not have_content("Investor Requirement!")
end
