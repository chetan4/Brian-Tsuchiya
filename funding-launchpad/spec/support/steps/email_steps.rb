step "I should receive the you invested email" do
  email = ActionMailer::Base.deliveries[-2]
  email.to.should include @user.email
  email.should have_body_text /expressed an interest/
end

step "the you invested email should have platform language" do
  email = ActionMailer::Base.deliveries[-2]
  email.should have_body_text /info@fundinglaunchpad.com/
end

step "the you invested email should have pcp language" do
  email = ActionMailer::Base.deliveries[-2]
  email.should have_body_text /#{PCP_CONFIG.support_email}/
end
