require 'user_helper'

step "I am logged in" do
  @user = FactoryGirl.create(:user)
  as_user(@user)
  @offering = FactoryGirl.create(:offering_with_filing, status: 14, company: FactoryGirl.create(:company))
end

step "I am logged in as offering admin" do
  @user = FactoryGirl.create(:user)
  @offering ||= FactoryGirl.create(:offering, company: FactoryGirl.create(:company))
  @user.add_role(:offering_admin, @offering)
  as_user(@user)
end

step "I am logged in without a company" do
  @user = FactoryGirl.create(:user)
  @user.person.company.destroy
  as_user(@user)
end

step "I am not logged in" do
  visit destroy_user_session_path
end

step "I am logged in as an administrator" do
  @user = FactoryGirl.create(:user, :email => "admin@fundinglaunchpad.com", :password => "secret", :access => 1)
  @user.confirm!
  @user.add_role(:vim_admin)
  visit "/login"
  fill_in "signin_email", with: "admin@fundinglaunchpad.com"
  fill_in "signin_password", with: "secret"
  click_button "Log In"
end

step "I am already subscribed to the newsletter with :email_address" do |email_address|
  @user = User.create(:email => email_address)
  @user.newsletter = 1
  @user.save!
end

step "I am logged in with :role_text" do |role_text|
  @user = FactoryGirl.create(:user)
  case role_text
    when "pcp_admin role"
      @user.add_role(:pcp_admin)
    else
      puts "role name not found"
  end
  as_user(@user)
end

step "I am logged in as pcp admin" do
  @user = FactoryGirl.create(:user, email: "admin@fundinglaunchpad.com")
  @user.add_role(:pcp_admin)
  as_user(@user)
end

step "I should have a person record with a residence address" do
  user = User.last
  user.person.should_not eq nil
  user.person.addresses.should_not eq []
  user.person.addresses.residence.should_not eq []
end
