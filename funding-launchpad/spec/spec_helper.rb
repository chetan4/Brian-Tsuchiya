require 'simplecov'
SimpleCov.start

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'capybara/webkit'
require 'turnip/capybara'
require 'database_cleaner'
require 'email_spec'

Capybara.javascript_driver = :webkit

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

# load fake font-awesome.sass file in the test environment
# to fix the webkit broken pipe errors
Rails.application.config.assets.paths << "spec/support/assets"

RSpec.configure do |config|
  config.mock_with :rspec
  config.filter_run :focus => true
  config.run_all_when_everything_filtered = true

  DatabaseCleaner.strategy = :truncation
  config.before do
    DatabaseCleaner.start
  end

  config.before :each do
    PaperTrail.controller_info = {}
    PaperTrail.whodunnit = nil
    ActionMailer::Base.deliveries.clear
  end

  config.after do
    DatabaseCleaner.clean
    ENV['PCP_ENV'] = 'false'
    ENV['PCP'] = 'this_doesnt_match_anything'
  end

  config.after(:all) do
    if Rails.env.test?
      FileUtils.rm_rf(Dir["#{Rails.root}/spec/support/uploads"])
    end
  end

  # include helpers
  config.treat_symbols_as_metadata_keys_with_true_values = true
  config.include Devise::TestHelpers, :type => :controller
  config.extend ControllerMacros,     :type => :controller
  config.include EmailSpec::Matchers, :turnip, type: :mailer
  config.include EmailSpec::Helpers,  :turnip, type: :mailer
end

if defined?(CarrierWave)
  CarrierWave::Uploader::Base.descendants.each do |klass|
    next if klass.anonymous?
    klass.class_eval do
      def cache_dir
        "#{Rails.root}/spec/support/uploads/tmp"
      end

      def store_dir
        "#{Rails.root}/spec/support/uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
      end
    end
  end
end

def with_versioning
  was_enabled = PaperTrail.enabled?
  PaperTrail.enabled = true
  begin
    yield
  ensure
    PaperTrail.enabled = was_enabled
  end
end
