require 'spec_helper'

describe QuestionObserver do
  describe "should observe Question#save" do
    it "should email the Issuer and followers to inform them a question has been asked" do
      contact   = FactoryGirl.create(:user)
      offering  = FactoryGirl.create(:offering, contact: contact)
      follower  = FactoryGirl.create(:following, offering_id: offering.id)
      follow_option = FactoryGirl.create(:following_option, offering_id: offering.id, user_id: follower.user_id, new_offering_qa: 1)
      follower_1  = FactoryGirl.create(:following, offering_id: offering.id)
      follow_option = FactoryGirl.create(:following_option, offering_id: offering.id, user_id: follower_1.user_id, new_offering_qa: 1)
      follower_contact  = FactoryGirl.create(:following, offering_id: offering.id, user_id: contact.id)
      follow_option = FactoryGirl.create(:following_option, offering_id: offering.id, user_id: contact.id, new_offering_qa: 1)

      question  = FactoryGirl.build(:question, offering_id: offering.id, user_id: follower_1.user_id)
      question.save

      #Issuer Email is sent
      email_issuer = ActionMailer::Base.deliveries[-2]
      email_issuer.to.should include question.offering.contact.email
      email_issuer.should have_body_text /#{question.content}/

      #Follower email is sent to all followers, except if the follower
      #is the user who asked the question or the issuer
      email_followers = ActionMailer::Base.deliveries.last
      email_followers.bcc.should include follower.user.email
      email_followers.bcc.should_not include follower_1.user.email
      email_followers.bcc.should_not include question.offering.contact.email

      email_followers.should have_body_text /#{question.content}/
      email_followers.should have_subject "#{offering.name}: A new question has been posted"
    end

    it "should not send an email if there are no followers" do
      contact   = FactoryGirl.create(:user)
      offering  = FactoryGirl.create(:offering, contact: contact)
      question  = FactoryGirl.build(:question, offering_id: offering.id)
      question.save

      #Only the issuer email is sent.
      ActionMailer::Base.deliveries.length.should eq 1
    end
  end
end
