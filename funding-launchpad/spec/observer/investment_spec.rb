require 'spec_helper'

describe InvestmentObserver do
  before (:each) do
    @user       = FactoryGirl.build(:user)
    @offering   = FactoryGirl.build(:offering, :contact => @user)
    @investment = FactoryGirl.build(:investment, :offering => @offering)
  end

  describe "should observer Investment#save" do
    it "should create an investment status upon each save" do
      @investment.save
      InvestmentStatus.last.status.should == Investment.initiated
    end
  end

  describe "should observer Investment#create" do
    it "should send a you invested email upon create" do
      @investment.save
      ActionMailer::Base.deliveries[-2].subject.should == "You've Expressed an Interest in Investing!"
    end

    it "should send a new investment email upon create" do
      @investment.save
      ActionMailer::Base.deliveries.last.subject.should == "Someone Has Expressed an Interest in Investing!"
    end
  end
end
