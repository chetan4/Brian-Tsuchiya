require 'spec_helper'

describe AnswerObserver do
  describe "should observe Answer#save" do

    it "should email the Issuer and followers to inform them an answer has been posted" do
      contact   = FactoryGirl.create(:user)
      offering  = FactoryGirl.create(:offering, contact: contact)
      follower  = FactoryGirl.create(:following, offering_id: offering.id)
      follow_option = FactoryGirl.create(:following_option, offering_id: offering.id, user_id: follower.user_id, new_offering_qa: 1)
      follower_1  = FactoryGirl.create(:following, offering_id: offering.id)
      follow_option = FactoryGirl.create(:following_option, offering_id: offering.id, user_id: follower_1.user_id, new_offering_qa: 1)
      follower_contact  = FactoryGirl.create(:following, offering_id: offering.id, user_id: contact.id)
      follow_option = FactoryGirl.create(:following_option, offering_id: offering.id, user_id: contact.id, new_offering_qa: 1)
      follower_questioner  = FactoryGirl.create(:following, offering_id: offering.id)
      follow_option = FactoryGirl.create(:following_option, offering_id: offering.id, user_id: follower_questioner.user_id, new_offering_qa: 1)

      question  = FactoryGirl.create(:question, offering_id: offering.id, user_id: follower_questioner.user_id)
      answer    = FactoryGirl.build(:answer, question_id: question.id)
      answer.save

      #Issuer Email is sent
      email_participants = ActionMailer::Base.deliveries[-2]
      email_participants.bcc.should include question.offering.contact.email
      email_participants.bcc.should include question.user.email
      email_participants.bcc.should_not include answer.user.email
      email_participants.should have_body_text /#{question.content}/
      email_participants.should have_body_text /#{answer.content}/
      email_participants.should have_subject "#{offering.name}: A new answer has been posted"

      #Follower email is sent to all followers, except if the follower
      #is the user who answered or the issuer
      email_followers = ActionMailer::Base.deliveries.last
      email_followers.bcc.should include follower.user.email
      email_followers.bcc.should include follower_1.user.email
      email_followers.bcc.should_not include question.offering.contact.email
      email_followers.bcc.should_not include follower_questioner.user.email
      email_followers.bcc.should_not include answer.user.email

      email_followers.should have_body_text /#{question.content}/
      email_followers.should have_body_text /#{answer.content}/
      email_followers.should have_subject "#{offering.name}: A new answer has been posted"
    end

    it "should not send an email if there are no followers" do
      contact   = FactoryGirl.create(:user)
      offering  = FactoryGirl.create(:offering, contact: contact)
      question  = FactoryGirl.create(:question, offering_id: offering.id, user_id: FactoryGirl.create(:user).id)
      answer    = FactoryGirl.build(:answer, question_id: question.id)
      answer.save

      #Only the issuer email is sent for question save and answer save
      ActionMailer::Base.deliveries.length.should eq 2
      email = ActionMailer::Base.deliveries.last
      email.should have_subject "#{offering.name}: A new answer has been posted"
    end
  end
end


