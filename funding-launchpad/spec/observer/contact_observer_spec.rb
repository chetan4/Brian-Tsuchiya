require 'spec_helper'

describe ContactObserver do
  describe "should observe Contact#save" do
    it "should email a confirmation email to the contact" do
      contact   = FactoryGirl.create(:contact)
      contact.save
      email = ActionMailer::Base.deliveries[-2]
      email.to.should include contact.email
      email.should have_body_text /Thank you for contacting Funding Launchpad/
    end

    it "should email the team to notify them of a new contact" do
      contact   = FactoryGirl.create(:contact)
      contact.save
      email = ActionMailer::Base.deliveries.last
      email.to.should include 'info@fundinglaunchpad.com'
      email.should have_body_text /interested Issuer/
    end
  end
end
