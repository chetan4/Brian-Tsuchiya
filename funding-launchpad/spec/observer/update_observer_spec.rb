require 'spec_helper'

describe UpdateObserver do
  describe "should observe Update#update" do
    it "should email an array of users to inform them an update has been posted for a given offering" do
      contact       = FactoryGirl.create(:user)
      offering      = FactoryGirl.create(:offering, contact: contact)
      follower      = FactoryGirl.create(:following, user_id: contact.id, offering_id: offering.id)
      follow_option = FactoryGirl.create(:following_option, offering_id: offering.id, user_id: contact.id, new_offering_update: 1)
      update        = FactoryGirl.create(:update, offering_id: offering.id)
      email = ActionMailer::Base.deliveries.last
      email.to.should include contact.email
      email.should have_body_text(/#{update.content}/)
    end
  end
end
