require 'spec_helper'

describe AnswersController do
  login_user

  before(:each) do
    @contact  = FactoryGirl.create(:user)
    @offering = FactoryGirl.create(:offering, contact: @contact)
    @question = FactoryGirl.create(:question, offering: @offering, user_id: FactoryGirl.create(:user).id)
    @answer   = FactoryGirl.create(:answer, question: @question)
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new', offering_id: @offering.id, question_id: @question.id
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "returns http success" do
      #answer = FactoryGirl.create(:answer)
      post 'create', offering_id: @offering.id, question_id: @question.id, answer: {content: "this is my answer"}
      response.should redirect_to(offering_question_path(@offering, @question))
    end
  end
end
