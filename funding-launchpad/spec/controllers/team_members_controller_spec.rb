require 'spec_helper'

describe TeamMembersController do
login_user

  before(:each) do
    @offering = FactoryGirl.create(:offering, contact_id: @user.id)
    @role = FactoryGirl.create(:offering_admin_role, resource_id: @offering.id)
    @user.add_role :offering_admin, @offering
    @user.save!
    @team_member = FactoryGirl.create(:team_member, offering_id: @offering.id)
  end

  describe "GET 'index'" do
    it "should return http success" do
      get 'index', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'new'" do
    it "should return http succes" do
      get 'new', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    it "should return http success" do
      get 'edit', id: @team_member.id, offering_id: @offering.id
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "should redirect to offering_team_members_path" do
      post 'create', team_member: {name: "John Doe"}, offering_id: @offering.id
      response.should redirect_to(offering_team_members_url(@offering))
    end
  end

  describe "PUT 'update'" do
    it "should redirect to offering_team_members_path" do
      put 'update', team_member: {}, offering_id: @offering.id, id: @team_member.id
      response.should redirect_to(offering_team_members_path(@offering))
    end
    it "should return nothing when updating the collection of team members" do
      put 'update', team_member: @offering.team_members.map(&:id), offering_id: @offering.id
      response.body.should be_blank
    end
  end

  describe "DELETE 'destroy'" do
    it "should redirect to offering_team_members_path" do
      delete 'destroy', offering_id: @offering.id, id: @team_member.id
      response.should redirect_to(offering_team_members_path(@offering))
    end
  end
end
