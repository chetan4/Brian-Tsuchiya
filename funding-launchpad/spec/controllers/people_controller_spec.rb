require 'spec_helper'

describe PeopleController do
  before(:each) do
    @user2 = FactoryGirl.create(:user, person: FactoryGirl.create(:person))
    @offering = FactoryGirl.create(:offering)
  end

  describe "GET 'index'" do
    login_user
    it "should return http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    login_user
    it "should return http success" do
      get 'show', id: @user2.person.id
      response.should be_success
    end
  end

  describe "GET 'new'" do
    login_user
    it "should return http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    login_user
    it "should return http success" do
      get 'edit', id: @user.person.id
      response.should be_success
    end
  end

  describe "POST 'create'" do
    login_user
    it "should redirect_to people_path" do
      post 'create', person: {first_name: "John", last_name: "Example", resident: "1", offering_id: @offering.id}
      response.should redirect_to(people_path(offering: @offering.id))
    end
  end

  describe "PUT 'update'" do
    login_user
    it "should redirect to people_path" do
      person  = FactoryGirl.create(:person)
      address = FactoryGirl.create(:address, addressable_id: person.id, addressable_type: "Person", state: "CO")
      @user.person.addresses << address
      @user.update_attribute(:person_id, person.id)

      put 'update', id: @user.person.id, person: {resident: "1", offering_id: @offering.id}
      response.should redirect_to(people_path(offering: @offering.id))
    end
  end

  describe "DELETE 'destroy'" do
    login_user

    it "should redirect to people_path" do
      @user.person.update_attribute(:offering_id, @offering.id)
      delete 'destroy', id: @user.person.id
      response.should redirect_to(people_path(offering: @offering.id))
    end
  end

  describe "PUT 'profile_update'" do
    login_user

    it "should redirect to the person's profile" do
      address = FactoryGirl.create(:address, addressable_id: @user.person.id, addressable_type: "Person", state: "CO")
      @user.person.addresses << address

      put 'profile_update', id: @user.person.id, person: {resident: "1"}
      response.should redirect_to(profile_person_path(@user.person))
    end
  end
end
