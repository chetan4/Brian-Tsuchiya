require 'spec_helper'
include ApplicationHelper

describe CompaniesController do
  login_user

  before(:each) do
    @offering = FactoryGirl.create(:offering)
    @company = FactoryGirl.create(:company, offering_id: @offering.id, phone: FactoryGirl.create(:phone) )
    @user.add_role :offering_admin, @offering
  end

  # describe "GET 'index'" do
    # it "returns http success" do
      # get 'index', id: @company.id
      # response.should be_success
    # end
  # end

  # describe "GET 'show'" do
    # it "returns http success" do
      # get 'show', id: @company.id
      # response.should be_success
    # end
  # end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    it "returns http success" do
      get 'edit', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "returns http success" do
      post 'create', offering_id: @offering.id, :company => {:name => 'test'}
      response.should redirect_to(admin_offering_path(@offering))
    end
  end

  describe "PUT 'update'" do
    it "returns http success" do
      put 'update', offering_id: @offering.id
      response.should redirect_to(admin_offering_path(@offering))
    end
  end

end
