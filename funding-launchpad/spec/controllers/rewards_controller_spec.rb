require 'spec_helper'

describe RewardsController do
  login_user

  before(:each) do
    @reward = FactoryGirl.create(:reward)
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "returns http success" do
      get 'show', id: @reward.id
      response.should be_success
    end
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    it "returns http success" do
      get 'edit', id: @reward.id
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "redirects to rewards path" do
      offering = FactoryGirl.create(:offering)
      post 'create', reward: @reward.attributes.merge(offering_id: offering.id)
      response.should redirect_to(rewards_path(offering: offering.id))
    end
  end

  describe "PUT 'update'" do
    it "redirects to rewards path" do
      offering = FactoryGirl.create(:offering)
      put 'update', id: @reward.id, reward: {offering_id: offering.id}
      response.should redirect_to(rewards_path(offering: offering.id))
    end
  end

  describe "DELETE 'destroy'" do
    it "redirects to rewards path" do
      delete 'destroy', id: @reward.id
      response.should redirect_to(rewards_path)
    end
  end
end
