require 'spec_helper'

describe VideosController do
  login_user

  before(:each) do
    @offering = FactoryGirl.create(:offering, contact_id: @user.id)
    @role = FactoryGirl.create(:offering_admin_role, resource_id: @offering.id)
    @user.add_role :offering_admin, @offering
    @user.save!
  end

  describe "GET 'preview'" do
    it "should return json" do
      get 'preview', offering_id: @offering.id, video: "http://www.youtube.com/watch?v=b_91lhuUh_4"
      response.body.should match(/\/\/www.youtube.com\/embed\/b_91lhuUh_4\?wmode=opaque/)
    end
  end

  describe "GET 'index'" do
    it "should return http success" do
      get 'index', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'new'" do
    it "should return http success" do
     get 'new', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    it "should return http success" do
      video = FactoryGirl.create(:video, offering_id: @offering.id)
      get 'edit', id: video.id, offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'create'" do
    it "should redirect to offering videos path" do
      get 'create', offering_id: @offering.id, video: {description: "lorem ipsum", link: "http://www.youtube.com/watch?v=b_91lhuUh_4"}
      response.should redirect_to(offering_videos_path(@offering))
    end
  end

  describe "PUT 'update'" do
    it "should redirect to offering videos path" do
      video = FactoryGirl.create(:video, offering_id: @offering.id)
      put 'update', id: video.id, offering_id: @offering.id, video: {}
      response.should redirect_to(offering_videos_path(@offering))
    end
  end

  describe "DELETE 'destroy'" do
    it "should redirect to offering videos path" do
      video = FactoryGirl.create(:video, offering_id: @offering.id)
      delete 'destroy', id: video.id, offering_id: @offering.id
      response.should redirect_to(offering_videos_path(@offering))
    end
  end
end
