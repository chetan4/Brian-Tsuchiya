require 'spec_helper'

describe UpdatesController do
  login_user

  before(:each) do
    @offering = FactoryGirl.create(:offering, contact_id: @user.id)
    @user.add_role :offering_admin, @offering
    @user.save!
    @update = FactoryGirl.create(:update, offering_id: @offering.id)
    end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'admin'" do
    it "returns http success" do
      get 'admin', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "should redirect to updates admin path" do
      post 'create', update: {subject: "test", content: "my update!"}, offering_id: @offering.id, commit: 'Save'
      response.should redirect_to(admin_offering_updates_path(@offering))
    end
  end

  describe "POST 'create'" do
    it "should render show for a preview" do
      post 'create', update: {subject: "test", content: "my update!"}, offering_id: @offering.id, commit: 'Preview'
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    it "should return http success" do
      get 'edit', id: @update.id, offering_id: @offering.id
      response.should be_success
    end
  end

  describe "PUT 'update'" do
    it "should redirect to admin offering path" do
      put 'update', id: @update.id, update: {offering_id: @offering.id}, offering_id: @offering.id, commit: 'Save'
      response.should be_success
    end
  end

  describe "PUT 'update'" do
    it "should render show for a preview" do
      post 'create', update: {subject: "test", content: "my update!"}, offering_id: @offering.id, commit: 'Preview'
      response.should be_success
    end
  end

end
