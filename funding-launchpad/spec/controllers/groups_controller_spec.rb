require 'spec_helper'

describe GroupsController do
  login_user

  before(:each) do
    @group = FactoryGirl.create(:group)
    @user.add_role :group_admin, @group
    @user.add_role :group_creator
  end

  describe "GET index" do
    it "should return http success" do
      get :index
      response.should be_success
    end
  end

  describe "GET show" do
    it "assigns the requested group as @group" do
      get :show, id: @group.id
      assigns(:group).should eq(@group)
      response.should be_success
    end
  end

  describe "GET new" do
    it "assigns a new group as @group" do
      get :new
      assigns(:group).should be_a_new(Group)
      response.should be_success
    end
  end

  describe "GET edit" do
    it "assigns the requested group as @group" do
      get :edit, id: @group.id
      assigns(:group).should eq(@group)
      response.should be_success
    end
  end

  describe "POST create" do
    it "should redirect to groups_path" do
      post :create, group: {name: "test"}
      response.should redirect_to(group_path(Group.last))
    end

  end

  describe "PUT 'update'" do
    it "should redirect to group path" do
      put 'update', group: {}, id: @group.id
      response.should redirect_to(group_path(@group))
    end
  end

  describe "GET 'members'" do
    it "should assign group members to members" do
      @user.add_role :group_member, @group
      get :members, id: @group.id
      assigns(:members).should eq(User.with_role(:group_member, @group))
      response.should be_success
    end
  end

  describe "PUT 'toggle_offering'" do
    it "should toggle the offerings group" do
      @user.add_role :group_member, @group
      @offering = FactoryGirl.create(:offering)
      @user.add_role :offering_admin, @offering
      @offering.group.should eq nil

      put :toggle_offering, id: @group.id, offering_id: @offering.id, :format => 'js'

      response.should be_success
      Offering.find(@offering).group.should eq @group

      put :toggle_offering, id: @group.id, offering_id: @offering.id, :format => 'js'
      response.should be_success
      Offering.find(@offering).group.should eq nil
    end
  end

end
