require 'spec_helper'

describe OfferingsController do
  login_user

  before(:each) do
    @offering = FactoryGirl.create(:offering, contact_id: @user.id)
    @user.add_role :offering_admin, @offering
    
    @vim_admin = FactoryGirl.create(:user)
    @vim_admin.add_role :vim_admin
  end

  describe "PUT 'complete'" do
    it "should change status, email vim admins and return http success" do
      put 'complete', id: @offering.id

      @offering = Offering.find(@offering) #reload it!
      response.should redirect_to(@offering)
      @offering.development_completed?.should eq true
      @offering.in_development?.should eq false

      email = ActionMailer::Base.deliveries.last
      email.to.should include @vim_admin.email
      email.should have_subject "#{@offering.name} is awaiting admin approval!"
    end
  end

end
