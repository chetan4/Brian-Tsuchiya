require 'spec_helper'

describe LinksController do
  login_user

  before(:each) do
    @offering = FactoryGirl.create(:offering)
    @role = FactoryGirl.create(:offering_admin_role, resource_id: @offering.id)
    @user.add_role :offering_admin, @offering
    @user.save!
    @link     = FactoryGirl.create(:link, offering_id: @offering.id)
  end

  describe "GET 'index'" do
    it "should return http success" do
      get 'index', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "should return http success" do
      get 'show', id: @link.id, offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'new'" do
    it "should return http success" do
      get 'new', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    it "should return http success" do
      get 'edit', id: @link.id, offering_id: @offering.id
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "redirects to links path" do
      post 'create', link: {link: 'example.com'}, offering_id: @offering.id
      response.should redirect_to(offering_links_path(@offering))
    end
  end

  describe "PUT 'update'" do
    it "redirects to links path" do
      put 'update', id: @link.id, link: {offering_id: @offering.id}, offering_id: @offering.id
      response.should redirect_to(offering_links_path(@offering))
    end
  end

  describe "DELETE 'destroy'" do
    it "redirects to links url" do
      delete 'destroy', id: @link.id, offering_id: @offering.id
      response.should redirect_to(offering_links_path(@offering))
    end
  end
end
