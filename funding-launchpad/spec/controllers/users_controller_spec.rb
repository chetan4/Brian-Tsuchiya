require 'spec_helper'

describe UsersController do
  login_user

  describe "GET 'index'" do
    login_vim_admin_user
    it "should return http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'index'" do
    it "should not return http success" do
      get 'index'
      response.should_not be_success
    end
  end

  describe "GET 'show'" do
    it "should return http success" do
      get 'show', id: @user.id
      response.should be_success
    end
  end

  # TODO: fix test if controller action is even needed
  #describe "GET 'invite'" do
  #  it "should redirect to new invitation path" do
  #    invitation = FactoryGirl.create(:invitation, registration_key: "rkey", password: "secret")
  #    get 'invite', r: "rkey", p: "secret"
  #    response.should redirect_to(new_invitation_path(i: "rkey"))
  #  end
  #end

  describe "GET 'followings'" do
    it "should return http success" do
      offering  = FactoryGirl.create(:offering)
      following = Following.create(user: @user, offering: offering)
      get 'followings', id: @user.id
      response.should be_success
    end
  end

  describe "GET 'unfollow'" do
    it "should redirect to followings user path" do
      offering  = FactoryGirl.create(:offering)
      following = Following.create(user: @user, offering: offering)
      options   = FollowingOption.create(user: @user, offering_id: offering.id)
      get 'unfollow', id: offering.id
      response.should redirect_to(followings_user_path(@user))
    end
  end
end
