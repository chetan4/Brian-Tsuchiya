require 'spec_helper'

describe PagesController do

  describe "GET 'admin'" do
    it "should be successful" do
      get 'admin'
      response.should redirect_to(new_user_session_path)
    end
  end

  describe "GET 'help'" do
    it "should be successful" do
      get 'help'
      response.should be_success
    end
  end

  describe "GET 'my_offerings'" do
    login_user
    it "should be successful" do
      get 'my_offerings'
      response.should be_success
    end
  end

  describe "GET 'privacy'" do
    it "should be successful" do
      get 'privacy'
      response.should be_success
    end
  end

  describe "GET 'terms'" do
    it "should be successful" do
      get 'terms'
      response.should be_success
    end
  end

  describe "GET 'settings'" do
    login_user
    it "should be successful" do
      get 'settings'
      response.should be_success
    end
  end

  describe "GET 'optin'" do
    it "should be successful" do
      get 'optin', email: FactoryGirl.create(:user).email
      response.should be_redirect
    end
  end
end
