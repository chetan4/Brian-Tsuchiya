require 'spec_helper'

describe QuestionsController do
  login_user
  let(:params) { {:format => 'html'} }

  before(:each) do
    @contact  = FactoryGirl.create(:user)
    @offering = FactoryGirl.create(:offering, contact: @contact, status: 14)
    @question = FactoryGirl.create(:question, offering: @offering)
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index', params.merge(offering_id: @offering.id, page: 1)
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "returns http success" do
      get 'show', params.merge(offering_id: @offering.id, id: @question.id)
      response.should be_success
    end
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "redirects to questions path for offering" do
      post 'create', offering_id: @offering.id, question: {offering_id: @offering.id, content: "lorem ipsum"}
      response.should redirect_to(offering_questions_path(@offering))
    end
  end
end
