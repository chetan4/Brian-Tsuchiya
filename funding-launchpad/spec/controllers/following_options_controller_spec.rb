require 'spec_helper'

describe FollowingOptionsController do
  login_user
  let(:params) { {:format => 'html'} }

  before(:each) do
    @offering   = FactoryGirl.create(:offering)
    @following  = FactoryGirl.create(:following_option, user_id: @user.id, offering_id: @offering.id)
  end

  describe "GET 'edit'" do
    it "returns http success" do
      get 'edit', params.merge(id: @offering.id)
      response.should be_success
    end
  end

  describe "GET 'update'" do
    it "returns http success" do
      get 'update', params.merge(id: @following.id, following_option: {})
      response.should redirect_to followings_user_path(@user)
    end
  end
end
