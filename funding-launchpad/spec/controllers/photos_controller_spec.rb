require 'spec_helper'

describe PhotosController do
  login_user

  before(:each) do
    @offering = FactoryGirl.create(:offering, contact_id: @user.id)
    @role = FactoryGirl.create(:offering_admin_role, resource_id: @offering.id)
    @user.add_role :offering_admin, @offering
    @user.save!
    @photo = FactoryGirl.create(:photo, offering_id: @offering.id)
  end

  describe "GET 'index'" do
    it "should return http success" do
      get 'index', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "should return http success" do
      get 'show', id: @photo.id, offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'new'" do
    it "should return http succes" do
      get 'new', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    it "should return http success" do
      get 'edit', id: @photo.id, offering_id: @offering.id
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "should redirect to offering_photos_path" do
      post 'create', photo: {image: fixture_file_upload(Rails.root + 'spec/data/foobear.jpg', 'image/jpg'), caption: "example"}, offering_id: @offering.id
      response.should redirect_to(offering_photos_path(@offering))
    end
  end

  describe "PUT 'update'" do
    it "should redirect to offering photos path" do
      put 'update', photo: {}, offering_id: @offering.id, id: @photo.id
      response.should redirect_to(offering_photos_path(@offering))
    end
  end

  describe "DELETE 'destroy'" do
    it "should redirect to offering photos path" do
      delete 'destroy', offering_id: @offering.id, id: @photo.id
      response.should redirect_to(offering_photos_path(@offering))
    end
  end
end
