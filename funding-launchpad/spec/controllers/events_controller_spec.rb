require 'spec_helper'

describe EventsController do
  login_user

  before(:each) do
    @offering = FactoryGirl.create(:offering, contact_id: @user.id)
    @role = FactoryGirl.create(:offering_admin_role, resource_id: @offering.id)
    @user.add_role :offering_admin, @offering
    @user.save!
    @event = FactoryGirl.create(:event, offering_id: @offering.id)
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    it "returns http success" do
      get 'edit', offering_id: @offering.id, id: @event.id
      response.should be_success
    end
  end

  describe "GET 'admin'" do
    it "returns http success" do
      get 'admin', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "returns http success" do
      post 'create', offering_id: @offering.id, event: {formatted_date_string: "05/01/2030", time_string: "1:00pm", title: "foo", description: "bar"}
      response.should redirect_to(offering_events_admin_path(@offering))
    end
  end

  describe "PUT 'update'" do
    it "returns http success" do
      put 'update', offering_id: @offering.id, event: {}, id: @event.id
      response.should redirect_to(offering_events_admin_path(@offering))
    end
  end

  describe "DELETE 'destroy'" do
    it "returns http success" do
      delete 'destroy', offering_id: @offering.id, id: @event.id
      response.should redirect_to(offering_events_admin_path(@offering))
    end
  end
end
