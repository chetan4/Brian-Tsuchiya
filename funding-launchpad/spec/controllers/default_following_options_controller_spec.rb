require 'spec_helper'

describe DefaultFollowingOptionsController do
  login_user
  let(:params) { {:format => 'html'} }

  before(:each) do
    @id = FactoryGirl.create(:default_following_option, :user_id => @user.id).id
  end

  describe "GET 'edit'" do
    it "returns http success" do
      get 'edit', params.merge(id: @id)
      response.should be_success
    end
  end

  describe "GET 'update'" do
    it "returns http success" do
      get 'update', params.merge(id: @id, default_following_option: {new_offering_update: 1})
      response.should redirect_to("/")
    end
  end
end
