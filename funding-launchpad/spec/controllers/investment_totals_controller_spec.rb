require 'spec_helper'

describe InvestmentTotalsController do
  login_user

  before(:each) do
    @user.add_role :vim_admin
    @investment_total = InvestmentTotal.create
  end

  describe "GET edit" do
    it "assigns the requested group as @group" do
      get :edit, id: @investment_total.id
      assigns(:investment_total).should eq(@investment_total)
      response.should be_success
    end
  end

  describe "PUT 'update'" do
    it "should redirect_to admin" do
      put 'update', id: @investment_total.id
      response.should redirect_to(admin_path)
    end
  end
end
