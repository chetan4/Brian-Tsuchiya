require 'spec_helper'

describe AuthenticationsController do
  login_user

  before(:each) do
    @authentication = FactoryGirl.create(:authentication, :user_id => @user.id)
    @fb_authentication = FactoryGirl.create(:authentication, :user_id => @user.id, :provider => "facebook")
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "DELETE 'destroy'" do
    it "redirects to authentication path" do
      delete 'destroy', id: @authentication.id
      response.should redirect_to(authentications_path)
      @user.authentications.length.should eq 1
    end
  end

end
