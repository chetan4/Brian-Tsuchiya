require 'spec_helper'

describe InvestmentsController do
  login_user

  before(:each) do
    @offering = FactoryGirl.create(:offering_with_filing, contact_id: @user.id)
    @role = FactoryGirl.create(:offering_admin_role, resource_id: @offering.id)
    @user.add_role :offering_admin, @offering
    @user.save!
    @investment = FactoryGirl.create(:investment, offering_id: @offering.id, user_id: @user.id)
  end

  describe "GET 'index'" do
    it "should return http success" do
      get 'index', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'new'" do
    it "should return http success" do
      get 'new', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "should redirect to investments" do
      post 'create', offering_id: @offering.id, investment: {offered_amount: 1000}
      response.should redirect_to(offering_investments_share_path(@offering))
    end
  end

  describe "PUT 'update'" do
    it "should redirect_to investment" do
      put 'update', offering_id: @offering.id, id: @investment.id, investment: {}
      response.should redirect_to(offering_investments_path(@offering))
    end
  end

  describe "DELETE 'destroy'" do
    it "should redirect_to investment" do
      delete 'destroy', offering_id: @offering.id, id: @investment.id
      response.should redirect_to(myinvestments_path)
    end
  end
end
