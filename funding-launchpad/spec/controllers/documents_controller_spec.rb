require 'spec_helper'

describe DocumentsController do
  login_user

  before(:each) do
    @offering = FactoryGirl.create(:offering, contact_id: @user.id)
    @role = FactoryGirl.create(:offering_admin_role, resource_id: @offering.id)
    @user.add_role :offering_admin, @offering
    @user.save!
    @document = FactoryGirl.create(:document, offering_id: @offering.id)
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "returns http success" do
      get 'show', offering_id: @offering.id, id: @document.id
      response.should be_success
    end
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new', offering_id: @offering.id
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    it "returns http success" do
      get 'edit', offering_id: @offering.id, id: @document.id
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "returns http success" do
      post 'create', offering_id: @offering.id, document: {}
      response.should be_success
    end
  end

  describe "PUT 'update'" do
    it "returns http success" do
      put 'update', offering_id: @offering.id, id: @document.id, document: {}
      response.should redirect_to(offering_documents_path(@offering))
    end
  end

  describe "DELETE 'destroy'" do
    it "returns http success" do
      delete 'destroy', offering_id: @offering.id, id: @document.id
      response.should redirect_to(offering_documents_path(@offering))
    end
  end
end
