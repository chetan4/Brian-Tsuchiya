require 'spec_helper'

describe ReviewsController do
  login_vim_admin_user

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "returns http success" do
      offering = FactoryGirl.create(:offering)
      get 'show', id: offering.id
      response.should be_success
    end
  end
end
