require 'spec_helper'

describe Users::InvitationsController do
  login_user

  before(:each) do
    @resource = User.new
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end
end
