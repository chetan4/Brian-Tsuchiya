FactoryGirl.define do
  factory :invitation do
    email "investor@example.com"
    name  "John Investor"
    registration_key "abc123"
  end
end
