FactoryGirl.define do
  factory :team_member do
    name "John Doe"
    title "the awesome"
    bio "Really cool guy"
  end
end