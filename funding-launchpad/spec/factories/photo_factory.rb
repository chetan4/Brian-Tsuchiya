include ActionDispatch::TestProcess

FactoryGirl.define do
  factory :photo do
    offering
    image File.new("spec/data/foobear.jpg")
    caption "lorem ipsum dolor"
  end
end
