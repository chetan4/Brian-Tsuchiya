FactoryGirl.define do
  factory :follower do
    offering_id { FactoryGirl.create(:offering).id }
    user_id     { FactoryGirl.create(:user).id }
  end
end
