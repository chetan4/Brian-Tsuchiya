FactoryGirl.define do
  factory :link do
    offering
    link "http://www.example.com"
  end
end
