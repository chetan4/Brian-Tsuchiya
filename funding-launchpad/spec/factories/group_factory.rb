FactoryGirl.define do
  factory :group do
    name "Bacon!"
    subdomain "bacon"
    description "we love bacon!"
  end
end