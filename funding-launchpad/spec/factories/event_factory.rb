FactoryGirl.define do
  factory :event do
    formatted_date_string "05/01/2030"
    time_string "1:00pm"
    sequence(:title) {|n| "Title #{n}"}
    description "lorem ipsum dolor"
    offering_id { FactoryGirl.create(:offering).id }
  end
end
