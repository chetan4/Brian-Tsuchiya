FactoryGirl.define do
  factory :investment do
    offering
    user
    offered_amount 1000
  end
end
