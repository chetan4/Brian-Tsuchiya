FactoryGirl.define do
  factory :question do
    offering { FactoryGirl.create(:offering, contact: FactoryGirl.create(:user)) }
    content "who am I?"
    user  {FactoryGirl.create(:user)}
  end
end
