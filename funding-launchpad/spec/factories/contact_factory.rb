FactoryGirl.define do
  factory :contact do
    sequence(:email) {|i| "contact_#{i}@fundinglaunchpad.com" }
    name                      'John Doe'
    business_state            Contact::BUSINESS_STATES.first
    industry                  Contact::INDUSTRIES.first
    referral                  Contact::REFERRERS.first
    investor_ready            true
    description               'lorem ipsum dolor'
    raise_amount              100
  end
end
