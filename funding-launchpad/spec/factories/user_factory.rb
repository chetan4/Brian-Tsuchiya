FactoryGirl.define do
  factory :user do
    sequence(:email) {|i| "user_#{i}@fundinglaunchpad.com" }
    password "secret"
    password_confirmation "secret"
    confirmed_at  Time.now
    person {FactoryGirl.create(:person_with_address)}
  end

  factory :user_without_person, class: User do
    sequence(:email) {|i| "user_without_person_#{i}@fundinglaunchpad.com" }
    password "secret"
    password_confirmation "secret"
    confirmed_at  Time.now
  end

  factory :unconfirmed_user, class: User do
    sequence(:email) {|i| "user_#{i}@fundinglaunchpad.com" }
    confirmed_at  nil
    person {FactoryGirl.create(:person)}
  end
end
