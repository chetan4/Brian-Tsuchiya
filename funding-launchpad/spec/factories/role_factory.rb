FactoryGirl.define do
  factory :role do
    resource_id { FactoryGirl.create(:user).id }
  end

  factory :offering_admin_role, parent: :role do
    name 'offering_admin'
    resource_type 'Offering'
  end

  factory :vim_admin_role, parent: :role do
    name 'vim_admin'
  end
end
