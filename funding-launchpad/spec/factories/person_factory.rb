FactoryGirl.define do
  factory :person do
    first_name "John"
    last_name "Doe"
    resident "1"
    dont_validate_residence true
  end

  factory :person_with_address, parent: :person do
    after(:create) do |person, evaluator|
      FactoryGirl.create(:address, addressable_type: "Person", addressable_id: person.id, country_code: 'US', state: 'CO')
    end
  end
end
