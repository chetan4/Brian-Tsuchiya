include ActionDispatch::TestProcess

FactoryGirl.define do
  factory :update do
    offering
    subject "update sub"
    content "Lorem Ipsum"
  end
end
