FactoryGirl.define do
  factory :company do
    sequence(:name) {|i| "Company #{i}" }
    description "lorem ipsum dolor setit"
  end
end
