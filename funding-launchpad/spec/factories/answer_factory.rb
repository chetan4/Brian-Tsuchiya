FactoryGirl.define do
  factory :answer do
    question_id { FactoryGirl.create(:question).id }
    user_id     { FactoryGirl.create(:user).id }
    content     "lorem ipsum"
  end
end
