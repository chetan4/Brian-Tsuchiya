FactoryGirl.define do
  factory :reward, class: Reward do
    offering_id                 2
    price                       100.00
    number_available            10
    number_granted              5
    title                       "Reward title"
    description                 "Reward description is longer"
    base_cost                   1000
    base_cost_description       "Base cost"
    ancillary_cost              599
    ancillary_cost_description  "Ancillary cost"
    note                        "This is a note"
    enabled                     true
    reward_type                 "Gift"
  end
end
