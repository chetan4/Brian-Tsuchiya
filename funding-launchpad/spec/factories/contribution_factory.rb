FactoryGirl.define do
  factory :contribution do
    offering_id { FactoryGirl.create(:offering).id }
    user_id { FactoryGirl.create(:user).id }
    payment_type "Credit Card"
    status "New"
  end
end
