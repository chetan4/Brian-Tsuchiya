FactoryGirl.define do
  factory :video do
    offering
    description "lorem ipsum"
    link        "http://www.youtube.com/watch?v=b_91lhuUh_4"
  end
end
