FactoryGirl.define do
  factory :offering do
    sequence(:name) {|i| "Offering Name #{i}" }
    share_price 10
    min_investment 10
    min_shares 10
    max_shares 100
    shares_issued 20
    company
    expiration_date Date.tomorrow
    contact { FactoryGirl.build(:user) }

    trait :debt do
      debt true
      interest_rate 5
      minimum_goal 100
      term "2 years"
    end
  end


  factory :offering_with_filing, parent: :offering do
    after(:create) do |offering, evaluator|
      FactoryGirl.create(:filing, state: 'CO', offering: offering)
    end
  end
end
