FactoryGirl.define do
  factory :address do
    addressable_id 1
    addressable_type "Person"
    address1 "123 Here St"
    address2 "Suite 100"
    city "Boulder"
    state "CO"
    zip_code "80304"
    country_code "US"
    content_type "Residence"
  end
end
