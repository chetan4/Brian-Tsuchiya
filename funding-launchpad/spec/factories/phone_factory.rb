include ActionDispatch::TestProcess

FactoryGirl.define do
  factory :phone do
    content "555-1234"
  end
end
