include ActionDispatch::TestProcess

FactoryGirl.define do
  factory :document do
    offering
    document File.new("spec/data/foobear.jpg")
    description "lorem ipsum dolor"
  end
end
