FactoryGirl.define do
  factory :page do
    name    "Page name"
    title   "Page title"
    content "lorem ipsum"
  end
end
