FactoryGirl.define do
  factory :authentication do
    user_id  { FactoryGirl.create(:user).id }
    provider "linkedin"
    uid      82171377
  end
end