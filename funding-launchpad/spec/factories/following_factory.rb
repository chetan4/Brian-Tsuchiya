FactoryGirl.define do
  factory :following do
    user_id     { FactoryGirl.create(:user).id }
    offering_id { FactoryGirl.create(:offering).id }
  end
end
