require 'spec_helper'

describe Reward do
  before do
    @reward = FactoryGirl.create(:reward)
  end
  subject { @reward }

  it { should validate_presence_of :title }
  it { should validate_numericality_of :price }
  it { should validate_numericality_of :number_available }
  it { should validate_numericality_of :number_granted }
  it { should validate_numericality_of :base_cost }
  it { should validate_numericality_of :ancillary_cost }
end
