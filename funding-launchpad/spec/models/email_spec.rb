require 'spec_helper'

describe Email do
  before  { @email = Email.new }
  subject { @email }

  describe ".emailable" do
    it "should always have a emailable_id" do
      subject.emailable.should be_nil
    end
  end
end
