require 'spec_helper'

describe Contribution do
  before do
    @offering = FactoryGirl.create(:offering)
    @user = FactoryGirl.create(:user)
    @contribution = FactoryGirl.create(:contribution, offering: @offering, user: @user)
  end
  subject { @contribution }

  describe ".offering" do
    it "has an offering" do
      @contribution.offering.should_not be_nil
    end
  end

  describe ".user" do
    it "has a user" do
      @contribution.user.should_not be_nil
    end
  end
  
end
