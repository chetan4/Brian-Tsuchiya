require 'spec_helper'

describe Event do
  before  { @event = FactoryGirl.create(:event, start_date: Date.today, end_date: Date.today) }
  subject { @event }

  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:formatted_date_string) }
  it { should validate_presence_of(:time_string) }

  describe "#formatted_date_string=" do
    it "receives a string and parses to a date and saves as date string" do
      @event.formatted_date_string = Date.tomorrow.strftime("%m/%d/%Y")
      @event.save!
      @event.start_date.should eq (DateTime.now + 1.day).at_beginning_of_day.getutc
      @event.end_date.should eq (DateTime.now + 1.day).at_beginning_of_day.getutc
    end
  end

  describe "#formatted_date" do
    it "understands Dates as Strings" do
      @event.send(:formatted_date, Date.today.strftime("%m/%d/%Y")).should == Date.today
    end
    it "leaves correct formatting alone" do
      @event.send(:formatted_date, "01/01/1990").should == Date.strptime("01/01/1990", '%m/%d/%Y')
    end
    it "corrects formatting if needed" do
      @event.send(:formatted_date, "2012/01/01").should == Date.strptime("01/01/2012", '%m/%d/%Y')
    end
  end

  describe "#time_string" do
    it "returns the start_date time only" do
      subject.time_string.should == subject.start_date.to_s(:time_only)
    end
  end

  describe "#time_string=" do
    it "sets the time portion of start_date" do
      d = subject.start_date
      t = "02:45 PM"
      subject.time_string = t
      subject.start_date.should == DateTime.new(d.year, d.month, d.day, 14, 45, 0)
    end
  end

  describe "#end_time_string" do
    it "returns the end_date time only" do
      subject.end_time_string.should == subject.end_date.to_s(:time_only)
    end
  end

  describe "#endtime_string=" do
    it "sets the time portion of end_date" do
      d = subject.start_date
      t = "03:45 PM"
      subject.end_time_string = t
      subject.end_date.should == DateTime.new(d.year, d.month, d.day, 15, 45, 0)
    end

    it "sets the end_date to nil if time is not passed in" do
      subject.end_time_string = ""
      subject.end_date.should be_nil
    end
  end

  describe "#validate" do
    it "adds errors to the event if conditions are met" do
      subject.formatted_date_string = "foo"
      subject.time_string = "foo"
      subject.end_time_string = "foo"
      subject.custom_validate
      subject.errors.messages.should == {:formatted_date_string=>["is invalid"], :time_string=>["is invalid"], :end_time_string=>["is invalid"]}
    end
  end

end
