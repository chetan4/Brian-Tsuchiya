require 'spec_helper'

describe Address do
  before  { @address = FactoryGirl.create(:address) }
  subject { @address }

  describe "#us_residence" do
    it "returns all addresses with a us residence listed" do
      Address.us_residence.should == [subject]
    end
  end

  describe "#residence" do
    it "returns all addresses with residence as content type" do
      invalid_address = FactoryGirl.create(:address, content_type: 'non-residence')
      Address.residence.include?(subject).should == true
      Address.residence.include?(invalid_address).should == false
    end
  end

  describe "#has_residence?" do
    it "returns true if residence country code is not blank" do
      Address.has_residence?.should eq true
      subject.update_attributes({country_code: ""})
      Address.has_residence?.should eq false
    end
  end
end
