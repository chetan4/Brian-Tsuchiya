require 'spec_helper'

describe Update do
  before  { @update = Update.new }
  subject { @update }

  it {should validate_presence_of :subject }
  it {should validate_presence_of :content }

  it "should validate presence of edit summary on update" do
    @newUpdate = FactoryGirl.create(:update, offering: FactoryGirl.create(:offering))
    @newUpdate.update_attributes({subject: "test", content: "bacon", edit_summary: ""})
    @newUpdate.valid?
    @newUpdate.errors.messages[:edit_summary].should == ["can't be blank"]
  end
end
