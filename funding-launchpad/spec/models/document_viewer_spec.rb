require 'spec_helper'

describe DocumentViewer do
  it { should belong_to :document }
  it { should belong_to :user }
end
