require 'spec_helper'

describe Answer do
  before do
    @contact  = FactoryGirl.create(:user)
    @offering = FactoryGirl.create(:offering, contact: @contact)
    @question = FactoryGirl.create(:question, offering: @offering, user_id: FactoryGirl.create(:user).id)
    @answer   = FactoryGirl.create(:answer, question: @question)
  end
  subject { @answer }

  describe ".question" do
    it "has a question" do
      @answer.question.should_not be_nil
    end
  end

  describe ".user" do
    it "has a user" do
      @answer.user.should_not be_nil
    end
  end
end


