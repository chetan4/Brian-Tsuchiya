require 'spec_helper'

describe Video do
  it {should validate_presence_of :description }
  it {should validate_presence_of :link }

  describe "#reset_defaults" do
    it "should remove the default tag on any photo for the offering" do
      offering  = FactoryGirl.create(:offering)
      photo     = FactoryGirl.create(:photo, offering: offering, default: true)
      video     = FactoryGirl.build(:video, offering: offering)
      offering.photos << photo
      video.default = true
      video.save
      photo.default.should == false
    end

    it "should remove the default tag on any video for the offering" do
      offering  = FactoryGirl.create(:offering)
      video     = FactoryGirl.create(:video, offering: offering, default: true)
      new_video = FactoryGirl.build(:video, offering: offering)
      offering.videos << video
      new_video.default = true
      new_video.save
      video.default == false
    end
  end
end
