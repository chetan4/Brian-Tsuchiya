require 'spec_helper'

describe Role do
  before  { @role = Role.new }
  subject { @role }

  describe ".users" do
    it "should belong to users" do
      subject.users.should_not be_nil
    end
  end
end
