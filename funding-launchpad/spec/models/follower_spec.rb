require 'spec_helper'

describe Follower do
  before  { @follower = FactoryGirl.create(:follower) }
  subject { @follower }

  it { should validate_presence_of :offering_id }
  it { should validate_presence_of :user_id }
end
