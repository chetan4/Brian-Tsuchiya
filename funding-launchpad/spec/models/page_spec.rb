require 'spec_helper'

describe Page do
  before  { @page = FactoryGirl.create(:page) }
  subject { @page }

  it { should validate_presence_of :name }
  it { should validate_presence_of :title }
  it { should validate_presence_of :content }
end
