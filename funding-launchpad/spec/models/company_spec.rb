require 'spec_helper'

describe Company do
  before  { @company = FactoryGirl.create(:company) }
  subject { @company }

  describe "#name" do
    it { should validate_presence_of(:name) }
  end
end
