require 'spec_helper'

describe User do
  before  { @user = FactoryGirl.create(:user, newsletter: 0) }
  subject { @user }

  it { should belong_to :person }

  describe "following?" do
    it "returns array of followings belonging to offering passed in" do
      offering  = FactoryGirl.create(:offering)
      following = FactoryGirl.create(:following, offering_id: offering.id, user_id: @user.id)
      subject.following?(offering).should == following
    end
  end

  describe "follow!" do
    it "creates a following for a given offering" do
      offering = FactoryGirl.create(:offering)
      response = subject.follow!(offering)
      response.class.should == Following
      response.offering.should == offering
      response.user.should == subject
    end
  end

  describe "unfollow!" do
    it "destroys a following for a given offering" do
      offering  = FactoryGirl.create(:offering)
      following = FactoryGirl.create(:following, offering_id: offering.id, user_id: @user.id)
      subject.unfollow!(offering)
      subject.following?(offering).should == nil
    end
  end

  describe "can_invest?" do
    it "returns boolean if user can invest in a offering" do
      person = FactoryGirl.create(:person_with_address)
      subject.update_attribute(:person_id, person.id)
      person.stub!(:us_resident? => true)
      person.stub!(:has_residence? => true)
      subject.can_invest?(FactoryGirl.create(:offering_with_filing)).should == true
    end
  end

  describe "#add_user_to_mailchimp_lists" do
    it "should always define the list ids in the test environment" do
      subject.should_receive(:define_list_ids).exactly(1).times
      subject.send(:add_user_to_mailchimp_lists)
    end

    it "should call add_to_newsletter if newsletter opt-in" do
      subject.newsletter = true
      subject.should_receive(:add_to_newsletter).exactly(1).times
      subject.send(:add_user_to_mailchimp_lists)
    end

    it "should call add_to_offerings if offerings opt-in" do
      subject.pcp_email = 1
      subject.should_receive(:add_to_offerings).exactly(1).times
      subject.send(:add_user_to_mailchimp_lists)
    end
  end

  describe "#define_list_ids" do
    it "should set the mailchimp list constants to testing list constant in test env" do
      subject.send(:define_list_ids)
      FLP["MAILCHIMP_NEWSLETTER_LIST_ID"].should  == FLP["MAILCHIMP_TESTING_LIST_ID"]
      FLP["MAILCHIMP_OFFERRING_LIST_ID"].should   == FLP["MAILCHIMP_TESTING_LIST_ID"]
    end
  end

  describe "#add_to_newsletter" do
    it "should call add_use_to_mailchimp with the newsletter_id" do
      subject.should_receive(:add_user_to_mailchimp).exactly(1).times do |*args|
        args.first.should == FLP["MAILCHIMP_NEWSLETTER_LIST_ID"]
        args.second.has_key?(:GROUPINGS).should == true
      end
      subject.send(:add_to_newsletter)
    end
  end

  describe "#add_to_offerings" do
    it "should call add_use_to_mailchimp with the offering_id" do
      subject.should_receive(:add_user_to_mailchimp).exactly(1).times do |*args|
        args.first.should == FLP["MAILCHIMP_OFFERRING_LIST_ID"]
        args.second.has_key?(:GROUPINGS).should == false
      end
      subject.send(:add_to_offerings)
    end
  end

  describe "#define_info" do
    it "should not include FNAME and LNAME if not provided" do
      subject.stub!(:person => nil)
      result = subject.send(:define_info)
      result.has_key?(:FNAME).should == false
      result.has_key?(:LNAME).should == false
    end

    it "should include FNAME and LNAME if provided" do
      result = subject.send(:define_info)
      result.has_key?(:FNAME).should == true
      result.has_key?(:LNAME).should == true
    end

    it "should return an empty hash if no person and no newsletter" do
      user = FactoryGirl.build(:user, person: nil, newsletter: false)
      user.send(:define_info).should == {}
    end
  end

  describe "#add_user_to_mailchimp" do
    it "should add the user to mailchimp" do
      user = FactoryGirl.create(:user, email: 'test_user4@fundinglaunchpad.com', newsletter: true, person: FactoryGirl.create(:person, first_name: 'test', last_name: 'user'))
      user.send(:add_user_to_mailchimp, FLP["MAILCHIMP_TESTING_LIST_ID"], true).should == true
    end

    it "should add the user to mailchimp without the person attributes" do
      user = FactoryGirl.create(:user_without_person, email: 'test_user2@fundinglaunchpad.com', newsletter: true)
      user.send(:add_user_to_mailchimp, FLP["MAILCHIMP_TESTING_LIST_ID"], true).should == true
    end
  end

  describe "#remove_user_from_mailchimp_lists" do
    it "should be received on destroy" do
      user = FactoryGirl.create(:user)
      user.should_receive(:remove_user_from_mailchimp_lists)
      user.destroy
    end
  end

  describe "#assign_group_roles" do
    it "should assign group roles to the user" do
      group = FactoryGirl.create(:group)
      subject.assign_group_roles(group)
      subject.has_role?(:group_admin, group).should eq true
      subject.has_role?(:group_member, group).should eq true
    end
  end
end
