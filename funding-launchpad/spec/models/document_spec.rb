require 'spec_helper'

describe Document do
  it { should belong_to(:offering) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:document) }

  describe ".sorted" do
    it "should sort the documents, putting the required doc first" do
      offering  = FactoryGirl.create(:offering)
      offering.documents = []
      document1 = FactoryGirl.create(:document, offering: offering)
      document2 = FactoryGirl.create(:document, offering: offering, required: true)
      document3 = FactoryGirl.create(:document, offering: offering)
      offering.documents << document1
      offering.documents << document2
      offering.documents << document3

      offering.documents.sort_by { |doc| doc.required ? 0 : 1 }.should eq([document2, document1, document3])
    end
  end
end
