require 'spec_helper'

describe Link do
  before  { @link = FactoryGirl.create(:link) }
  subject { @link }

  describe ".offering" do
    it "should belong to a offering" do
      subject.offering.should_not be_nil
    end
  end

  describe "#add_protocol_if_needed" do
    it "should add http to link if needed" do
      subject.link = "www.apple.com"
      subject.save
      subject.link.should == "http://www.apple.com"
    end
  end
end
