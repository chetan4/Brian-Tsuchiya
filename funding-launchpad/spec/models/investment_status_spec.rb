require 'spec_helper'

describe InvestmentStatus do
  before do
    @offering   = FactoryGirl.create(:offering, share_price: 100, min_investment: 100)
    @investment = FactoryGirl.create(:investment, offering_id: @offering.id)
    @status     = InvestmentStatus.new(investment_id: @investment.id)
  end
  subject { @status }

  describe ".investment" do
    it "should belong to an investment" do
      @status.investment.should == @investment
    end
  end
end
