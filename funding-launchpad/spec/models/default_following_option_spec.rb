require 'spec_helper'

describe DefaultFollowingOption do
  describe ".user" do
    it "should belong to a user" do
      dfo = FactoryGirl.create(:default_following_option)
      dfo.user.should_not be_nil
    end
  end
end
