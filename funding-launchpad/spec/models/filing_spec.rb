require 'spec_helper'

describe Filing do
  before  { @filing = FactoryGirl.create(:filing) }
  subject { @filing }

  describe ".offering" do
    it "should belong to a offering" do
      subject.offering.should be_nil
    end
  end

end
