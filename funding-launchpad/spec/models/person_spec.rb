require 'spec_helper'

describe Person do
  before do
    @person   = FactoryGirl.create(:person)
    @address  = FactoryGirl.create(:address, addressable_type: "Person", addressable_id: @person.id, country_code: nil, state: 'CO')
  end
  subject { @person }

  describe "#resident" do
    it "should validate presence of unless dont_validate_residence is true" do
      @person.dont_validate_residence = false
      @person.resident = nil
      subject.valid?
      subject.errors.messages[:resident].should == ["must be selected", ""]
    end
    it "should validate presence of if should_validate_residence is true" do
      @person.dont_validate_residence = true
      subject.valid?
      subject.errors.messages.should == {}
    end
  end

  describe "#resident?" do
    it "returns boolean if resident attribute is set" do
      subject.resident = ""
      subject.resident?.should == false
      subject.resident = "1"
      subject.resident?.should == true
      subject.resident = "0"
      subject.resident?.should == false
    end
  end

  describe "#set_residence" do
    it "sets the country code if a resident, removes state if not a resident" do
      subject.addresses[0].country_code.should be_nil
      subject.set_residence
      subject.addresses[0].country_code.should == "US"
    end

    it "sets the state to nil if not a resident" do
      subject.stub(:resident?).and_return(false)
      subject.set_residence
      subject.addresses[0].state.should be_nil
    end
  end

  describe "#check_state_presence" do
    it "ensures the state is set if the person is a US resident" do
      subject.addresses[0].state = ""
      subject.resident = "1"
      subject.addresses[0].country_code = "US"
      subject.check_state_presence
      subject.addresses[0].errors.messages[:state].should == ["must be selected"]
    end
  end

  describe "#check_country_presence" do
    it "ensures country code is set if resident is not set" do
      subject.addresses[0].country_code = nil
      subject.resident = "0"
      subject.check_country_presence
      subject.addresses[0].errors.messages[:country_code].should == ["must be selected"]
    end
  end

  describe "#us_resident?" do
    it "returns boolean if any address listed is in US" do
      subject.addresses[0].country_code = "US"
      subject.addresses[0].content_type = "Residence"
      subject.addresses[0].save!
      subject.us_resident?.should == true
    end
  end

  describe "#state_of_residence" do
    it "returns the state of the subject's residence" do
      subject.addresses[0].state = "CO"
      subject.addresses[0].content_type = "Residence"
      subject.addresses[0].save!
      subject.state_of_residence.should == "CO"
    end
  end

  describe "#full_name" do
    it "returns the subject's first and last name" do
      subject.full_name.should == "John Doe"
    end
  end

  describe "#state=" do
    it "creates an address with the state if no address saved" do
      subject.stub!(:addresses => [])
      subject.state = "OH"
      subject.addresses.first.state.should == "OH"
      subject.addresses.length.should == 1
    end

    it "sets the state of the residence address" do
      subject.state = "WA"
      subject.state.should == "WA"
    end
  end
end
