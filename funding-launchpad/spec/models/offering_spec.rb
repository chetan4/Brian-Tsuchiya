require 'spec_helper'

describe Offering do
  before  { @offering = FactoryGirl.create(:offering) }
  subject { @offering }

  it { should validate_presence_of(:name) }

  it "should validate presence of edit summary on update if offering has been active and updating versioned fields" do
    subject.updating_versioned_fields = true
    subject.update_attributes({status: subject.status | HAS_BEEN_ACTIVE, edit_summary: ""})
    subject.valid?.should eq false
    subject.errors.messages[:edit_summary].should == ["can't be blank"]
  end

  it "should not validate presence of edit summary on update if offering has not been active" do
    subject.update_attributes({edit_summary: ""})
    subject.valid?.should eq true
  end

  describe "self.active_and_invited" do
    it "should return all active offerings and private offerings a user has been invited to" do
      public_offering   = FactoryGirl.create(:offering, :status => 14)
      private_offering  = FactoryGirl.create(:offering, :private => 1, :status => 14)
      user              = FactoryGirl.create(:user, :invitation_offering => private_offering.id)

      Offering.active_and_invited(user).should == [public_offering, private_offering]
    end
  end

  it "should clear equity fields on save if its a debt offering" do
    subject = FactoryGirl.create(:offering, :debt, {shares_issued: 100, min_shares: 200, max_shares: 250, share_price: 2.50})
    subject.shares_issued.should eq 0
    subject.min_shares.should eq nil
    subject.max_shares.should eq nil
    subject.share_price.should eq nil
  end

  it "should clear debt fields and create minimum goal on save if its an equity offering" do
    subject.update_attributes({interest_rate: 5.5, term: "2 years", repayment: 1, minimum_goal: 2000})
    subject.interest_rate.should eq nil
    subject.term.should eq nil
    subject.repayment.should eq nil
    subject.minimum_goal.should eq 100
  end

  it "should validate presence of term if debt" do
    subject = FactoryGirl.create(:offering, :debt)
    subject.term = nil
    subject.valid?.should eq false
  end

  it "should save term as seconds and return as text using chronic duration" do
    subject = FactoryGirl.create(:offering, :debt, :term => "1 day")
    subject.term.should eq "1 day"
  end

  describe "#min_investment" do
    it { should_not allow_value('abcd').for(:min_investment) }
    it { should allow_value(200).for(:min_investment) }
    it "should allow a value at least greater than or equal to share_price" do
      subject.stub!(:share_price => 10)
      subject.min_investment = 10
      subject.valid?.should eq true
      subject.min_investment = 11
      subject.valid?.should eq true
      subject.min_investment = 9
      subject.valid?.should eq false
    end
  end

  describe "#min_shares" do
    it { should_not allow_value('abcd').for(:min_shares) }
    it { should allow_value(2).for(:min_shares) }
    it "should allow a value less than or equal to max shares" do
      subject.stub!(:max_shares => 100)
      subject.min_shares = 99
      subject.valid?.should eq true
    end
    it "should not allow a value greater than max shares" do
      subject.stub!(:max_shares => 100)
      subject.min_shares = 101
      subject.valid?.should eq false
    end
  end

  describe "#max_shares" do
    it { should_not allow_value('abcd').for(:max_shares) }
    it "should allow a value greater than or equal to min_shares" do
      subject.stub!(:min_shares => 100)
      subject.max_shares = 101
      subject.valid?.should eq true
    end
    it "should not allow a value less than min_shares" do
      subject.stub!(:min_shares => 100)
      subject.max_shares = 99
      subject.valid?.should eq false
    end
  end

  describe "#share_price" do
    it { should allow_value(0).for(:share_price) }
    it { should allow_value(1).for(:share_price) }
    it { should allow_value(1.75).for(:share_price) }
    it "should remove any commas" do
      offering = FactoryGirl.build(:offering, share_price: "1,000")
      offering.save
      offering.share_price.should eq 1000
    end

    it "should save correctly" do
      offering = FactoryGirl.build(:offering, share_price: "1.75")
      offering.save
      offering.share_price.should eq 1.75
    end
  end

  describe "#shares_issued" do
    it { should allow_value("1,000").for(:shares_issued) }
    it { should allow_value(0).for(:shares_issued) }
    it { should allow_value(1).for(:shares_issued) }
    it "should remove any commas" do
      offering = FactoryGirl.build(:offering, shares_issued: "5,000")
      offering.save
      offering.shares_issued.should eq 5000
    end
  end

  describe "#minimum_goal" do
    subject = FactoryGirl.create(:offering, :debt, :term => "3 years")
    it "should not allow letters if debt" do
      subject.minimum_goal = 'abcd'
      subject.valid?.should eq false
    end
    it "should allow a value greater than or equal to min_investment if debt" do
      subject.stub!(:min_investment => 1000)
      subject.minimum_goal = 1001
      subject.valid?.should eq true
      subject.minimum_goal = 1000
      subject.valid?.should eq true
    end
    it "should not allow a value less than min_investment if debt" do
      subject.stub!(:min_investment => 1000, :debt => true)
      subject.minimum_goal = 999
      subject.valid?.should eq false
    end
  end

  describe "#in_development" do
    it "returns all offerings with a status matching IN_DEVELOPMENT" do
      valid_offering    = FactoryGirl.create(:offering, status: IN_DEVELOPMENT)
      invalid_offering  = FactoryGirl.create(:offering, status: DEVELOPMENT_COMPLETED)
      Offering.in_development.all.include?(valid_offering).should eq true
      Offering.in_development.all.include?(invalid_offering).should eq false
    end
  end

  describe "#development_completed" do
    it "returns all offerings with a status of matching DEVELOPMENT_COMPLETED" do
      valid_offering    = FactoryGirl.create(:offering, status: DEVELOPMENT_COMPLETED)
      invalid_offering  = FactoryGirl.create(:offering, status: IN_DEVELOPMENT)
      Offering.development_completed.all.include?(valid_offering).should eq true
      Offering.development_completed.all.include?(invalid_offering).should eq false
    end
  end

  describe "#approved" do
    it "returns all offerings with a status of matching APPROVED_FOR_LAUNCH" do
      valid_offering    = FactoryGirl.create(:offering, status: APPROVED_FOR_LAUNCH)
      invalid_offering  = FactoryGirl.create(:offering, status: IN_DEVELOPMENT)
      Offering.approved.all.include?(valid_offering).should eq true
      Offering.approved.all.include?(invalid_offering).should eq false
    end
  end

  describe "#unapproved" do
    it "returns all offerings with a status of matching APPROVED_FOR_LAUNCH" do
      valid_offering    = FactoryGirl.create(:offering, status: IN_DEVELOPMENT)
      invalid_offering  = FactoryGirl.create(:offering, status: APPROVED_FOR_LAUNCH)
      Offering.unapproved.all.include?(valid_offering).should eq true
      Offering.unapproved.all.include?(invalid_offering).should eq false
    end
  end

  describe "#active" do
    it "returns all offerings with a status of matching ACTIVE" do
      valid_offering    = FactoryGirl.create(:offering, status: ACTIVE)
      invalid_offering  = FactoryGirl.create(:offering, status: APPROVED_FOR_LAUNCH)
      Offering.active.all.include?(valid_offering).should eq true
      Offering.active.all.include?(invalid_offering).should eq false
    end
  end

  describe "#funding" do
    it "returns all offerings with a status of matching FUNDING" do
      valid_offering    = FactoryGirl.create(:offering, status: FUNDING)
      invalid_offering  = FactoryGirl.create(:offering, status: APPROVED_FOR_LAUNCH)
      Offering.funding.all.include?(valid_offering).should eq true
      Offering.funding.all.include?(invalid_offering).should eq false
    end
  end

  describe "#funded" do
    it "returns all offerings with a status of matching FUNDED" do
      valid_offering    = FactoryGirl.create(:offering, status: FUNDED)
      invalid_offering  = FactoryGirl.create(:offering, status: APPROVED_FOR_LAUNCH)
      Offering.funded.all.include?(valid_offering).should eq true
      Offering.funded.all.include?(invalid_offering).should eq false
    end
  end

  describe "#disabled" do
    it "returns all offerings with a status of matching DISABLED" do
      valid_offering    = FactoryGirl.create(:offering, status: DISABLED)
      invalid_offering  = FactoryGirl.create(:offering, status: APPROVED_FOR_LAUNCH)
      Offering.disabled.all.include?(valid_offering).should eq true
      Offering.disabled.all.include?(invalid_offering).should eq false
    end
  end

  describe "#closed" do
    it "returns all offerings with a status of matching CLOSED" do
      valid_offering    = FactoryGirl.create(:offering, status: CLOSED)
      invalid_offering  = FactoryGirl.create(:offering, status: APPROVED_FOR_LAUNCH)
      Offering.closed.all.include?(valid_offering).should eq true
      Offering.closed.all.include?(invalid_offering).should eq false
    end
  end

  describe "#by_state" do
    it "returns all offerings with filings in a state" do
      valid_offering    = FactoryGirl.create(:offering, status: ACTIVE)
      valid_filing      = FactoryGirl.create(:filing, state: "CO", offering_id: valid_offering.id)
      invalid_offering  = FactoryGirl.create(:offering, status: ACTIVE)
      invalid_filing    = FactoryGirl.create(:filing, state: "WA", offering_id: invalid_offering.id)
      Offering.by_state("CO").all.include?(valid_offering).should eq true
      Offering.by_state("CO").all.include?(invalid_offering).should eq false
    end
  end

  describe "#ending_soon" do
    it "returns all active offerings ending within 7 days" do
      valid_offering    = FactoryGirl.create(:offering, status: ACTIVE, expiration_date: (Date.today + 6.days))
      invalid_offering  = FactoryGirl.create(:offering, status: ACTIVE, expiration_date: (Date.today + 8.days))
      Offering.ending_soon.all.include?(valid_offering).should eq true
      Offering.ending_soon.all.include?(invalid_offering).should eq false
    end
  end

  describe "#valuation_pre" do
    it "returns the valuation of the company pre money" do
      @offering.stub!(shares_issued: 0, share_price: 0)
      @offering.valuation_pre.should eq 0
      @offering.stub!(shares_issued: 100, share_price: 10)
      @offering.valuation_pre.should eq 1000
    end
  end

  describe "#min_valuation_post" do
    it "returns the minimum valuation of the company post money" do
      @offering.stub!(shares_issued: 0, share_price: 1, minimum_goal: 5)
      @offering.min_valuation_post.should eq 5
      @offering.stub!(shares_issued: 100, share_price: 10, minimum_goal: 50)
      @offering.min_valuation_post.should eq 1050
    end
  end

  describe "#max_valuation_post" do
    it "returns the maximum valuation of the company post money" do
      @offering.stub!(shares_issued: 0, share_price: 1, max_shares: 5)
      @offering.max_valuation_post.should eq 5
      @offering.stub!(shares_issued: 100, share_price: 10, max_shares: 5)
      @offering.max_valuation_post.should eq 1050
    end
  end

  describe "Offering.valid_filter?" do
    it "returns boolean if filter is valid" do
      Offering::FILTERS.each do |filter|
        Offering.valid_filter?(filter).should eq true
      end

      Offering.valid_filter?("not_a_valid_filter").should eq false
    end
  end

  describe "#current_state_tokens" do
    it "returns a JSON hash of state ids and names, based on filings" do
      filing = FactoryGirl.create(:filing, state: "CO", offering_id: @offering.id)
      expected = [{ :id => "CO", :name => "Colorado" }].to_json
      @offering.current_state_tokens.should eq expected
    end
  end

  describe "#state_tokens=" do
    it "should remove all current filings and build new based on array of ids" do
      ids = "CO,WA"
      @offering.state_tokens = ids

      @offering.filings.map(&:state).should eq ['CO', 'WA']
    end
  end

  describe "@desired_start" do
    it "returns a formatted version of desired_start_date" do
      @offering.desired_start_date = Date.today
      @offering.desired_start.should eq Date.today.strftime("%m/%d/%Y")
    end
  end

  describe "#activation" do
    it "returns a formatted version of the activated_at" do
      @offering.activated_at = Date.today
      @offering.activation.should eq Date.today.strftime("%m/%d/%Y")
    end
  end

  describe "#expiration_date" do
    it "accepts a string in the format of %m/%d/%Y" do
      @offering.expiration_date = Date.today
      @offering.save!
      @offering.expiration_date.should eq Date.today
      @offering.expiration.should eq Date.today.strftime("%m/%d/%Y")
    end
  end

  describe "#formatted_expiration_date=" do
    it "receives a string and parses to a date and saves as expiration date" do
      @offering.formatted_expiration_date = Date.today.strftime("%m/%d/%Y")
      @offering.save!
      @offering.expiration_date.should eq Date.today
    end
  end

  describe "#formatted_date" do
    it "understands Dates as Strings" do
      @offering.send(:formatted_date, Date.today.strftime("%m/%d/%Y")).should eq Date.today
    end
    it "leaves correct formatting alone" do
      @offering.send(:formatted_date, "01/01/1990").should eq Date.strptime("01/01/1990", '%m/%d/%Y')
    end
    it "corrects formatting if needed" do
      @offering.send(:formatted_date, "2012/01/01").should eq Date.strptime("01/01/2012", '%m/%d/%Y')
    end
  end

  describe "#expiration" do
    it "returns a formatted version of the expiration date" do
      @offering.expiration_date = Date.today
      @offering.expiration.should eq Date.today.strftime("%m/%d/%Y")
    end
  end

  describe "#invested" do
    it "returns a sum of all investment accepted amounts" do
      investment = FactoryGirl.create(:investment, accepted_amount: 100, offering_id: @offering.id)
      @offering.invested.should eq 100
    end
  end

  describe "#maximum_goal" do
    it "returns the sum of the share_price x max_shares" do
      @offering.stub!(share_price: 100, max_shares: 10)
      @offering.maximum_goal.should eq 1000
    end
  end

  describe "#min_equity" do
    it "returns the sum of minimum amount of equity for a offering for shares_issued" do
      @offering.stub!(shares_issued: 0, min_shares: 0)
      @offering.min_equity.should eq 0

      @offering.stub!(shares_issued: 100, min_shares: 10)
      @offering.min_equity.should eq "9.09"
    end
  end

  describe "#max_equity" do
    it "returns the sum of maximum amount of equity for a offering for shares_issued" do
      @offering.stub!(shares_issued: 0, max_shares: 0)
      @offering.max_equity.should eq 0

      @offering.stub!(shares_issued: 100, max_shares: 10)
      @offering.max_equity.should eq "9.09"
    end
  end

  describe "#min_funded_percent" do
    it "returns the progress towards the min funding goal" do
      @offering.min_funded_percent.should eq 0

      investment = FactoryGirl.create(:investment, accepted_amount: 100, offering_id: @offering.id)
      @offering.min_funded_percent.should eq 100
    end
  end

  describe "#max_funded_percent" do
    it "returns the progress towards the max funding goal" do
      @offering.max_funded_percent.should eq 0
      investment = FactoryGirl.create(:investment, accepted_amount: 100, offering_id: @offering.id)
      @offering.max_funded_percent.should eq 10
    end
  end

  describe "#min_of_max_percent" do
    it "returns the percent of the minimum goal to the max funding goal" do
      @offering.min_of_max_percent.should eq 10
    end
  end

  describe "#offering_status" do
    it "returns one of the status constants" do
      @offering.stub!(status: CLOSED)
      @offering.offering_status.should eq "Closed"
      @offering.stub!(status: DISABLED)
      @offering.offering_status.should eq "Disabled"
      @offering.stub!(status: ACTIVE)
      @offering.offering_status.should eq "Active"
      @offering.stub!(status: APPROVED_FOR_LAUNCH)
      @offering.offering_status.should eq "Approved For Launch"
      @offering.stub!(status: DEVELOPMENT_COMPLETED)
      @offering.offering_status.should eq "Development Completed"
      @offering.stub!(status: IN_DEVELOPMENT)
      @offering.offering_status.should eq "In Development"
    end
  end

  describe "#num_investors" do
    it "returns the number of unique investors in a offering " do
      @investment1 = FactoryGirl.create(:investment, user_id: 1, offering_id: @offering.id)
      @investment1.update_attribute(:status, Investment.completed)
      @investment2 = FactoryGirl.create(:investment, user_id: FactoryGirl.create(:user).id, offering_id: @offering.id)
      @investment3 = FactoryGirl.create(:investment, user_id: 1, offering_id: @offering.id)
      @offering.num_investors.should eq 1
    end
  end

  describe "#offering_status?" do
    it "returns a boolean" do
      @offering.stub!(status: ACTIVE)
      @offering.offering_status?(ACTIVE).should eq true
    end
  end

  describe "#in_development?" do
    it "returns a boolean" do
      @offering.stub!(status: IN_DEVELOPMENT)
      @offering.in_development?.should eq true
    end
  end

  describe "#development_completed?" do
    it "returns a boolean" do
      @offering.stub!(status: DEVELOPMENT_COMPLETED)
      @offering.development_completed?.should eq true
    end
  end

  describe "#approved_for_launch?" do
    it "returns a boolean" do
      @offering.stub!(status: APPROVED_FOR_LAUNCH)
      @offering.approved_for_launch?.should eq true
    end
  end

  describe "#active?" do
    it "returns a boolean" do
      @offering.stub!(status: ACTIVE)
      @offering.active?.should eq true
    end
  end

  describe "#funded?" do
    it "returns a boolean" do
      @offering.stub!(status: FUNDED)
      @offering.funded?.should eq true
    end
  end

  describe "#filed_in_state?" do
    it "returns a boolean" do
      filing = FactoryGirl.create(:filing, state: "CO", offering_id: @offering.id)
      @offering.filed_in_state?("CO").should eq true
      @offering.filed_in_state?("WA").should eq false
    end
  end

  describe "#upcoming_events" do
    it "returns an array of upcoming events" do
      event = FactoryGirl.create(:event, offering_id: @offering.id, start_date: Date.today)
      @offering.upcoming_events.include?(event).should eq true
    end
  end

  describe "#ppm" do
    it "should return a boolean" do
      subject.ppm?.should eq false
    end
  end

  describe "#private" do
    it "should return a boolean" do
      subject.private?.should eq false
    end
  end

  describe "#filed_in_states" do
    it "returns a sorted array of the filed states" do
      filing = FactoryGirl.create(:filing, state: "CO", offering_id: @offering.id)
      filing = FactoryGirl.create(:filing, state: "AL", offering_id: @offering.id)
      filing = FactoryGirl.create(:filing, state: "MO", offering_id: @offering.id)
      @offering.filed_in_states.should eq ['AL', 'CO', 'MO']
    end
  end

  describe "#min_goal_reached" do
    it "returns a boolean" do
      @offering.stub!(share_price: 100, min_shares: 10, minimum_goal: 1000 )
      investment = FactoryGirl.create(:investment, accepted_amount: 999, offering_id: @offering.id)
      @offering.min_goal_reached?.should eq false
      investment = FactoryGirl.create(:investment, accepted_amount: 1, offering_id: @offering.id)
      @offering.min_goal_reached?.should eq true
    end
  end

  describe "#required_docs" do
    it "returns the documents marked as required for the offering" do
      document  = FactoryGirl.create(:document, required: true)
      document2 = FactoryGirl.create(:document, required: true)
      document3 = FactoryGirl.create(:document, required: false)
      @offering.documents << document
      @offering.documents << document2
      @offering.documents << document3
      @offering.required_docs.should eq([document, document2])
    end
  end

  describe "#final_weeks?" do
    it "should return false if the expiration date has passed" do
      @offering.expiration_date = Date.yesterday
      @offering.final_weeks?.should be_false
    end
    it "should return false if the expiration date is not set" do
      @offering.expiration_date = nil
      @offering.final_weeks?.should be_false
    end
    it "should return boolean depending on if the offering close date is within range" do
      @offering.expiration_date = Date.tomorrow
      @offering.final_weeks?.should be_true
      @offering.expiration_date = Date.today + 5.weeks
      @offering.final_weeks?.should be_false
    end
  end

  describe "#expired?" do
    it "should return true if the expiration date has passed" do
      @offering.expiration_date = Date.yesterday
      @offering.expired?.should be_true
    end
    it "should return false if the expiration date is not set" do
      @offering.expiration_date = nil
      @offering.expired?.should be_false
    end
    it "should return false if the expiration date has not passed" do
      @offering.expiration_date = Date.today
      @offering.expired?.should be_false
    end
  end

  describe "#repayment_text" do
    it "should return the repayment text" do
      @offering.stub!(repayment: 1 )
      @offering.repayment_text.should  eq "Annual"
      @offering.stub!(repayment: 2 )
      @offering.repayment_text.should  eq "Semi Annual"
      @offering.stub!(repayment: 4 )
      @offering.repayment_text.should  eq "Quarterly"
      @offering.stub!(repayment: 12 )
      @offering.repayment_text.should eq "Monthly"
    end
  end
end
