require 'spec_helper'

describe Contact do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:business_state) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:raise_amount) }
  it { should allow_value(Contact::BUSINESS_STATES.first).for(:business_state) }
  it { should_not allow_value('test').for(:business_state) }
  it { should allow_value(true).for(:investor_ready) }
  it { should allow_value(Contact::INDUSTRIES.first).for(:industry) }
  it { should allow_value('typed in industry').for(:industry) }
  it { should allow_value(Contact::REFERRERS.first).for(:referral) }
  it { should_not allow_value('test').for(:referral) }
  it { should allow_value('').for(:referral) }

  describe "#set_industry" do
    it "should overwrite the industry if industry_input is set" do
      contact = FactoryGirl.build(:contact, industry: 'Other')
      contact.industry_input = "another input"
      contact.save
      contact.industry.should == "another input"
    end

    it "should leave the category if industry_input is not set" do
      contact = FactoryGirl.build(:contact, industry: 'Other')
      contact.save
      contact.industry.should == 'Other'
    end
  end
end
