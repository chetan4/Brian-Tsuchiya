require 'spec_helper'

describe FollowingOption do
  before  { @following_option = FactoryGirl.create(:following_option) }
  subject { @following_option }

  describe "#update_from_defaults" do
    it "should copy the attributes" do
      dfo = FactoryGirl.create(:default_following_option, user_id: 1)
      subject.update_from_defaults(dfo)
      subject.user_id.should == 1
    end
  end
end
