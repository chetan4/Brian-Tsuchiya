require 'spec_helper'

describe Message do
  before  { @message = Message.new(:email => 'john@example.com') }
  subject { @message }

  it { should validate_presence_of :name }
  it { should validate_presence_of :email }
  it { should validate_presence_of :subject }
  it { should validate_presence_of :body }
  it { should allow_value('john@example.com').for(:email) }
  it { should_not allow_value('foo').for(:email) }

  describe "#initialize" do
    it "should allow a hash to set values on initialization" do
      message = Message.new(:subject => 'bar')
      message.subject.should == 'bar'
    end
  end

  describe "#persisted?" do
    it "should return false" do
      subject.persisted?.should == false
    end
  end
end

