require 'spec_helper'

describe Investment do
  before do
    @offering = FactoryGirl.create(:offering, share_price: 100, min_investment: 100)
    @investment = FactoryGirl.create(:investment, offering_id: @offering.id)
  end
  subject { @investment }

  describe "STATES" do
    it "should be an array of all accepted states of an investment" do
      Investment::STATES.class.should == Array
    end
  end

  describe "self.completed" do
    it "returns the state in which we consider the investment completed" do
      Investment.completed.should == Investment::STATES.fourth
    end
  end

  describe "self.initiated" do
    it "returns the first state of the investment process" do
      Investment.initiated.should == Investment::STATES.first
    end
  end

  describe "self.signed" do
    it "returns the second state of the investment process" do
      Investment.signed.should == Investment::STATES.second
    end
  end

  describe "self.countersigned" do
    it "returns the third state of the investment process" do
      Investment.countersigned.should == Investment::STATES.third
    end
  end

  describe "self.declined" do
    it "returns message if investment has been declined" do
      Investment.declined.should == Investment::STATES.last
    end
  end

  describe "self.escrowed" do
    it "returns the investments in the escrowed state" do
      @escrowed_investment  = FactoryGirl.create(:investment)
      @escrowed_investment.update_attribute(:status, Investment.completed)
      @open_investment      = FactoryGirl.create(:investment)
      Investment.escrowed.include?(@escrowed_investment).should == true
      Investment.escrowed.include?(@open_investment).should == false
    end
  end

  describe "#save" do
    it "should put new records in an initiated state" do
      subject.status.should == Investment.initiated
    end
  end

  describe "#offered_amount" do
    it { should_not allow_value('abcd').for(:offered_amount) }
    it { should allow_value(500).for(:offered_amount) }
    it { should validate_presence_of(:offered_amount) }
  end

  describe "#greater_than_min_investment" do
    it "validates if the offered_amount is less than the offering's min_investment" do
      subject.offered_amount = 50
      subject.valid?.should == false
      subject.offered_amount = 100
      subject.valid?.should == true
    end
  end

  describe "#greater_than_share_price" do
    it "validates if the offered_amount is less than the offering's share price" do
      subject.offered_amount = 50
      subject.valid?.should == false
      subject.offered_amount = 100
      subject.valid?.should == true
    end
  end

  describe "#no_remainder" do
    it "validates that the offered_amount is a clean divider of the share price" do
      subject.offered_amount = 200
      subject.valid?.should == true
      subject.offered_amount = 133
      subject.valid?.should == false
    end
  end

  describe "#num_shares_offered" do
    it "returns the number of shares that can be purchased for an offered amount" do
      subject.offered_amount = 200
      subject.num_shares_offered.should == 2
    end
  end
end
