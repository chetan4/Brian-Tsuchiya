require 'spec_helper'

describe Following do
  before  { @following = FactoryGirl.create(:following) }
  subject { @following }

  it { should validate_presence_of :offering_id }
  it { should validate_presence_of :user_id }
end
