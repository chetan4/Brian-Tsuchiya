require 'spec_helper'

describe Detail do
  before  { @detail = FactoryGirl.create(:detail) }
  subject { @detail }

  describe ".detailable" do
    it "should always have a detailable_id" do
      subject.detailable.should be_nil
    end
  end
end
