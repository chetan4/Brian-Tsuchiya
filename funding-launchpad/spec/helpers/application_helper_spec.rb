require 'spec_helper'

describe ApplicationHelper do
  describe "#set_analytics_tracker" do
    it "should not set the tracker code to the stored PCP's if tracker does not exist" do
      PCP_CONFIG.stub!(:ga_tracker => nil)
      set_analytics_tracker
      GA.tracker.should == "UA-xxxxxx-x"
    end

    it "should set the tracker code to the stored PCP's if tracker exists" do
      PCP_CONFIG.stub!(:ga_tracker => 'a_fake_key')
      set_analytics_tracker
      GA.tracker.should == "a_fake_key"
    end
  end
end
