Feature: PCP Admin Scenarios

  Background:
    Given I run the app instance as 'funding_launchpad'
    And I am logged in as offering admin

  Scenario: PCP Admin can update the offering details
    When I visit the 'offering admin page'
    Then I can edit the various offering sections

  @javascript
  Scenario: PCP Admin can update the Company Description
    When  I visit the 'offering edit page for section d'
    Then  I should see a WYSIWYG

  Scenario: PCP Admin views Update admin page
    When I visit the 'updates admin page'
    Then I should be on the 'updates admin page'

  Scenario: PCP Admin selects featured Photo
    Given I have already uploaded photos
    When I visit the 'offering admin photo page'
    And I mark a photo as the default
    Then I should see it featured on the offering page
    And I should not see it listed in the photos section

