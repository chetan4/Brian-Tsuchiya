Feature: User submits Issuer information

  Background:
    Given I am not logged in

  Scenario: User initiates contact through the Issuer contact form
    When I visit the "contact page"
    And I fill out the contact form
    Then a contact should be created
    And I should be sent a confirmation email
    And the team should receive a notification

  Scenario: User views contact page and chooses an 'other' category
    When I visit the "contact page"
    And I select "Other" as a "contact_industry"
    Then I should see the "industry-input" input


