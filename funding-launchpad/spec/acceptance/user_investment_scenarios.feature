Feature: User investment scenarios

  Background:
    Given I am logged in

  #@javascript
  #Scenario: User is required to read the required_doc
  #  Given the offering has a required doc
  #  When I visit the "offering page"
  #  And I click the "Ready to Invest?" button
  #  Then I should see a required disclaimer

  Scenario: User has read required_doc
    When the offering has a required doc
    When I visit the "offering page"
    And I click the required doc
    And I visit the "offering page"
    And I click the "I Want to Invest" button
    Then I should be on the "new investment page"
    And I should not see a required disclaimer

  Scenario: User on share page and minimum is not reached
    When the offering has not met its minimum goal
    And I visit the "investment share page"
    Then I should see "until our minimum investment level is reached"

  Scenario: User on share page and minimum is reached
    When the offering has met its minimum goal
    And I visit the "investment share page"
    Then I should see "We've already reached our minimum investment threshold"
