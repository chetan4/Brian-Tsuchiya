Feature: Signup and Login

  Background:
    Given I am not logged in

  # @javascript
  # Scenario: User submits log in form
    # When I visit the "sign up page"
    # And I fill in "signin_email" with "foo@fundinglaunchpad.com"
    # And I fill in "signin_password" with "secret"
    # And I press "Log In"
    # Then I should see a notification saying "Logged in successfully."

  Scenario: User submits registration form
    When I visit the "sign up page"
    #TODO: refactor to open step when steps can call other steps
    And I fill in "user_email" with "foo@fundinglaunchpad.com"
    And I fill in "user_password" with "secret"
    And I fill in "user_password_confirmation" with "secret"
    And I choose "In the U.S."
    And I select "Colorado" from "user_person_attributes_addresses_attributes_0_state"
    And I press "Create Account"
    Then I will receive the welcome email at "foo@fundinglaunchpad.com"

#
  # @javascript
  # Scenario: User sees focus around signup form
    # When I visit the "sign up page"
    # Then I should see a focus around signup well
#
  # Scenario: User sees focus around login form
    # When I visit the "login page"
    # Then I should see a focus around login well


