Feature: Offering scenarios

  Background:
    Given there is an offering

  Scenario: User views an offering page
    When I visit the "offering page"
    Then I should see offering details

  Scenario: User views a pcp offering page on the main site
    When I visit a pcp offering page on the main site
    Then I should be redirected to the root

  Scenario: User views index page with a private offering
    When I am logged in
    And I have been invited to an offering
    And I visit the "offerings page"
    Then I should see that private offering
