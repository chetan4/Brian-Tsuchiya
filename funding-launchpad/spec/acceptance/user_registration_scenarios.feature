Feature: User requests registration (pre-launch)

  Background:
    Given I am not logged in

  #Scenario: User views home page
  #  When I visit the "home page"
  #  Then I should see a button "Sign Up"

  #Scenario: User views registration form
  #  When I visit the "home page"
  #  And I click a button "Sign Up"
  #  Then I should be on the "sign up page"

  Scenario: Investor requests invite
    When I visit the "home page"
    And I click a button "Join Us - Beta Signup"
    And I submit the investor request form
    Then I will receive the welcome email
    And I should see sharethis buttons
    And I should have a person record with a residence address

  Scenario: User views home page to sign up for newsletter
    When I visit the "home page"
    And I submit the newsletter form
    Then I should see a notification saying "Thank you for signing up for the newsletter. You will be the first to hear exciting news from The Funding Launchpad."
    And I will receive the newsletter email at "foo@fundinglaunchpad.com"

  Scenario: User tried to sign up for newsletter twice
    When I am already subscribed to the newsletter with "foo@fundinglaunchpad.com"
    And I visit the "home page"
    And I submit the newsletter form
    Then I should see a notification saying "That email address is already subscribed."
