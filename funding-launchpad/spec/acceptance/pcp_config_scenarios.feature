Feature: PCP configuration

  Scenario: PCPs have a global configuration object
    When I run the app instance as 'funding_launchpad'
    Then I should have a global object for that pcp

  #FIXME: the issue is that routes are eval'd (aka drawn) before
  # the test steps run and set the ENV['PCP'] value (and seed the db)
  # resulting in the root route constraint never matching
  #Scenario: PCP root routes are the public_view of the offering
  #  When I run the app instance as 'funding_launchpad'
  #  And I visit the "home page"
  #  Then I should be redirected to the offering public view

  Scenario: PCP admin can add a private investment
    When I run the app as a pcp
    And I am logged in as pcp admin
    And I visit the "private investment page"
    And I should see "Add An Investment"
    And I submit the investment form
    Then there should be an investment saved


  Scenario: Default open graph tags use PCP config
    When I run the app instance as 'funding_launchpad'
    And I visit the "home page"
    Then I should see an open graph "description" tag with "Funding Launchpad is awesome!"
    And I should see an open graph "title" tag with "Funding Launchpad PCP"
    And I should see an open graph "image" tag with "pcp/logo/rails.png"
    And I should see an open graph "url" tag with "http://www.example.com/"