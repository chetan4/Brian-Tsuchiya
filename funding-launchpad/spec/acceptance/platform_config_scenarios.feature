Feature: Platform configuration

  Scenario: Default open graph tags
    When I visit the "home page"
    Then I should see an open graph "description" tag with "Only Funding Launchpad allows you"
    And I should see an open graph "title" tag with "Funding Launchpad"
    And I should see an open graph "image" tag with "square_color_logo.png"
    And I should see an open graph "url" tag with "http://www.example.com/"