Feature: Admin tasks

  Background:
    Given I am logged in as an administrator

  Scenario: Admin views administration page
    When I visit the "home page"
    And I choose "Administration" from the user drop down
    Then I should be on the "administration page"

  Scenario: Admin views unapproved registered users
    When I visit the "administration page"
    And I click the link "User Review"
    Then I should be on the "Users page"
    And I should see a list of unapproved registered users
