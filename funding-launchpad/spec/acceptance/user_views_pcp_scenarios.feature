Feature: User viewing pcp site scenarios

  Background:
    Given I run the app instance as 'funding_launchpad'

  Scenario: User doesn't have access to offering updates page
    Given I am logged in
    When I visit the 'updates admin page'
    Then I should be on the 'offerings page'

  Scenario: User tries to create an offering
    When I am logged in
    And I visit the "new offering page"
    Then I should be on the 'offerings page'

  Scenario: User with pcp_admin role tries to create an offering
    When I am logged in as pcp admin
    And I visit the "new offering page"
    Then I should be on the "new offering page"

  Scenario: User does not see create offering
    When I am logged in
    And I visit the "root page"
    Then I should not see a link "Create Offering"

  Scenario: User with pcp_admin sees create offering
    When I am logged in as pcp admin
    And I visit the "root page"
    Then I should see a link "Create Offering"

  Scenario: User does not see My Offerings in dropdown
    When I am logged in
    And I visit the "root page"
    Then I should see a dropdown without a link "My Offerings"

  Scenario: User with pcp_admin role sees My Offerings in dropdown
    When I am logged in as pcp admin
    And I visit the "root page"
    Then I should see a dropdown with a link "My Offerings"

  Scenario: User should not see Browse
    When I am logged in
    And I visit the "root page"
    Then I should not see a link "Browse"

  Scenario: User should see Browse
    Given I don't run the app as a pcp
    When I am logged in
    And I visit the "root page"
    Then I should see a link "Browse"

  Scenario: User should see pcp logo
    When I visit the "root page"
    Then I should see the pcp logo

  Scenario: User should not see pcp logo
    Given I don't run the app as a pcp
    When I visit the "root page"
    Then I should see the normal logo

  Scenario: User should not see Company or Newsletter in footer
    When I visit the "root page"
    Then I should not see "Company" in the footer
    And I should not see "Newsletter" in the footer

  Scenario: User should see Company and Newsletter in footer
    Given I don't run the app as a pcp
    When I visit the "root page"
    Then I should "" see "Company" in the footer
    And I should "" see "Newsletter" in the footer

  Scenario: User should see pcp links in footer
    When I visit the "root page"
    Then I should "" see the pcp links

  Scenario: User should not see pcp links in footer
    Given I don't run the app as a pcp
    When I visit the "root page"
    Then I should not see the pcp links

  Scenario: User should not see links in navbar
    When I visit the "root page"
    Then I should not see the nav links

  Scenario: User should see links in navbar
    Given I don't run the app as a pcp
    When I visit the "root page"
    Then I should "" see the nav links

  Scenario: User should see login links in navbar
    When I visit the "root page"
    Then I should "" see the login links

  Scenario: User should not see login links in navbar
    Given I don't run the app as a pcp
    When I visit the "root page"
    Then I should not see the login links

  Scenario: User should be able to sign up again (if not confirmed)
    Given I have signed up for the beta
    When I sign up at a PCP
    Then I should receive the confirmation email
    And the confirmation email should have pcp language

  Scenario: User should be able to sign in if they are registered
    Given I have completed registration
    When I visit the pcp sign in page
    Then I should be able to sign in

  Scenario: User can optin to pcp email while signing up
    When I visit the "sign up page"
    Then I should see an optin for the pcp email
    And I should not see an optin for the newsletter

  Scenario: User opts into pcp email while signing up
    When I visit the "sign up page"
    And I fill out the form opting into pcp email
    Then I should be opted in to pcp email

  Scenario: User can opt out of pcp emails while signing up
    When I visit the "sign up page"
    And I fill out the form not opting into pcp email
    Then I should not be opted in

  Scenario: User makes investment in PCP
    When I am logged in
    And I visit the "new investment page"
    And I submit the new investment form
    Then I should receive the you invested email
    And the you invested email should have pcp language

#  Scenario: User tries to view offering that is not the pcp's
#    When I am logged in
#    And I visit a different offering page
#    Then I should be on the "pcp offering page"

  #FIXME: this is just going to continue redirecting
  #between the public_view that doesn't exist and user sign in
  #see the pcp_configuration test for details
  #Scenario: User tries to create new company
  #  When I am logged in
  #  And I visit the "new company page"
  #  Then I should see a notification saying "You are not authorized to access this page."

