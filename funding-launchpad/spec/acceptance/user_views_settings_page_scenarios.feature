Feature: User visits settings page

  Background:
    Given I am logged in
    And I visit the "settings page"

  Scenario: User views settings page
    I should see a tab "Account"
    And I should see a tab "Personal"

  Scenario: User views Account tab
    When I click a tab "Account"
    Then I should see a form with a field "Email"
    And I should see a form with a field "Password"

  @javascript
  Scenario: User views Personal tab
    When I click a tab "Personal"
    Then I should see content "General Info"
    And I should see content "Address"

  @javascript
  Scenario: User edits Personal info
    When I click a tab "Personal"
    And I click a button "Edit"
    Then I should see a form with a field "First name"
    And I should see a form with a field "Last name"

  @javascript
  Scenario: User edits Address info
    When I click a tab "Personal"
    And I click a button "address-edit-btn"
    Then I should see a form with a field "Street"
    And I should see a form with a field "Suite/Apt"
    And I should see a form with a field "City"
