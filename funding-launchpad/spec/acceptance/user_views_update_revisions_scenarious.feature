Feature: User views update revisions page

  Background:
    Given I am logged in
    And the offering has an update with versions
    And I visit the "updates page"


  Scenario: User views revisions

    And I should be on the "updates page"
    And I click the num edits link
    Then I should see update revisions content
